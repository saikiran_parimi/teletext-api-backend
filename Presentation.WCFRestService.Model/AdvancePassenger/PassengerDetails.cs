﻿using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.AdvancePassenger
{
    public class TopDogAdvPassengers
    {
        private List<Passenger> passengers;
        private string surname;
        private string departureDate;
        private string bookingReference;
        private string countrySite;
        
        public string CountrySite
        {
            get { return countrySite; }
            set { countrySite = value; }
        }

        
        public string BookingReference
        {
            get { return bookingReference; }
            set { bookingReference = value; }
        }
        
        public string Surname
        {
            get
            {
                return surname;
            }
            set { surname = value; }
        }
        
        public string DepartureDate
        {
            set
            {
                departureDate = value;
            }
            get { return departureDate; }
        }
        
        public List<Passenger> Passengers
        {
            get
            {
                return passengers;
            }
            set { passengers = value; }
        }
    }
    
    public class Passenger
    {
        private string passengerFullName;
        private string dateOfBirth;
        private string passportNumber;
        private string passportIssuedDate;
        private string placeOfIssue;
        private string redressNumber;
        private string passportExpiryDate;
        private string nationality;
        private bool isLeadPassenger;

        
        public string PassengerFullName
        {
            get { return passengerFullName; }
            set { passengerFullName = value; }
        }
        
        public string DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }
        
        public string PassportNumber
        {
            get { return passportNumber; }
            set { passportNumber = value; }
        }
        
        public string PassportIssuedDate
        {
            get
            {
                return passportIssuedDate;
            }
            set { passportIssuedDate = value; }
        }
        
        public string PlaceOfIssue
        {
            get { return placeOfIssue; }
            set { placeOfIssue = value; }
        }
        
        public string RedressNumber
        {
            get
            {
                return redressNumber;
            }
            set { redressNumber = value; }
        }
        
        public string PassportExpiryDate
        {
            get
            {
                return passportExpiryDate;
            }
            set { passportExpiryDate = value; }
        }
        
        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }
        
        public bool IsLeadPassenger
        {
            get { return isLeadPassenger; }
            set { isLeadPassenger = value; }
        }
    }
}
