﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Utilities
{
    public class VisionTicket
    {
        public string vis_txtusername { get; set; }
        public string vis_txttoken { get; set; }
        public string vis_module { get; set; }
        public string vis_operation { get; set; }
        public string vis_domain { get; set; }
        public string vis_department { get; set; }
        public string vis_status { get; set; }
        public string vis_priority { get; set; }
        public string vis_subject { get; set; }
        public string vis_type { get; set; }
        public string vis_ticket_post { get; set; }
        public string vis_email { get; set; }
        public string vis_from { get; set; }
        public string vis_send_message { get; set; }
        public string vis_private { get; set; }
        public string vis_sendmail { get; set; }
        public string vis_as_client { get; set; }
        public string vis_category { get; set; }
        public string vis_label { get; set; }
        public string vis_channel { get; set; }
    }
}
