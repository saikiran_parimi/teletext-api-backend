﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.LambdaHits
{
    public class RecentBlog
    {
        public int nodeId { get; set; }
        public string teaserTextOne { get; set; }
        public string teaserTextTwo { get; set; }
        public string imageUrl { get; set; }
        public string aliasUrl { get; set; }
        public string nameTag { get; set; }
        public string bodySummary { get; set; }
    }
}
