﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Routing
{
    public class RoutingDataByAirportCode
    {
        public string dateTimeLastUpdatedIntoS3 { get; set; }
        public string dateTimeLastUpdatedIntoRedis { get; set; }
        public string airportCode { get; set; }
        public int departureId { get; set; }
        public List<int> resortIds { get; set; }
        public List<int> duration { get; set; }
        public List<string> dates { get; set; }

        public RoutingDataByAirportCode()
        {
            this.resortIds = new List<int>();
            this.duration = new List<int>();
            this.dates = new List<string>();
        }
    }
}
