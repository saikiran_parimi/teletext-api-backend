﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Routing
{
    public class RoutingDataByResortId
    {
        public string dateTimeLastUpdatedIntoS3 { get; set; }
        public string dateTimeLastUpdatedIntoRedis { get; set; }
        public List<string> airportCodes { get; set; }
        public List<int> departureIds { get; set; }
        public int resortId { get; set; }
        public List<int> duration { get; set; }
        public List<string> dates { get; set; }

        public RoutingDataByResortId()
        {
            this.departureIds = new List<int>();
        }
    }
}
