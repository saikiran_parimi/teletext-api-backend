﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Routing
{
    public class Airport
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Code { get; set; }
    }
}
