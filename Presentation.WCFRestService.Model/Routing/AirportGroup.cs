﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Routing
{
    public class AirportGroup
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public List<Airport> Airports { get; set; }

        public AirportGroup()
        {
            this.Airports = new List<Airport>();
        }
    }
}
