﻿namespace Presentation.WCFRestService.Model.MaintainElasticSearchCluster
{
    public class DeleteQueryByOffersModel
    {
        public string ElasticSearchUserName { get; set; }
        public string ElasticSearchPassword { get; set; }
        public string IntEndPoint { get; set; }
        public string ProdEndPoint { get; set; }
        public string RequestBody { get; set; }
        public int ConnectTimeout { get; set; }
        public int ReadTimeout { get; set; }
    }
}
