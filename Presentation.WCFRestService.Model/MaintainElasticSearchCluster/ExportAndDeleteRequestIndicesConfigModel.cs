﻿namespace Presentation.WCFRestService.Model.MaintainElasticSearchCluster
{
    public class ExportAndDeleteRequestIndicesConfigModel
    {
        public string ElasticSearchUserName { get; set; }
        public string ElasticSearchPassword { get; set; }
        public string ProdListOfIndicesUrl { get; set; }
        public string ProdUrlToRetrieveScrollId { get; set; }
        public string RequestBodyToRetrieveScrollId { get; set; }
        public string ProdUrlToRetrieveSubsequentRecords { get; set; }
        public int NumberOfDaysBackToExportIndices { get; set; }
        public int NumberOfDaysBackToDeleteIndices { get; set; }
        public string RequestBodyToRetrieveSubsequentRecords { get; set; }
        public string ProdUrlToDeleteIndice { get; set; }
        public string IndicePrefix { get; set; }
        public string IndexPattern { get; set; }
        public int ConnectTimeout { get; set; }
        public int ReadTimeout { get; set; }
    }
}
