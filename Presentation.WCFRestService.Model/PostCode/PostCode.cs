﻿
using Newtonsoft.Json;

namespace Presentation.WCFRestService.Model
{
    public class PostCodeResponse
    {
        [JsonProperty("result")]
        public Result[] result { get; set; }
        [JsonProperty("code")]
        public int code { get; set; }
        [JsonProperty("message")]
        public string message { get; set; }
    }

    public class Result
    {
        [JsonProperty("postcode")]
        public string postcode { get; set; }
        [JsonProperty("postcode_inward")]
        public string postcode_inward { get; set; }
        [JsonProperty("postcode_outward")]
        public string postcode_outward { get; set; }
        [JsonProperty("post_town")]
        public string post_town { get; set; }
        [JsonProperty("dependant_locality")]
        public string dependant_locality { get; set; }
        [JsonProperty("double_dependant_locality")]
        public string double_dependant_locality { get; set; }
        [JsonProperty("thoroughfare")]
        public string thoroughfare { get; set; }
        [JsonProperty("dependant_thoroughfare")]
        public string dependant_thoroughfare { get; set; }
        [JsonProperty("building_number")]
        public string building_number { get; set; }
        [JsonProperty("building_name")]
        public string building_name { get; set; }
        [JsonProperty("sub_building_name")]
        public string sub_building_name { get; set; }
        [JsonProperty("po_box")]
        public string po_box { get; set; }
        [JsonProperty("department_name")]
        public string department_name { get; set; }
        [JsonProperty("organisation_name")]
        public string organisation_name { get; set; }
        [JsonProperty("udprn")]
        public int udprn { get; set; }
        [JsonProperty("postcode_type")]
        public string postcode_type { get; set; }
        [JsonProperty("su_organisation_indicator")]
        public string su_organisation_indicator { get; set; }
        [JsonProperty("delivery_point_suffix")]
        public string delivery_point_suffix { get; set; }
        [JsonProperty("line_1")]
        public string line_1 { get; set; }
        [JsonProperty("line_2")]
        public string line_2 { get; set; }
        [JsonProperty("line_3")]
        public string line_3 { get; set; }
        [JsonProperty("longitude")]
        public float longitude { get; set; }
        [JsonProperty("latitude")]
        public float latitude { get; set; }
        [JsonProperty("eastings")]
        public int eastings { get; set; }
        [JsonProperty("northings")]
        public int northings { get; set; }
    }
}
