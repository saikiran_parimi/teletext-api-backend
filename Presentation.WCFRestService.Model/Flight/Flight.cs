﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model
{

    public class FlightScheduleResponse
    {
        [JsonProperty("appendix")]
        public Appendix appendix { get; set; }
        [JsonProperty("scheduledFlights")]
        public List<ScheduledFlight> scheduledFlights { get; set; }
    }

    public class FlightStatusResponse
    {  
        [JsonProperty("appendix")]
        public Appendix appendix { get; set; }
        [JsonProperty("flightStatuses")]
        public List<Flightstatus> flightStatuses { get; set; }
    }

    public class Appendix
    {
        [JsonProperty("airlines")]
        public List<Airline> airlines { get; set; }
        [JsonProperty("airports")]
        public List<Airport> airports { get; set; }
        [JsonProperty("equipments")]
        public List<Equipment> equipments { get; set; }
    }

    public class Airline
    {
        [JsonProperty("fs")]
        public string fs { get; set; }
        [JsonProperty("iata")]
        public string iata { get; set; }
        [JsonProperty("icao")]
        public string icao { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("phoneNumber")]
        public string phoneNumber { get; set; }
        [JsonProperty("active")]
        public bool active { get; set; }
    }

    public class Airport
    {
        [JsonProperty("fs")]
        public string fs { get; set; }
        [JsonProperty("iata")]
        public string iata { get; set; }
        [JsonProperty("icao")]
        public string icao { get; set; }
        [JsonProperty("faa")]
        public string faa { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("street1")]
        public string street1 { get; set; }
        [JsonProperty("street2")]
        public string street2 { get; set; }
        [JsonProperty("city")]
        public string city { get; set; }
        [JsonProperty("cityCode")]
        public string cityCode { get; set; }
        [JsonProperty("stateCode")]
        public string stateCode { get; set; }
        [JsonProperty("postalCode")]
        public string postalCode { get; set; }
        [JsonProperty("countryCode")]
        public string countryCode { get; set; }
        [JsonProperty("countryName")]
        public string countryName { get; set; }
        [JsonProperty("regionName")]
        public string regionName { get; set; }
        [JsonProperty("timeZoneRegionName")]
        public string timeZoneRegionName { get; set; }
        [JsonProperty("weatherZone")]
        public string weatherZone { get; set; }
        [JsonProperty("localTime")]
        public DateTime localTime { get; set; }
        [JsonProperty("utcOffsetHours")]
        public float utcOffsetHours { get; set; }
        [JsonProperty("latitude")]
        public float latitude { get; set; }
        [JsonProperty("longitude")]
        public float longitude { get; set; }
        [JsonProperty("elevationFeet")]
        public int elevationFeet { get; set; }
        [JsonProperty("classification")]
        public int classification { get; set; }
        [JsonProperty("active")]
        public bool active { get; set; }
        [JsonProperty("delayIndexUrl")]
        public string delayIndexUrl { get; set; }
        [JsonProperty("weatherUrl")]
        public string weatherUrl { get; set; }
    }

    public class Equipment
    {
        [JsonProperty("iata")]
        public string iata { get; set; }
        [JsonProperty("name")]
        public string name { get; set; }
        [JsonProperty("turboProp")]
        public bool turboProp { get; set; }
        [JsonProperty("jet")]
        public bool jet { get; set; }
        [JsonProperty("widebody")]
        public bool widebody { get; set; }
        [JsonProperty("regional")]
        public bool regional { get; set; }
    }

    public class Flightstatus
    {
        [JsonProperty("flightId")]
        public int flightId { get; set; }
        [JsonProperty("carrierFsCode")]
        public string carrierFsCode { get; set; }
        [JsonProperty("flightNumber")]
        public string flightNumber { get; set; }
        [JsonProperty("departureAirportFsCode")]
        public string departureAirportFsCode { get; set; }
        [JsonProperty("arrivalAirportFsCode")]
        public string arrivalAirportFsCode { get; set; }
        [JsonProperty("departureDate")]
        public ScheduledTime departureDate { get; set; }
        [JsonProperty("arrivalDate")]
        public ScheduledTime arrivalDate { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("schedule")]
        public Schedule schedule { get; set; }
        [JsonProperty("operationalTimes")]
        public Operationaltimes operationalTimes { get; set; }
        [JsonProperty("flightDurations")]
        public Flightdurations flightDurations { get; set; }
        [JsonProperty("airportResources")]
        public Airportresources airportResources { get; set; }
        [JsonProperty("flightEquipment")]
        public Flightequipment flightEquipment { get; set; }
    }
    
    public class Schedule
    {
        public string flightType { get; set; }
        public string serviceClasses { get; set; }
        public string restrictions { get; set; }
        public Downline[] downlines { get; set; }
        public Upline[] uplines { get; set; }
    }

    public class Downline
    {
        public string fsCode { get; set; }
        public int flightId { get; set; }
    }

    public class Upline
    {
        public string fsCode { get; set; }
        public int flightId { get; set; }
    }

    public class Operationaltimes
    {
        public ScheduledTime publishedDeparture { get; set; }
        public ScheduledTime publishedArrival { get; set; }
        public ScheduledTime scheduledGateDeparture { get; set; }
        public ScheduledTime scheduledGateArrival { get; set; }
        public ScheduledTime estimatedGateArrival { get; set; }
        public ScheduledTime actualGateArrival { get; set; }
        public ScheduledTime actualGateDeparture { get; set; }
        public ScheduledTime flightPlanPlannedDeparture { get; set; }
        public ScheduledTime estimatedRunwayDeparture { get; set; }
        public ScheduledTime actualRunwayDeparture { get; set; }
        public ScheduledTime flightPlanPlannedArrival { get; set; }
        public ScheduledTime estimatedRunwayArrival { get; set; }
        public ScheduledTime actualRunwayArrival { get; set; }
    }

    public class ScheduledTime
    {
        public DateTime dateLocal { get; set; }
        public DateTime dateUtc { get; set; }
    }

    public class Flightdurations
    {
        public int scheduledBlockMinutes { get; set; }
    }

    public class Airportresources
    {
        public string departureTerminal { get; set; }
        public string departureGate { get; set; }
        public string arrivalTerminal { get; set; }
        public string arrivalGate { get; set; }
    }

    public class Flightequipment
    {
        public string scheduledEquipmentIataCode { get; set; }
    }
    
    public class ScheduledFlight
    {       
        [JsonProperty("carrierFsCode")]
        public string carrierFsCode { get; set; }
        [JsonProperty("flightNumber")]
        public string flightNumber { get; set; }
        [JsonProperty("departureAirportFsCode")]
        public string departureAirportFsCode { get; set; }
        [JsonProperty("arrivalAirportFsCode")]
        public string arrivalAirportFsCode { get; set; }
        [JsonProperty("departureTime")]
        public DateTime departureTime { get; set; }
        [JsonProperty("arrivalTime")]
        public DateTime arrivalTime { get; set; }
        [JsonProperty("flightEquipmentIataCode")]
        public string flightEquipmentIataCode { get; set; }
        [JsonProperty("stops")]
        public int stops { get; set; }
        [JsonProperty("departureTerminal")]
        public string departureTerminal { get; set; }
        [JsonProperty("arrivalTerminal")]
        public string arrivalTerminal { get; set; }
       
    }

}
