﻿using Presentation.WCFRestService.Model.Artirix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Presentation.WCFRestService.Model.Flight.Artirix
{
    public class HolidaysAsyncResult
    {
        public int total { get; set; }
        public List<Offer> offers { get; set; }
        public Facets facets { get; set; }
        public int totalHotels { get; set; }
        public bool isCompleted { get; set; }
    }
}
