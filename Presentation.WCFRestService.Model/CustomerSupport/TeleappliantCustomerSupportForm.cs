﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.CustomerSupport
{
    public class TeleappliantCustomerSupportForm
    {
        public string Callerid { get; set; }
        public string CallDateTime { get; set; }
        public string CallerNumber { get; set; }
        public string CallerNumberByAgent { get; set; }
        public string BookingReference { get; set; }
        public string Comment { get; set; }
        public string Session { get; set; }
        public string AgentId { get; set; }
        public string AgentName { get; set; }
        public string UniqueId { get; set; }
    }
}
