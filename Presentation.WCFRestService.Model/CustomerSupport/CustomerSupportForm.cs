﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.CustomerSupport
{
    public class CustomerSupportForm
    {
        public string leadPassengerName { get; set; }
        public string emailAddress { get; set; }
        public string bookingRef { get; set; }
        public string departureDate { get; set; }
        public string requestType { get; set; }
        public string message { get; set; }
    }
}
