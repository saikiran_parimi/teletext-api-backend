﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.CustomerSupport
{
    public class TeleappliantNumbering
    {
        public string session { get; set; }
        public string device_type { get; set; }        
        public Dictionary<string, Quote> quotes { get; set; }
        public double metric_geo { get; set; }
        public bool force_default_number { get; set; }
        public double metrics { get; set; }
        public string version { get; set; }
    }

    //public class Quotes
    //{
    //    public Dictionary<string, Quote> quotes { get; set; }
    //}

    public class Quote
    {
        public string ingress { get; set; }
        public int destination_id { get; set; }
        public string number { get; set; }
        public string expiry { get; set; }
        public string status { get; set; }
        public string area { get; set; }
        public int distance { get; set; }
        public string zone { get; set; }
    }
}
