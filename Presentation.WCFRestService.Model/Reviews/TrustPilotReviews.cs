﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{

    public class Link
    {
        public string method { get; set; }
        public string rel { get; set; }
        public string href { get; set; }
    }

    public class Consumer
    {
        public string id { get; set; }
        public int numberOfReviews { get; set; }
        public List<Link> links { get; set; }
        public string displayName { get; set; }
    }

    public class BusinessUnit
    {
        public string id { get; set; }
        public string identifyingName { get; set; }
        public List<Link> links { get; set; }
        public string displayName { get; set; }
    }

    public class TrustPilotReviews
    {
        public bool isVerified { get; set; }
        public string text { get; set; }
        public string status { get; set; }
        public List<Link> links { get; set; }
        public Consumer consumer { get; set; }
        public string id { get; set; }
        public BusinessUnit businessUnit { get; set; }
        public string title { get; set; }
        public int stars { get; set; }
        public string createdAt { get; set; }

        public string last_modified_at { get; set; }
        public int numberOfLikes { get; set; }
        public string language { get; set; }

        public bool isPublished { get; set; }
    }

}
