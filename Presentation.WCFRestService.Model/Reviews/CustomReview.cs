﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class CustomReview
    {
        public string id { get; set; }

        public string reviewTitle { get; set; }

        public string description { get; set; }
        public string reviewRating { get; set; }

        public Customer customer { get; set; }
        public string postedOn { get; set; }
        public string reviewProviderName { get; set; }

        public bool toPublish { get; set; }
    }
}
