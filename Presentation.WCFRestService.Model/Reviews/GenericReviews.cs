﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class GenericReviews
    {
        public List<ReviewProvider> reviewProvider { get; set; }
        public List<Review> reviews { get; set; }
    }
    public class Review
    {
        public string id { get; set; }
        public string reviewTitle { get; set; }
        public string description { get; set; }
        public int reviewRating { get; set; }
        public Customer customer { get; set; }
        public DateTime postedOn { get; set; }
        public string reviewProviderName { get; set; }
    }

    public class Customer
    {
        public string postedBy { get; set; }
    }

    public class ReviewProvider
    {
        public string name { get; set; }
        public string serviceRating { get; set; }
        public int totalReviewsCount { get; set; }
    }
}
