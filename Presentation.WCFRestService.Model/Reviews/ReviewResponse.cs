﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Reviews
{
    public class ReviewResponse
    {
        public int filteredReviewsCount { get; set; }
        public int totalReviewsCount { get; set; }
        public List<ReviewInformation> reviews { get; set; }
        public ResponseSummary error { get; set; }
    }
}
