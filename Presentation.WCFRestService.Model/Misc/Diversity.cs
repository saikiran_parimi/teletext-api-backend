﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{

    public class Diversity
    {
        public int destinationId { get; set; }
        public List<DiversityHotel> hotels { get; set; }
    }

    public class DiversityHotel
    {
        public string hotelKey { get; set; }
        public List<DiversityOffer> offers { get; set; }
    }

    public class DiversityOffer
    {
        public int boardTypeId { get; set; }
        public double price { get; set; }
        public string date { get; set; }
        public string quoteRef { get; set; }
    }
    public class DiversityError
    {
        public string error { get; set; }
    }

}
