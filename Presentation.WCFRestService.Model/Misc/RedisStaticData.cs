﻿using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.Misc
{

    public class RedisSataticData
    {
        public string _index { get; set; }
        public string _type { get; set; }
        public string _id { get; set; }
        public int _version { get; set; }
        public bool found { get; set; }
        public _Source _source { get; set; }
        public _Ttinfo _TTInfo { get; set; }
    }

    public class _Source
    {
        public int HotelId { get; set; }
        public string BuildingName { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public int MasterResortId { get; set; }
        public string MasterResortName { get; set; }
        public float CustomerRating { get; set; }
        public bool PopularHotel { get; set; }
        public int RatingLevel { get; set; }
        public string RatingName { get; set; }
        public int Rating { get; set; }
        public string Hotelkey { get; set; }
        public string ClosestAirport { get; set; }
        public string FriendlyUrl { get; set; }
        public bool HotelCompanyData { get; set; }
        public int RegionTTSSLabelD { get; set; }
        public Location Location { get; set; }
        public Address Address { get; set; }
        public Contact Contact { get; set; }
        public Region Region { get; set; }
        public Feature[] Features { get; set; }
        public Image[] Images { get; set; }
        public Description[] Descriptions { get; set; }
        public Policy[] Policies { get; set; }
    }

    public class Location
    {
        public float lat { get; set; }
        public float lon { get; set; }
    }

    public class Address
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string AddressLineFour { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }

    public class Contact
    {
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string EMailId { get; set; }
    }

    public class Region
    {
        public int RegionId { get; set; }
        public int ResortId { get; set; }
        public string Regname { get; set; }
        public int Reglevel { get; set; }
        public string AirportCodes { get; set; }
        public int ParentRegionId { get; set; }
        public string ParentRegionName { get; set; }
        public string ParentRegionLink { get; set; }
        public int GrandRegionId { get; set; }
        public string GrandRegionName { get; set; }
        public string GrandRegionLink { get; set; }
    }

    public class Feature
    {
        public string FeatureName { get; set; }
        public string[] FeaturesList { get; set; }
    }

    public class Image
    {
        public string ImageId { get; set; }
        public string ImageURL { get; set; }
        public string ImageDescription { get; set; }
        public bool DefaultImage { get; set; }
        public string ImageType { get; set; }
    }

    public class Description
    {
        public string DescriptionHeader { get; set; }
        public string DescriptionText { get; set; }
    }

    public class Policy
    {
        public string PolicyName { get; set; }
        public string PolicyDetail { get; set; }
    }

    public class _Ttinfo
    {
        public string[] HotelClassifications { get; set; }
        public Rating[] Ratings { get; set; }
        public string[] LocationAndNeighbourhood { get; set; }
        public string PropertyType { get; set; }
        public Offertag[] OfferTags { get; set; }
        public int NoOfRooms { get; set; }
        public string TARating { get; set; }
        public string[] Reviews { get; set; }
        public string GIATA { get; set; }
        public string TAID { get; set; }
        public string UpdatePermissionFromHC { get; set; }
        public string TripAdvisorRating { get; set; }
        public string TripAdvisorReviewCount { get; set; }
        public string TripAdvisorCacheCreateDate { get; set; }


    }

    public class Rating
    {
        public string RatingName { get; set; }
        public string RatingValue { get; set; }
    }

    public class Offertag
    {
        public string Offer { get; set; }
    }

}
