﻿using System;

namespace Presentation.WCFRestService.Model.Misc
{
    //public class PaymentDetails
    //{
    //    public String merchantRefNum { get; set; }
    //    public String paResp { get; set; }
    //    public String enrollmentId { get; set; }
    //    public String street { get; set; }
    //    public String city { get; set; }
    //    public String state { get; set; }
    //    public String country { get; set; }
    //    public String zip { get; set; }
    //    public String cardNum { get; set; }
    //    public int month { get; set; }
    //    public int year { get; set; }
    //    public decimal amount { get; set; }
    //    public String customerIp { get; set; }
    //    public String userAgent { get; set; }
    //    public string acceptHeader { get; set; }
    //    public string merchantUrl { get; set; }        
    //}

    public class CardCharge
    {
        public int CardID { get; set; }

        public String CardType { get; set; }

        public String MultiCom { get; set; }

        public String ThreeDSecapy { get; set; }

        public String Secpay { get; set; }

        public decimal CardCharges { get; set; }

        public bool IsPercentType { get; set; }

        public String SagePay { get; set; }
    }

    public class MakePayment
    {
        public String customerIp { get; set; }
        public String userAgent { get; set; }
        public string acceptHeader { get; set; }
        public string merchantUrl { get; set; }
        public String paResp { get; set; }
        public String merchantRefNum { get; set; }

        public String enrollmentId { get; set; }
        public int PaymentId { get; set; }

        public String RefNo { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Email { get; set; }

        public String CardType { get; set; }

        public String SelectedCardType { get; set; }

        public String CardNumber { get; set; }

        public String ValidFromMonth { get; set; }

        public String ValidFromYear { get; set; }

        public String ExpiryMonth { get; set; }

        public String ExpiryYear { get; set; }

        public String IssueNo { get; set; }

        public String CCVNumber { get; set; }

        public Decimal Amount { get; set; }

        public Decimal TotalDueAmount { get; set; }

        public Decimal CardCharges { get; set; }

        public Boolean IsSuccessful { get; set; }

        public String NameOnCard { get; set; }

        public String AcsUrl { get; set; }

        public String PaReq { get; set; }

        public Boolean IsThreeDSecure { get; set; }

        public String Address1 { get; set; }

        public String Address2 { get; set; }

        public String City { get; set; }

        public String State { get; set; }
        public String CountryCode { get; set; }

        public String Country { get; set; }

        public String PostCode { get; set; }

        public String Phone { get; set; }

        public string PaymentDate { get; set; }

        public string PaymentDueDate { get; set; }

        public string DepartureDate { get; set; }

        public String TransactionID { get; set; }

        public String PaysafeAuthorisationCode { get; set; }

        public String  PaySafeVPSAuthCode { get; set; }

        public String paymentError { get; set; }

        public String TopDogError { get; set; }

        public bool IsTopDogPostingSuccess { get; set; }

        public int CardId { get; set; }
        public string Status { get; set; }

        public string ErrorMessages { get; set; }

        public string Source { get; set; }

        public string Platform { get; set; }
    }
}

