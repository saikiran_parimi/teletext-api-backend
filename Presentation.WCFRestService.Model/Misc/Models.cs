﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model
{

    public class DestinationRegionList
    {
        public List<DestinationRegion> destinations { get; set; }
    }

    public class DestinationRegion
    {
        public string id { get; set; }
        public string name { get; set; }
        public string linkurl { get; set; }
        public string destcodes { get; set; }
        public string resortcode { get; set; }
        public string reglevel { get; set; }
        public string parent_regid { get; set; }
        public string parent_regname { get; set; }
        public string parent_link { get; set; }
        public string grand_regid { get; set; }
        public string grand_regname { get; set; }
        public string grand_reglink { get; set; }
    }

    public class RequestedRegion
    {
        public string regid { get; set; }
        public string regUrl { get; set; }
        public string regname { get; set; }
        public string reglevel { get; set; }
        public string parent_regid { get; set; }
        public string parent_regname { get; set; }
        public string parent_link { get; set; }
        public string grand_regid { get; set; }
        public string grand_regname { get; set; }
        public string grand_reglink { get; set; }
    }

    public class OverseasDestination
    {
        [JsonProperty("k")]
        public string DestinationId { get; set; }
        [JsonProperty("v")]
        public string DestinationName { get; set; }
        [JsonProperty("more")]
        public More[] More { get; set; }
        public string ParentDestinationId { get; set; }
        public string ParentDestinationName { get; set; }
    }

    public class More
    {
        [JsonProperty("k")]
        public string DestinationId { get; set; }
        [JsonProperty("v")]
        public string DestinationName { get; set; }
    }

    public class GuideHotel
    {
        public List<AutoSuggestHotel> Hotels { get; set; }
    }

    public class AutoSuggestHotel
    {
        public int MasterHotelId { get; set; }
        public string DestinationCode { get; set; }
        public string HotelName { get; set; }
        public string FriendlyURL { get; set; }
        public int ResortID { get; set; }
    }

    public class OTP
    {
        public string OTPValue { get; set; }
        public DateTime ExpirationTime { get; set; }
    }

    public class AuthStatus
    {
        public string Status { get; set; }
    }

    public class NewsLetterSubscriptionStatus
    {
        public bool Status { get; set; }
        public Int64 JobId { get; set; }
    }

    public class MinPriceView
    {
        public int minimumPrice { get; set; }
        public string quoteRef { get; set; }
        public DateTime departureDate { get; set; }
        public string websiteUrl { get; set; }
        public string artirixUrl { get; set; }
        public string boardBasis { get; set; }
        public float starRating { get; set; }
        public string iff { get; set; }
        public string hotelName { get; set; }
        public string description { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
    }

    public class HotelURL
    {
        public int MasterHotelID { get; set; }
        public string FriendlyURL { get; set; }
    }

    public class LowDepositInstallmentPlan
    {
        public string Amount { get; set; }

        public string AmountType { get; set; }

        public string PaymentStatus { get; set; }

        public DateTime PaymentDate { get; set; }
    }

    public class PaymentPlanFAQ
    {
        public string Name { get; set; }

        public string Link { get; set; }
    }
    public class MyBookings
    {
        public List<Passenger> passengers { get; set; }
        public List<Hotelproduct> hotelDetails { get; set; }
        public List<Flightproduct> flightDetails { get; set; }
        public List<Transferproduct> transferDetails { get; set; }
        public Payment payments { get; set; }
        public Client client { get; set; }
        public DateTime bookingDate { get; set; }
        public string bookingReference { get; set; }
        public string status { get; set; }
        public Bookedby bookedBy { get; set; }
        public ProductCost cost { get; set; }
        public List<FlightSuppliment> flightSupplimentList { get; set; }
        public string error { get; set; }
        public string Etag { get; set; }
        public string DestinationName { get; set; }
        public string lowDepositBalance { get; set; }
        public List<LowDepositInstallmentPlan> lowDepositInstallmentPlan { get; set; }
        public string  priorDays { get; set; }
        public string priorDaysInBtm        { get; set; }
        public string finalInstallment { get; set; }
        public string   cardName        { get; set; }
        public string cardType { get; set; }
        public string cardNumber { get; set; }        
        public bool isAutoPayments  { get; set; }
        public bool isLowDepositBooking { get; set; }        
        public List<PaymentPlanFAQ> paymentPlanFAQs { get; set; }
        public string airportName { get; set; }
        public string airportParkingLink { get; set; }
        public string airPortHotel { get; set; }
        public string airportHotelAndParking { get; set; }        
        public bool footerAddbaggagesExists { get; set; }
        public bool footerAirportHotelExists { get; set; }
        public bool footerAirportParkingExists { get; set; }
        public bool footerHotelsParkingExists { get; set; }
        public bool showATOLCertificate { get; set; }
        public string PhoneNumber { get; set; }
        public bool SubmitDocuments { get; set; } = false;
    }



    public class Client
    {
        public string title { get; set; }
        public string firstName { get; set; }
        public string surname { get; set; }
    }


    public class Deposit
    {
        public double depositValue { get; set; }
        public string depositDue { get; set; }
    }

    public class Lowdeposit
    {
        public double lowDepositValue { get; set; }
        public string lowDepositDue { get; set; }
    }

    public class Bookedby
    {
        public string name { get; set; }
        public string emailAddress { get; set; }
    }

    public class Passenger
    {
        public long passengerId { get; set; }
        public bool isLeadPassenger { get; set; }
        public string title { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string surname { get; set; }
        public int age { get; set; }
    }

    public class Hotelproduct
    {
        public Int64 topDogProductId { get; set; }
        public string supplierRef { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string hotelCode { get; set; }
        public string hotelName { get; set; }
        public string category { get; set; }
        public string roomType { get; set; }
        public string boardType { get; set; }

        public string Rating { get; set; }
        public string resortName { get; set; }
        public int adultsCount { get; set; }
        public int childrenCount { get; set; }
        public int infantsCount { get; set; }
        public string clientNotes { get; set; }
    }

    public class ProductCost
    {
        public float hotelGrossPrice { get; set; }
        public float flightGrossPrice { get; set; }
        public float transportGrossPrice { get; set; }
        public float paymentCharges { get; set; }
        public float totalPrice { get; set; }
    }


    public class Flightproduct
    {
        public Int64 topDogProductId { get; set; }
        public string supplierRef { get; set; }
        public long id { get; set; }
        public bool idSpecified { get; set; }
        public string flightNumber { get; set; }
        public string operatingAirlineCode { get; set; }
        public string departureAirportCode { get; set; }
        public string departureAirportName { get; set; }
        public string arrivalAirportCode { get; set; }        
        public string arrivalAirportName { get; set; }
        public DateTime departureDateTime { get; set; }
        public string Airline { get; set; }
        public string AirlineType { get; set; }
        public string DepartureTerminal { get; set; }
        public DateTime arrivalDateTime { get; set; }
        public int adultsCount { get; set; }
        public int childrenCount { get; set; }
        public int infantsCount { get; set; }
        public int FreeHandBagCount { get; set; }
        public string AdditionalCheckInBag { get; set; }
        public string operatingAirlineType { get; set; }
        public string ConfirmationCardLastFourDigits { get; set; }
        public string SupplierCheckinLink { get; set; }
        public string SupplierName { get; set; }
        public bool HideAdditionalInfo { get; set; }
    }

    public class FlightSuppliment
    {
        public Int64 topDogProductId { get; set; }
        public List<Supplements> supplements { get; set; }
    }

    public class Supplements
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public int? Type { get; set; }
        public bool TypeSpecified { get; set; }
        public string CurrencyCode { get; set; }
        public float TotalPrice { get; set; }
        public FlightPerPaxPrice AdultPrices { get; set; }
        public FlightPerPaxPrice ChildPrices { get; set; }
        public FlightPerPaxPrice InfantPrices { get; set; }
        public float GrossPrice { get; set; }
        public float NetPrice { get; set; }
        public float ClientCommissionRate { get; set; }
        public float ClientCommission { get; set; }
        public float VatRate { get; set; }
        public float ClientVAT { get; set; }
        public float TotalVAT { get; set; }
    }

    public class FlightPerPaxPrice
    {
        public float PerPaxPrice { get; set; }
        public int Quantity { get; set; }
    }


    public class Transferproduct
    {
        public Int64 topDogProductId { get; set; }
        public string supplierRef { get; set; }
        public string transferName { get; set; }
        public string transferType { get; set; }
        public string pickUpLocation { get; set; }
        public string dropLocation { get; set; }
        public int transferMinutes { get; set; }
        public DateTime pickUpDateTime { get; set; }
        public string clientNotes { get; set; }
    }
    public class Payment
    {
        public List<KeyValuePair<string, string>> charges { get; set; }
        public Deposit deposit { get; set; }
        public Lowdeposit lowDeposit { get; set; }
    }

    public class DocumentType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    public class DocumentList
    {
        public string CreationDateTime { get; set; }
        public string UserName { get; set; }
        public DocumentType DocumentType { get; set; }
        public string DocumentFormat { get; set; }
        public string DocumentUrl { get; set; }
        public string DeliveryType { get; set; }
        public int Category { get; set; }
    }

    public class MyDocuments
    {
        public string BookingReference { get; set; }
        public List<DocumentList> DocumentList { get; set; }
        public string Comment { get; set; }
    }

    public class MasterHotelImagesMap
    {
        public int MasterHotelID { get; set; }
        public int ImageCount { get; set; }
    }

    public class TelePhoneSet
    {
        public List<string> Number { get; set; }
        public string Name { get; set; }
    }

    public class TradingNameDestinationMapping
    {
        public Dictionary<string, List<DestinationTelePhoneSet>> TradingNameDestination { get; set; }

    }

    public class DestinationTelePhoneSet
    {
        public int Price { get; set; }
        public int TelephoneSetId { get; set; }

    }

    public class DestinationTelePhoneNumber
    {
        public int Price { get; set; }
        public string TelephoneSet { get; set; }
    }
    public class UserLocation
    {
        public string name { get; set; }
        public string id { get; set; }
    }

    public class User
    {
        public string username { get; set; }
        public UserLocation user_location { get; set; }
    }
    

    public class Datum
    {
        public string id { get; set; }
        public string lang { get; set; }
        public string location_id { get; set; }
        public string published_date { get; set; }
        public int rating { get; set; }
        public string type { get; set; }
        public string ratingImageUrl { get; set; }
        public string url { get; set; }
        public string travel_date { get; set; }
        public string text { get; set; }
        public User user { get; set; }
        public string title { get; set; }
        public List<object> subratings { get; set; }
    }

    public class TAResponse
    {
        public List<Datum> data { get; set; }
        //public string insertionTime { get; set; }
    }
    public class TAReviews
    {
        public List<ReviewsData> data { get; set; }
    }
    public class ReviewsData
    {
        public string published_date { get; set; }
        public int rating { get; set; }
        public string text { get; set; }
        public User user { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public List<object> subratings { get; set; }
    }
    public class AkamaiPurgeRequest
    {
        public List<string> objects { get; set; }
    }
    public class AkamaiPurgeResponse
    {
        public int estimatedSeconds { get; set; }
        public string purgeId { get; set; }
        public string supportId { get; set; }
        public int httpStatus { get; set; }
        public string detail { get; set; }
    }
    public class ChatMessage
    {
        public Guid sessionId { get; set; }
        public string message { get; set; }
        public string sender { get; set; }
    }
    public class ESMessageDoc
    {
        public Guid chatId { get; set; }
        public List<MessageObj> messages { get; set; }
    }

    public class MessageObj
    {
        public string sender { get; set; }
        public string message { get; set; }
        public string timestamp { get; set; }
    }
    public class BespokeTemplate
    {
        public string TemplateId { get; set; }
        public string PageUrl { get; set; }
        public string FileInput { get; set; }
    }
}