﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class SignUpDetails
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Phone_Code { get; set; }
        public string Telephone { get; set; }
        public string Departure_airport { get; set; }
        public string PreferredBoardBasis { get; set; }
        public string DisplayName { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string Email { get; set; }
        public string Keep_me_deals { get; set; }
        public string SMS { get; set; }
        public string Post { get; set; }
        public string Provider { get; set; }
        public string SignUpDate { get; set; }
        public string Platform { get; set; }
        public string AppVersion { get; set; }
        public string Brand { get; set; }
    }
}
