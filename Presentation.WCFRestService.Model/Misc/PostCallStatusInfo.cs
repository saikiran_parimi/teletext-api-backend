﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class PostCallStatusInfo
    {
        public string statusCode { get; set; }
        public string statusDescription { get; set; }
        public string X_Application_Status_Code { get; set; }
    }
}
