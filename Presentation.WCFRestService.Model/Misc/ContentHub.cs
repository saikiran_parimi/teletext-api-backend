﻿using System;
using System.Collections.Generic;

namespace Presentation.WCFRestService.Model.Misc
{
    public class ESHolidays
    {
        public int total { get; set; }
        public List<ESOffer> offers { get; set; }
        public ESFacets facets { get; set; }
        public int totalHotels { get; set; }


    }

    public class ESFacets
    {
        public List<ESFacetHotel> hotels { get; set; }
        public List<ESAirport> airports { get; set; }
        public ESPricerange priceRange { get; set; }
        public List<ESResort> resorts { get; set; }
        public int totalHotels { get; set; }
    }

    public class ESPricerange
    {
        public int min { get; set; }
        public int max { get; set; }
    }

    public class ESFacetHotel
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public string hotelKey { get; set; }
        public int count { get; set; }
    }

    public class ESAirport
    {
        public string name { get; set; }
        public int departureId { get; set; }
        public int count { get; set; }
    }

    public class ESResort
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public int count { get; set; }
    }

    public class ESOffer
    {
        public string id { get; set; }
        public string quoteRef { get; set; }
        public int price { get; set; }
        public bool isPreferential { get; set; }
        public string phone { get; set; }
        public ESJourney journey { get; set; }
        public ESAccommodation accommodation { get; set; }
        public ESOfferHotel hotel { get; set; }
        public string hotelOperator { get; set; }
        public string contentId { get; set; }
        public int tradingNameId { get; set; }
        public bool isPreferredByUser { get; set; }
        public int airportCode { get; set; }
        public int airportCollectionId { get; set; }
        public int regionId { get; set; }
        public int labelId { get; set; }
        public long responseId { get; set; }
        public int resortId { get; set; }
        public string regionName { get; set; }
        public string  timeStamp { get; set; }
    }

    public class ESJourney
    {
        public string outboundArrivalDate { get; set; }
        public string inboundDepartureDate { get; set; }
        public string departureDate { get; set; }
        public string returnDate { get; set; }
        public int duration { get; set; }
        public string departure { get; set; }
        public string gatewayName { get; set; }
        public string flightOperator { get; set; }
        public string destination { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public string gatewayCode { get; set; }
        public bool inboundFlightDirect { get; set; }
        public bool outboundFlightDirect { get; set; }
    }

    public class ESAccommodation
    {
        public string boardTypeCode { get; set; }
        public int boardTypeId { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int rating { get; set; }
        public string updated { get; set; }
    }

    public class ESOfferHotel
    {
        public int iff { get; set; }
        public List<string> images { get; set; }
        public List<string> mobileimages { get; set; }
        public List<string> thumbnailimages { get; set; }
        public string description { get; set; }
        public List<object> features { get; set; }
        public string name { get; set; }
        public string latlong { get; set; }
        public string starRating { get; set; }
        public ESRating rating { get; set; }
    }

    public class ESRating
    {
        public string tripAdvisorId { get; set; }
        public int averageRating { get; set; }
        public int reviewCount { get; set; }
    }
    //*****************Search Request for Elasticsearch********************//

    public class ESSearchRequest
    {
        public int adults { get; set; }
        //public string boardTypeId { get; set; }
        public int children { get; set; }
        public string dateMax { get; set;}
        public string dateMin { get; set; }
        //public string departureDate { get; set; }
        public int departureIds { get; set; }
        public string destinationIds { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int ttssCount { get; set; }
        public long responseId { get; set; }
        public List<string> regionIds { get; set; }
        public int parentDepartureIds { get; set; }
        public double ttssResponseTime { get; set; }
        public bool isCahceHit { get; set; }
        public List<string> boardType { get; set; }
        public List<string> ratings { get; set; }
        public List<string> tradingNameId { get; set; }
        public string timeStamp { get; set; }
        public string ttssURL { get; set; }
        public string GUID { get; set; }
        public int offersFound { get; set; }
        public int offersDecorated { get; set; }
        public int offersInserted { get; set; }
        public string hotelKeys { get; set; }
        public string isHotelKeys { get; set; }
        //public int priceMax { get; set; }
        //public int priceMin { get; set; }
        //public long timeStamp { get; set; }
        //public string ratings { get; set; }
    }
    public class ESDiversitySearchRequest
    {
        public int adults { get; set; }
        //public string boardTypeId { get; set; }
        public int children { get; set; }
        public string dateMax { get; set; }
        public string dateMin { get; set; }
        //public string departureDate { get; set; }
        public int departureIds { get; set; }
        public string destinationIds { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int ttssCount { get; set; }
        public long responseId { get; set; }
        public double ttssResponseTime { get; set; }
        public bool isCahceHit { get; set; }
        public List<string> ratings { get; set; }
        public string timeStamp { get; set; }
        public List<string> tradingNameId { get; set; }
        public string ttssURL { get; set; }
        public int offersFound { get; set; }
        public int offersDecorated { get; set; }
        public int offersInserted { get; set; }
        public List<string> hotelKeys { get; set; }
        //public int priceMax { get; set; }
        //public int priceMin { get; set; }
        //public long timeStamp { get; set; }
        //public string ratings { get; set; }
    }
    //********************************** TTSS request *********************//
    public class ttssResponseStream
    {
        public string ttssResponse { get; set; }
        public double ttssResponseTime { get; set; }

    }
}
