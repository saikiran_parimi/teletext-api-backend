﻿using System;
using System.Collections.Generic;
using Presentation.WCFRestService.Model.Reviews;

namespace Presentation.Web.WCFRestServices
{  
    public class MinPriceToolQuery
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string baseDepartureDate { get; set; }
        public List<int> destinationIds { get; set; }
        public List<int> departureIds { get; set; }
        public List<int> boardType { get; set; }
        public int duration { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public List<int> ratings { get; set; }
        public string channelId { get; set; }
        public bool flexibleDates { get; set; }
        public int dateOffset { get; set; }
        public string tradingNameId { get; set; }
        public string iff { get; set; }
        public string channelName { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public string destinationName { get; set; }
        public string departureName { get; set; }
        public string websiteUrl { get; set; }
        public string artirixUrl { get; set; }
        public bool isDynamic { get; set; }
        public string dynamicUrl { get; set; }        
    }

    public class MinPriceToolObject
    {
        public bool isActive { get; set; }
        public int runHour { get; set; }
        public string sectionId { get; set; }
        public string queryId { get; set; }
        public int minPrice { get; set; }
        public string lastUpdated { get; set; }
        public string quoteRef { get; set; }
        public string image { get; set; }
        public string hotelLocation { get; set; }
        public string starRating { get; set; }
        public string tripAdvisorRating { get; set; }
        public string reviewCount { get; set; }
        public string boardBasis { get; set; }
        public string displayParentRegion { get; set; }
        public string displaySectionName { get; set; }
        public string glat { get; set; }
        public string  glong { get; set; }
        public MinPriceToolQuery query { get; set; }
        public bool? isImageOverride { get; set; }
        public bool? isFromDesktop { get; set; }
        public bool? isFromMobile { get; set; }
    }
    public class SectionInfo
    {
        public string sectionId { get; set; }
        public string image { get; set; }
        public string displaySectionName { get; set; }
    }
    public class Slider
    {
        public int SliderId { get; set; }
        public int SliderTabIndex { get; set; }
        public string SliderHeading { get; set; }
        public string SliderSubheading { get; set; }
        public string SliderFooterText { get; set; }
        public string SliderFooterLink { get; set; }
        public int SliderDesktopImageId { get; set; }
        public string SliderDesktopImageName { get; set; }
        public string SliderDesktopImageAltText { get; set; }
        public string SliderDesktopImageOriginalFilePath { get; set; }
        public int SliderMobileImageId { get; set; }
        public string SliderMobileImageName { get; set; }
        public string SliderMobileImageAltText { get; set; }
        public string SliderMobileImageOriginalFilePath { get; set; }
        public int SliderBotsNoFollow { get; set; }
        public string SliderTextColor { get; set; }
        public string SliderCloseTextBGColor { get; set; }
        public string SliderCloseArrowBGColor { get; set; }
        public string SliderOpenTextBGColor { get; set; }
        public string SliderOpenArrowBGColor { get; set; }
        public string SliderOpenBoxBGColor { get; set; }
        public double SliderOpenBoxOpacity { get; set; }
        public List<SliderPlaceHolder> PlaceHolders { get; set; }
    }

    public class SliderPlaceHolder
    {                   
        public int SliderId { get; set; }
        public int SliderPlaceholderId { get; set; }
        public int SliderPlaceholderTabIndex { get; set; }
        public string SliderPlaceholderText { get; set; }
        public string SliderPlaceholderLink { get; set; }
    }

    /* Artirix Classes for Slider */

    public class ArtirixDataItem
    {
        public int minPrice { get; set; }
        public string lastUpdated { get; set; }
        public string quoteRef { get; set; }
        public Query query { get; set; }
    }

    public class Query
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string baseDepartureDate { get; set; }
        public List<int> destinationIds { get; set; }
        public List<int> departureIds { get; set; }
        public List<int> boardType { get; set; }
        public int duration { get; set; }
        public int adults { get; set; }
        public int? children { get; set; }
        public int? priceMin { get; set; }
        public int priceMax { get; set; }
        public List<int> ratings { get; set; }
        public string channelId { get; set; }
        public bool flexibleDates { get; set; }
        public int? dateOffset { get; set; }
        public string tradingNameId { get; set; }
        public string iff { get; set; }
        public string websiteUrl { get; set; }
        public string ttssUrl { get; set; }
    }

    public class editUpdate
    {
        public string queryId { get; set; }
        public string description { get; set; }
        public string ID { get; set; }
        public string sectionId { get; set; }
        public string displaySectionName { get; set;}

    }
    public class saveUpdateMinPrice
    {
        public string adults { get; set; }
        public string children { get; set; }
        public string starRatings { get; set; }
        public string departureDate { get; set; }
        public string dateMin { get; set; }
        public string dateMax { get; set; }
        public string durationMin { get; set; }
        public string durationMax { set; get; }
        public string destinationIds { get; set; }
        public string destinationName { get; set; }
        public string priceMin { get; set; }
        public string priceMax { get; set; }
        public string departureIds { get; set; }
        public string departureName { get; set; }
        public string tripadvisorrating { get; set; }
        public  string sort { get; set; }
        public string hotelKeysToExclude { get; set; }
        public  string channelId { get; set; }
        public string channelName { get; set; }
        public string hotelKeys { get; set; }
        public  string tradingNameIds { get; set; }
        public string boardType { get; set; }
        public string queryId { get; set; }
        public string sectionId { get; set; }
        public string dateOffSet { get; set; }
        public string flexDates { get; set; }
        public string minPrice { get; set; }
        public string quoteRef { get; set; }
        public string description { get; set; }
        public string iff { get; set; }
        public string hotelName { get; set; }
        public string ID { get; set; }
        public string flag { get; set; }
        public string displaySectionName { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public bool isImageOverride { get; set; }
        public bool isFromDesktop { get; set; }
        public bool isFromMobile { get; set; }
        public bool isDynamic { get; set; }
        public string dynamicUrl { get; set; }
    }
    public class emailSubscription
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string airport { get; set; }
    }
    /* ******************************/
    
}