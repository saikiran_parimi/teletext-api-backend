﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class SearchQueryParameters
    {
        public string adults { get; set; }
        public string boardType { get; set; }
        public string children { get; set; }
        public string ratings { get; set; }
        public string duration { get; set; }
        public string departureIds { get; set; }
        public string channelId { get; set; }
        public int flexibilityDuration { get; set; }
        public int dateRolling { get; set; }
        public string labelName { get; set; }
        public string labelId { get; set; }
    }
}
