﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.Misc
{
    public class MHIDLocation
    {
        public double lat { get; set; }
        public double lon { get; set; }
    }

    public class MHIDAddress
    {
        public string AddressLineOne { get; set; }
        public string AddressLineTwo { get; set; }
        public string AddressLineThree { get; set; }
        public string AddressLineFour { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }

    public class MHIDContact
    {
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string EMailId { get; set; }
    }

    public class MHIDRegion
    {
        public int RegionId { get; set; }
        public int ResortId { get; set; }
        public string Regname { get; set; }
        public string RegionLink { get; set; }
        public int Reglevel { get; set; }
        public string AirportCodes { get; set; }
        public int ParentRegionId { get; set; }
        public string ParentRegionName { get; set; }
        public string ParentRegionLink { get; set; }
        public int GrandRegionId { get; set; }
        public string GrandRegionName { get; set; }
        public string GrandRegionLink { get; set; }
    }

    //public class Images
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    //public class MobileImages
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    //public class ThumbnailImages
    //{
    //    public int count { get; set; }
    //    public string Location { get; set; }
    //}

    public class MHIDSource
    {
        public int MasterHotelId { get; set; }
        public string BuildingName { get; set; }
        public string DestinationCode { get; set; }
        public string DestinationName { get; set; }
        public int MasterResortId { get; set; }
        public string MasterResortName { get; set; }
        public bool PopularHotel { get; set; }
        public int RatingLevel { get; set; }
        public int Rating { get; set; }
        public string GIATAID { get; set; }
        public string FriendlyUrl { get; set; }
        public int RegionTTSSLabelD { get; set; }
        public MHIDLocation Location { get; set; }
        public MHIDAddress Address { get; set; }
        public MHIDContact Contact { get; set; }
        public MHIDRegion Region { get; set; }
        public List<object> Features { get; set; }
        public int ImageCount { get; set; }
        public string Description { get; set; }
    }

    public class TripAdvisor
    {
        public int TAID { get; set; }
        public int reviewsCount { get; set; }
        public double TARating { get; set; }
        public int noOfRooms { get; set; }
        public List<string> hotelStyle { get; set; }
    }

    public class MHIDStaticInfo
    {
        public MHIDSource _source { get; set; }
        public string insertedTime { get; set; }
        public TripAdvisor TripAdvisor { get; set; }
        public bool Override { get; set; }
        public bool IsDirty { get; set; }
        public string ETag { get; set; }
        public SearchQueryParameters searchParams { get; set; }
        public List<ErrataInfo> errataData { get; set; }
    }

    public class ErrataInfo
    {
        public string errataDescription { get;set;}
        public string startDate { get; set; }
        public string endDate { get; set; }
    }

    public class TAInfo
    {
        public string numberOfReviews { get; set; }
        public string rating { get; set; }
        public int masterHotelId { get; set; }
        public int tripAdvisorId { get; set; }
    }
    public class MHDescription
    {
        public string description { get; set; }
        //public List<object> features { get; set;}
        public string MHID { get; set; }
        public string insertedTime { get; set; }
    }
    public class Geometry
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class TAFeatures
    {
        public List<string> highlights { get; set; }
        public List<string> aboutTheProperty { get; set; }
        public List<string> thingsToDo { get; set; }
        public List<string> roomtypes { get; set; }
        public List<string> inYourRoom { get; set; }
        public List<string> internet { get; set; }
        public List<string> services { get; set; }
        public Geometry geometry { get; set; }
        public int noOfRooms { get; set; }
        public List<string> hotelStyle { get; set; }
    }

    public class TAConfig
    {
        public string dateTimeLastUpdated { get; set; }
        public string defaultDate { get; set; }
    }
    public class TAUpdateList
    {      
        public List<int> updatedMHUIDs { get; set; }
        public int count { get; set; }
    }
    public class TAEtagResponse
    {
        public string content { get; set; }
        public string etag { get; set; }
    }

    public class TA
    {
        public int serialNumber { get; set; }
        public int giataCode { get; set; }
        public int tripAdvisorId { get; set; }
        public int masterHotelId { get; set; }
        public string lastModifiedDate { get; set; }
    }
    public class Akamai
    {
        public List<string> objects { get; set; }
        public string action { get; set; }
        public string type { get; set; }
        public string domain { get; set; }

    }
    public class AkamaiPurge
    {
        public string url { get; set; }
        public string email { get; set; }
    }


}
