﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class APISearchRequest
    {
        //IE CODE
        public string countrySite { get; set; }
        public int destinationIds { get; set; }
        public int departureIds { get; set; }
        public List<string> boardType { get; set; }
        public DateTime departureDate { get; set; }
        public DateTime dateMin { get; set; }
        public DateTime dateMax { get; set; }
        public int durationMin { get; set; }
        public int durationMax { get; set; }
        public int adults { get; set; }
        public int children { get; set; }
        public int priceMin { get; set; }
        public int priceMax { get; set; }
        public List<string> ratings { get; set; }
        public List<string> tradingNameIds { get; set; }
        public string destinationType { get; set; }
        public List<string> hotelKeys { get; set; }
        public int tripAdvisorRating { get; set; }
        public List<string> sort { get; set; }
        public List<int> hotelKeysToExclude { get; set; }
        public int channelId { get; set; }
        public int labelId { get; set; }
        public List<int> usersPreferredHotelKeys { get; set; }
        public bool skipFacets { get; set; }
        public bool isShortDescriptionRequired { get; set; }
        public string platform { get; set; }
        public string os { get; set; }
        public int timeout { get; set; }
        public bool longDateRangeQuery { get; set; }
        public int cacheHotTime { get; set; }
        public string navigatedPage { get; set; }
        public string pageSource { get; set;}
        public int resetStage { get; set; }
        public Dictionary<int, string> mhidsWithComments { get; set; }
        public bool isDirectionalSellingValue { get; set; }
        public APISearchRequest()
        {
            //ToDo: Set default values to the properties
        }

        public APISearchRequest(int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string isShortDescriptionRequired, 
                                       string platform, string os, int timeout, 
                                       bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, Dictionary<int, string> MhidCommentsDic, bool isDirectionalSellingValue)
        {
            if (isDirectionalSellingValue)
            {
                this.isDirectionalSellingValue = isDirectionalSellingValue;
                mhidsWithComments = MhidCommentsDic;
            }

            //IE Code - defaulting countrySite to UK
            this.countrySite = "UK";
            this.usersPreferredHotelKeys = new List<int>();
            if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                this.usersPreferredHotelKeys = usersPreferredHotelKeys.Split(',').Select(int.Parse).ToList();
            }
            if (string.Compare(skipFacets, "true") == 0)
            {
                this.skipFacets = true;
            }
            else
            {
                this.skipFacets = false;
            }
            if(!string.IsNullOrWhiteSpace(sort))
            {
                this.sort = sort.Split(',').ToList();
            }
            this.channelId = channelId;
            this.labelId = labelId;
            this.hotelKeysToExclude = new List<int>();
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                this.hotelKeysToExclude = hotelKeysToExclude.Split(',').Select(int.Parse).ToList();
            }
            this.tripAdvisorRating = tripAdvisorRating;
            this.hotelKeys = new List<string>();
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                this.hotelKeys = hotelKeys.Split(',').ToList();
            }
            if (string.IsNullOrEmpty(destinationType))
            {
                this.destinationType = ConfigurationManager.AppSettings["DefaultDestinationType"];
            }
            else
            {
                this.destinationType = destinationType;
            }
            this.tradingNameIds = new List<string>();
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                this.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();
            }
            else
            {
                this.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            this.ratings = new List<string>();
            if (string.IsNullOrWhiteSpace(ratings))
            {
                this.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                this.ratings = ratings.Split(',').ToList();
            }
            if (durationMax == 0)
            {
                this.durationMin = 7;
                this.durationMax = 7;
            }
            else
            {
                this.durationMin = durationMin;
                this.durationMax = durationMax;
            }
            if (adults == 0)
            {
                this.adults = 2;
            }
            else
            {
                this.adults = adults;
            }
            this.children = children;
            if (string.IsNullOrWhiteSpace(isShortDescriptionRequired))
            {
                this.isShortDescriptionRequired = false;
            }
            else
            {
                this.isShortDescriptionRequired = true;
            }
            this.priceMax = priceMax;
            this.priceMin = priceMin;
            this.departureDate = DateTime.Parse(departureDate);
            this.dateMax = DateTime.Parse(dateMax);
            this.dateMin = DateTime.Parse(dateMin);
            this.boardType = new List<string>();
            if (string.IsNullOrWhiteSpace(boardType))
            {
                this.boardType = ConfigurationManager.AppSettings["DefaultBoardType"].Split(',').ToList();
            }
            else
            {
                this.boardType = boardType.Split(',').ToList();
            }
            this.departureIds = departureIds;
            this.destinationIds = destinationIds;
            if (string.IsNullOrWhiteSpace(platform))
            {
                this.platform = "desktop";
            }
            else
            {
                this.platform = platform.ToLower();
            }
            if (string.IsNullOrWhiteSpace(os))
            {
                this.os = "windows";
            }
            else
            {
                this.os = os.ToLower();
            }
            if (timeout != 0)
            {
                this.timeout = timeout;
            }
            this.longDateRangeQuery = longDateRangeQuery;
            if (cacheHotTime == 0)
            {
                this.cacheHotTime = int.Parse(ConfigurationManager.AppSettings["DefaultCacheHotTime"]);
            }
            else
            {
                this.cacheHotTime = cacheHotTime;
            }
            if(string.IsNullOrWhiteSpace(navigatedPage))
            {
                this.navigatedPage = "HomePage";
            }
            else
            {
                this.navigatedPage = navigatedPage;
            }
            if (string.IsNullOrWhiteSpace(pageSource))
            {
                this.pageSource = "Serp/Fod";
            }
            else
            {
                this.pageSource = pageSource;
            }
            // reset logic stage to reduce null offers
            resetStage = -1;
        }

        //IE Code - added ctor with countrySite parameter
        public APISearchRequest(string countrySite, int destinationIds, int departureIds, string boardType, string departureDate, string dateMin,
                                       string dateMax, int durationMin, int durationMax, int adults, int children, int priceMin,
                                       int priceMax, string ratings, string tradingNameIds, string destinationType, string hotelKeys,
                                       int tripAdvisorRating, string hotelKeysToExclude, string sort, int channelId, int labelId,
                                       string usersPreferredHotelKeys, string skipFacets, string isShortDescriptionRequired,
                                       string platform, string os, int timeout,
                                       bool longDateRangeQuery, int cacheHotTime, string navigatedPage, string pageSource, Dictionary<int, string> MhidCommentsDic, bool isDirectionalSellingValue)
        {
            if (!string.IsNullOrWhiteSpace(countrySite))
            {
                this.countrySite = countrySite;
            }
            else
            {
                this.countrySite = "UK";
            }

            if (isDirectionalSellingValue)
            {
                this.isDirectionalSellingValue = isDirectionalSellingValue;
                mhidsWithComments = MhidCommentsDic;
            }

            this.usersPreferredHotelKeys = new List<int>();
            if (!string.IsNullOrWhiteSpace(usersPreferredHotelKeys))
            {
                this.usersPreferredHotelKeys = usersPreferredHotelKeys.Split(',').Select(int.Parse).ToList();
            }
            if (string.Compare(skipFacets, "true") == 0)
            {
                this.skipFacets = true;
            }
            else
            {
                this.skipFacets = false;
            }
            if (!string.IsNullOrWhiteSpace(sort))
            {
                this.sort = sort.Split(',').ToList();
            }
            this.channelId = channelId;
            this.labelId = labelId;
            this.hotelKeysToExclude = new List<int>();
            if (!string.IsNullOrWhiteSpace(hotelKeysToExclude))
            {
                this.hotelKeysToExclude = hotelKeysToExclude.Split(',').Select(int.Parse).ToList();
            }
            this.tripAdvisorRating = tripAdvisorRating;
            this.hotelKeys = new List<string>();
            if (!string.IsNullOrWhiteSpace(hotelKeys))
            {
                this.hotelKeys = hotelKeys.Split(',').ToList();
            }
            if (string.IsNullOrEmpty(destinationType))
            {
                this.destinationType = ConfigurationManager.AppSettings["DefaultDestinationType"];
            }
            else
            {
                this.destinationType = destinationType;
            }
            this.tradingNameIds = new List<string>();
            if (string.IsNullOrWhiteSpace(tradingNameIds))
            {
                string tradingIdWebConfigKey = "DefaultTradingNameIds";
                if (countrySite != "UK")
                {
                    tradingIdWebConfigKey += countrySite.ToUpper();
                }
                this.tradingNameIds = ConfigurationManager.AppSettings[tradingIdWebConfigKey].Split(',').ToList();

                //this.tradingNameIds = ConfigurationManager.AppSettings["DefaultTradingNameIds"].Split(',').ToList();
            }
            else
            {
                this.tradingNameIds = tradingNameIds.Split(',').ToList();
            }
            this.ratings = new List<string>();
            if (string.IsNullOrWhiteSpace(ratings))
            {
                this.ratings = ConfigurationManager.AppSettings["DefaultRatings"].Split(',').ToList();
            }
            else
            {
                this.ratings = ratings.Split(',').ToList();
            }
            if (durationMax == 0)
            {
                this.durationMin = 7;
                this.durationMax = 7;
            }
            else
            {
                this.durationMin = durationMin;
                this.durationMax = durationMax;
            }
            if (adults == 0)
            {
                this.adults = 2;
            }
            else
            {
                this.adults = adults;
            }
            this.children = children;
            if (string.IsNullOrWhiteSpace(isShortDescriptionRequired))
            {
                this.isShortDescriptionRequired = false;
            }
            else
            {
                this.isShortDescriptionRequired = true;
            }
            this.priceMax = priceMax;
            this.priceMin = priceMin;
            this.departureDate = DateTime.Parse(departureDate);
            this.dateMax = DateTime.Parse(dateMax);
            this.dateMin = DateTime.Parse(dateMin);
            this.boardType = new List<string>();
            if (string.IsNullOrWhiteSpace(boardType))
            {
                this.boardType = ConfigurationManager.AppSettings["DefaultBoardType"].Split(',').ToList();
            }
            else
            {
                this.boardType = boardType.Split(',').ToList();
            }
            this.departureIds = departureIds;
            this.destinationIds = destinationIds;
            if (string.IsNullOrWhiteSpace(platform))
            {
                this.platform = "desktop";
            }
            else
            {
                this.platform = platform.ToLower();
            }
            if (string.IsNullOrWhiteSpace(os))
            {
                this.os = "windows";
            }
            else
            {
                this.os = os.ToLower();
            }
            if (timeout != 0)
            {
                this.timeout = timeout;
            }
            this.longDateRangeQuery = longDateRangeQuery;
            if (cacheHotTime == 0)
            {
                this.cacheHotTime = int.Parse(ConfigurationManager.AppSettings["DefaultCacheHotTime"]);
            }
            else
            {
                this.cacheHotTime = cacheHotTime;
            }
            if (string.IsNullOrWhiteSpace(navigatedPage))
            {
                this.navigatedPage = "HomePage";
            }
            else
            {
                this.navigatedPage = navigatedPage;
            }
            if (string.IsNullOrWhiteSpace(pageSource))
            {
                this.pageSource = "Serp/Fod";
            }
            else
            {
                this.pageSource = pageSource;
            }
            // reset logic stage to reduce null offers
            resetStage = -1;
        }
    }
}
