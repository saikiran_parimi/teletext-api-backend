﻿using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class StaticResponseParms
    {
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public string tripAdvisorId { get; set; }
        public float averageRating { get; set; }
        public int reviewCount { get; set; }
        public List<object> features { get; set; }
        public string name { get; set; }
        public int mhid { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public List<ErrataInfo> errataData { get; set; }
    }
}
