﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class SearchResponseParams
    {

        public class SearchRequestResponseShards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int failed { get; set; }
        }

        public class SearchRequestResponseSource
        {
            public int adults { get; set; }
            public List<string> boardType { get; set; }
            public int children { get; set; }
            public string departureDate { get; set; }
            public string dateMin { get; set; }
            public string dateMax { get; set; }
            public int departureIds { get; set; }
            public List<string> airportIds { get; set; }
            public List<string> regionIds { get; set; }
            public int destinationIds { get; set; }
            public int durationMin { get; set; }
            public int durationMax { get; set; }
            public int ttssCount { get; set; }
            public int parentDepartureIds { get; set; }
            public int priceMax { get; set; }
            public int priceMin { get; set; }
            public double ttssResponseTime { get; set; }
            public string destinationType { get; set; }
            public bool isRegion { get; set; }
            public int taRating { get; set; }
            public int channelId { get; set; }
            public int labelId { get; set; }
            public bool isCacheHit { get; set; }
            public List<string> ratings { get; set; }
            public List<string> tradingNameIds { get; set; }
            public string timeStamp { get; set; }
            public string ttssURL { get; set; }
            public double indexingTime { get; set; }
            public double decoratingTime { get; set; }
            public int offersFound { get; set; }
            public int offersDecorated { get; set; }
            public int offersInserted { get; set; }
            public List<int> hotelKeys { get; set; }
            public List<int> usersPreferredHotelKeys { get; set; }
            public bool skipFacets { get; set; }
            public string endPoint { get; set; }
            public List<int> hotelKeysToExclude { get; set; }
            public object sort { get; set; }
            public bool isHotelKeys { get; set; }
            public int offersReturned { get; set; }
            public string searchURL { get; set; }
            public string platform { get; set; }
            public string os { get; set; }
            public bool longDateRangeQuery { get; set; }
            public int cacheHotTime { get; set; }
            public string navigatedPage { get; set; }
            public string pageSource { get; set; }
            public bool isBulkInsertionOnRetry { get; set; }
            public int facetsTrails { get; set; }
            public int resetStage { get; set; }
        }

        public class SearchRequestResponseHit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public double _score { get; set; }
            public SearchRequestResponseSource _source { get; set; }
        }

        public class SearchRequestResponseHits
        {
            public int total { get; set; }
            public double? max_score { get; set; }
            public List<SearchRequestResponseHit> hits { get; set; }
        }

        public class SearchRequestResponse
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public SearchRequestResponseShards _shards { get; set; }
            public SearchRequestResponseHits hits { get; set; }
        }

    }
}
