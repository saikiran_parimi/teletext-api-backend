﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class SearchRequestAvailability
    {
        public int ttssCount { get; set; }
        public int resetStage { get; set; }
    }
}
