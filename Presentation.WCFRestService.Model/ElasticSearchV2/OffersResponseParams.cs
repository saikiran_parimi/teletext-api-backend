﻿using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class OffersResponseParams
    {

        public class ORPShards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int failed { get; set; }
        }

        public class OffersResponseHits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<object> hits { get; set; }
        }

        public class ORPMinPrice
        {
            public double value { get; set; }
        }

        public class ORPJourney
        {
            public int duration { get; set; }
            public string destination { get; set; }
            public string glat { get; set; }
            public string glong { get; set; }
            public string parentRegion { get; set; }
            public int parentRegionId { get; set; }
            public DateTime outboundDepartureDate { get; set; }
            public DateTime outboundArrivalDate { get; set; }
            public string outboundDepartureAirportName { get; set; }
            public string outboundDepartureAirportCode { get; set; }
            public string outboundArrivalAirportName { get; set; }
            public string outboundArrivalAirportCode { get; set; }
            public string outboundLegCount { get; set; }
            public string outboundFlightDuration { get; set; }
            public string outboundAirlineICAO { get; set; }
            public string outboundFlightNumber { get; set; }
            public DateTime inboundDepartureDate { get; set; }
            public DateTime inboundArrivalDate { get; set; }
            public string inboundDepartureAirportName { get; set; }
            public string inboundDepartureAirportCode { get; set; }
            public string inboundArrivalAirportName { get; set; }
            public string inboundArrivalAirportCode { get; set; }
            public string inboundLegCount { get; set; }
            public string inboundFlightDuration { get; set; }
            public string inboundAirlineICAO { get; set; }
            public string inboundFlightNumber { get; set; }
            public string adultBagPrice { get; set; }
            public string childBagPrice { get; set; }
            /*//Original
            public DateTime outboundArrivalDate { get; set; }
            public DateTime inboundDepartureDate { get; set; }
            public DateTime departureDate { get; set; }
            public DateTime returnDate { get; set; }
            public int duration { get; set; }
            public string departure { get; set; }
            public string gatewayName { get; set; }
            public string destination { get; set; }
            public string glat { get; set; }
            public string glong { get; set; }
            public string parentRegion { get; set; }
            public int parentRegionId { get; set; }
            public string gatewayCode { get; set; } */
        }

        public class ORPAccommodation
        {
            public string boardTypeCode { get; set; }
            public int boardTypeId { get; set; }
            public int adults { get; set; }
            public int children { get; set; }
            public int rating { get; set; }
            public DateTime updated { get; set; }
        }

        public class ORPRating
        {
            public string tripAdvisorId { get; set; }
            public float averageRating { get; set; }
            public int reviewCount { get; set; }
        }

        public class ORPHotel
        {
            public int iff { get; set; }
            public List<object> features { get; set; }
            public string name { get; set; }
            public ORPRating rating { get; set; }
        }

        public class ORPSource
        {
            public string id { get; set; }
            public string quoteRef { get; set; }
            public int price { get; set; }
            public int rank { get; set; }
            public DateTime accommodationUpdateTime { get; set; }
            public bool isPreferential { get; set; }
            public ORPJourney journey { get; set; }
            public ORPAccommodation accommodation { get; set; }
            public ORPHotel hotel { get; set; }
            public List<int> tradingNameId { get; set; }
            public int departureIds { get; set; }
            public int parentDepartureIds { get; set; }
            public int regionId { get; set; }
            public int labelId { get; set; }
            public string timeStamp { get; set; }
        }

        public class ORPHit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public object _score { get; set; }
            public ORPSource _source { get; set; }
            public List<int> sort { get; set; }
        }

        public class ORPHHits
        {
            public int total { get; set; }
            public object max_score { get; set; }
            public List<ORPHit> hits { get; set; }
        }

        public class ORPTopPrice
        {
            public ORPHHits hits { get; set; }
        }

        public class ORPHotelBucket
        {
            public int key { get; set; }
            public int doc_count { get; set; }
            public ORPMinPrice min_price { get; set; }
            public ORPTopPrice top_price { get; set; }
        }

        public class ORPHotelIff
        {
            public int doc_count_error_upper_bound { get; set; }
            public int sum_other_doc_count { get; set; }
            public List<ORPHotelBucket> buckets { get; set; }
        }

        public class ORPAggregations
        {
            public ORPHotelIff hotel_iff { get; set; }
        }

        public class OffersResponse
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public ORPShards _shards { get; set; }
            public OffersResponseHits hits { get; set; }
            public ORPAggregations aggregations { get; set; }
        }

    }
}
