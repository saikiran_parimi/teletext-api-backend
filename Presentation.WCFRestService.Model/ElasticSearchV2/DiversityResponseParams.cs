﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class DiversityResponseParams
    {
        public class DiversityShards
        {
            public int total { get; set; }
            public int successful { get; set; }
            public int failed { get; set; }
        }

        public class DiversityHits
        {
            public int total { get; set; }
            public double max_score { get; set; }
            public List<object> hits { get; set; }
        }

        public class DiversityMinPrice
        {
            public double value { get; set; }
        }

        public class DiversityJourney
        {
            public string outboundDepartureDate { get; set; }
        }

        public class Source
        {
            public DiversityJourney journey { get; set; }
            public int price { get; set; }
            public string quoteRef { get; set; }
        }

        public class Hit
        {
            public string _index { get; set; }
            public string _type { get; set; }
            public string _id { get; set; }
            public object _score { get; set; }
            public Source _source { get; set; }
            public List<int> sort { get; set; }
        }

        public class TopPriceHits
        {
            public int total { get; set; }
            public object max_score { get; set; }
            public List<Hit> hits { get; set; }
        }

        public class TopPrice
        {
            public TopPriceHits hits { get; set; }
        }

        public class BoardTypeBucket
        {
            public int key { get; set; }
            public int doc_count { get; set; }
            public DiversityMinPrice min_price { get; set; }
            public TopPrice top_price { get; set; }
        }

        public class GroupByCategory
        {
            public int doc_count_error_upper_bound { get; set; }
            public int sum_other_doc_count { get; set; }
            public List<BoardTypeBucket> buckets { get; set; }
        }

        public class IFFBucket
        {
            public int key { get; set; }
            public int doc_count { get; set; }
            public GroupByCategory group_by_category { get; set; }
        }

        public class GroupByIff
        {
            public int doc_count_error_upper_bound { get; set; }
            public int sum_other_doc_count { get; set; }
            public List<IFFBucket> buckets { get; set; }
        }

        public class DiversityAggregations
        {
            public GroupByIff group_by_iff { get; set; }
        }

        public class DiversityResponse
        {
            public int took { get; set; }
            public bool timed_out { get; set; }
            public DiversityShards _shards { get; set; }
            public DiversityHits hits { get; set; }
            public DiversityAggregations aggregations { get; set; }
        }
    }
}
