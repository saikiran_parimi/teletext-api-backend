﻿using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.WCFRestService.Model.ElasticSearchV2
{
    public class HolidayOffers
    {
        //public int total { get; set; }
        public List<HOffer> offers { get; set; }
        public Facets facets { get; set; }
        public int totalHotels { get; set; }
        public int resetStage { get; set; }
        public bool isOfferFound { get; set; }
    }
    public class Facets
    {
        public List<FacetHotel> hotels { get; set; }
        public List<Airport> airports { get; set; }
        public Pricerange priceRange { get; set; }
        public List<Resort> resorts { get; set; }
        //public int totalHotels { get; set; }
    }
    public class Resort
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public int count { get; set; }
    }
    public class Pricerange
    {
        public int min { get; set; }
        public int max { get; set; }
    }
    public class Airport
    {
        public string name { get; set; }
        public int departureId { get; set; }
        public int count { get; set; }
    }
    public class FacetHotel
    {
        public string name { get; set; }
        public int destinationId { get; set; }
        public string hotelKey { get; set; }
        public int count { get; set; }
    }
    public class HOffer
    {
        public string id { get; set; } //TTSS
        public string quoteRef { get; set; } //TTSS
        public int price { get; set; } //TTSS
        public int rank { get; set; }
        public DateTime accommodationUpdateTime { get; set; }
        public bool isPreferential { get; set; }  //if TAId = 576 true else false
        public string phone { get; set; }  //TTSS
        public Journey journey { get; set; }
        public Accommodation accommodation { get; set; }
        public OfferHotel hotel { get; set; }
        //public string hotelOperator { get; set; }
        //public string contentId { get; set; }
        public string tradingNameId { get; set; } //TTSS
        //public object score { get; set; }
        public bool isPreferredByUser { get; set; }
        public string comment { get; set; }
        public List<ErrataInfo> errataData { get; set; }
    }
    
    public class Journey
    {
        public int duration { get; set; }
        public string destination { get; set; }
        public string glat { get; set; }
        public string glong { get; set; }
        public string parentRegion { get; set; }
        public int parentRegionId { get; set; }
        public DateTime outboundDepartureDate { get; set; }
        public DateTime outboundArrivalDate { get; set; }
        public string outboundDepartureAirportName { get; set; }
        public string outboundDepartureAirportCode { get; set; }
        public string outboundArrivalAirportName { get; set; }
        public string outboundArrivalAirportCode { get; set; }
        public string outboundLegCount { get; set; }
        public string outboundFlightDuration { get; set; }
        public string outboundAirlineICAO { get; set; }
        public string outboundAirlines { get; set; }
        public string outboundFlightNumber { get; set; }
        public DateTime inboundDepartureDate { get; set; }
        public DateTime inboundArrivalDate { get; set; }
        public string inboundDepartureAirportName { get; set; }
        public string inboundDepartureAirportCode { get; set; }
        public string inboundArrivalAirportName { get; set; }
        public string inboundArrivalAirportCode { get; set; }
        public string inboundLegCount { get; set; }
        public string inboundFlightDuration { get; set; }
        public string inboundAirlineICAO { get; set; }
        public string inboundAirlines { get; set; }
        public string inboundFlightNumber { get; set; }
        public string adultBagPrice { get; set; }
        public string childBagPrice { get; set; }

        // Duplicate fields for the app --- Start
        public DateTime departureDate { get; set; }
        public string departure { get; set; }
        public string gatewayCode { get; set; }
        public string gatewayName { get; set; }
        public DateTime returnDate { get; set; }
        // Duplicate fields for the app --- End

        /*//ORIGINAL
        public DateTime outboundArrivalDate { get; set; } //TTSS
        public DateTime inboundDepartureDate { get; set; } //TTSS
        public DateTime departureDate { get; set; } //TTSS
        public DateTime returnDate { get; set; } //TTSS
        public int duration { get; set; } //TTSS
        public string departure { get; set; } //TTSS
        public string gatewayName { get; set; } //TTSS
        //public string flightOperator { get; set; }
        public string destination { get; set; }  //TTSS
        public string glat { get; set; }  //TTSS
        public string glong { get; set; } //TTSS
        public string parentRegion { get; set; } //Static
        public int parentRegionId { get; set; }  //Static
        public string gatewayCode { get; set; } //TTSS
        //public bool inboundFlightDirect { get; set; }
        //public bool outboundFlightDirect { get; set; } */
    }
    public class Accommodation
    {
        public string boardTypeCode { get; set; } //TTSS
        public int adults { get; set; }  //TTSS
        public int children { get; set; }  //TTSS
        public int rating { get; set; }  //TTSS
        public DateTime updated { get; set; }  //TTSS
    }
    public class OfferHotel
    {
        public string iff { get; set; } //TTSS
        //public string latlon { get; set; }
        //public string starRating { get; set; }
        public List<string> images { get; set; } // Mapping
        public List<string> mobileimages { get; set; } //Mapping
        public List<string> thumbnailimages { get; set; } //Mapping
        //public string description { get; set; }
        public List<object> features { get; set; } //static
        public string name { get; set; } //static
        public Rating rating { get; set; }
        public string shortDescription { get; set; }
        public string landingPageURL { get; set; }
    }
    public class Rating
    {
        public string tripAdvisorId { get; set; } //Static
        public float averageRating { get; set; } //Static
        public int reviewCount { get; set; } //Static
    }
}
