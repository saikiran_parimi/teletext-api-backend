﻿using Amazon.S3;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace Presentation.Web.WCFRestServices
{
    public class MarketingService
    {
        public static NewsLetterSubscriptionStatus GetNewsLetterSubscription(string name, string email, string phone, string airport)
        {
            NewsLetterSubscriptionStatus status = new NewsLetterSubscriptionStatus();
            IMarketingService marketingService = new SmartFocusServices();
            status = marketingService.GetNewsLetterSubscription(name, email, phone, airport);            
            return status;
        }

        public static string GetNewsLetterSubscriberByEmail(string email)
        {
            IMarketingService marketingService = new SmartFocusServices();
            return marketingService.GetNewsLetterSubscriberByEmail(email);
        }

        public static bool SaveNewsletterSubscriptionToS3(NewsletterSubscriptionModel newsletterSubscription, string value = null)
        {
            List<NewsletterSubscriptionModel> newsletterSubscriptionList = new List<NewsletterSubscriptionModel>();
            string newsletterSubscriptionBucket = ConfigurationManager.AppSettings["NewsletterSubscriptionBucket"];
            string newsletterSubscriptionFileName = ConfigurationManager.AppSettings["NewsletterSubscriptionFileName"];
            string newsletterSubscriptionPath = string.Empty;

            if (newsletterSubscription.channel.ToLower().Equals("uk", StringComparison.InvariantCultureIgnoreCase))
            {
                newsletterSubscriptionPath = ConfigurationManager.AppSettings["NewsletterSubscriptionPathUK"];
            }
            else if (newsletterSubscription.channel.ToLower().Equals("ie", StringComparison.InvariantCultureIgnoreCase))
            {
                newsletterSubscriptionPath = ConfigurationManager.AppSettings["NewsletterSubscriptionPathIE"];
            }

            try
            {
                var response = string.Empty;
                using (StreamReader reader = new StreamReader(Utilities.DownloadS3Object(newsletterSubscriptionBucket, string.Format("{0}/", newsletterSubscriptionPath) + newsletterSubscriptionFileName + DateTime.UtcNow.ToString(ConfigurationManager.AppSettings["NewsletterSubscriptionDateFormat"]) + ConfigurationManager.AppSettings["NewsletterSubscriptionJsonExtension"])))
                {
                    response = reader.ReadToEnd();
                }
                newsletterSubscriptionList = JsonConvert.DeserializeObject<List<NewsletterSubscriptionModel>>(response);
                newsletterSubscriptionList.Add(newsletterSubscription);

                try
                {
                    if (!string.IsNullOrEmpty(Utilities.PutObjectToS3(newsletterSubscriptionBucket, newsletterSubscriptionFileName + DateTime.UtcNow.ToString(ConfigurationManager.AppSettings["NewsletterSubscriptionDateFormat"]) + ConfigurationManager.AppSettings["NewsletterSubscriptionJsonExtension"], newsletterSubscriptionList, string.Format("{0}/", newsletterSubscriptionPath))))
                    {
                        ErrorLogger.Log("The Newsletter Subscription successfully updated to S3", LogLevel.Information);
                        return true;
                    }
                    else
                    {
                        ErrorLogger.Log("Error occurred while uploading the object to S3.", LogLevel.Error);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("Unable to update the Newsletter Subscription data to S3 " + ex.Message, LogLevel.Error);
                    return false;
                }
            }
            catch (AmazonS3Exception ex)
            {
                ErrorLogger.Log("The Newsletter Subscription Happening for the First Time for the day.", LogLevel.Information);
                if (ex.InnerException.Message.Contains("404"))
                {
                    newsletterSubscriptionList.Add(newsletterSubscription);
                    if (!string.IsNullOrEmpty(Utilities.PutObjectToS3(newsletterSubscriptionBucket, newsletterSubscriptionFileName + DateTime.UtcNow.ToString(ConfigurationManager.AppSettings["NewsletterSubscriptionDateFormat"]) + ConfigurationManager.AppSettings["NewsletterSubscriptionJsonExtension"], newsletterSubscriptionList, string.Format("{0}/", newsletterSubscriptionPath))))
                    {
                        ErrorLogger.Log("The Newsletter Subscription successfully updated to S3", LogLevel.Information);
                        return true;
                    }
                    else
                    {
                        ErrorLogger.Log("Error occurred while uploading the object to S3 " + ex.Message, LogLevel.Error);
                        return false;
                    }
                }
                else
                {
                    ErrorLogger.Log("Unable to update the Newsletter Subscription", LogLevel.Error);
                    return false;
                }
            }
        }
    }
}