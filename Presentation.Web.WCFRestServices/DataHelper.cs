﻿
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Presentation.Web.WCFRestServices.DataAccessHelper
{
    static class DataHelper
    {  
        public static DataSet ExecuteQuery(string procName , string connString)
        {
            string connectionString = connString;

            DataSet ds = null;

            SqlConnection con = null;
            try
            {
                con = new SqlConnection(connectionString);

                con.Open();

                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandTimeout = 300;
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                SqlDataAdapter adap = new SqlDataAdapter(com);
                ds = new DataSet();

                adap.Fill(ds);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);

                con.Close();

                throw ex;
            }

            con.Close();

            return ds;
        }

        public static DataSet ExecuteQuery(string procName, string connString, List<SqlParameter> procParams = null)
        {
            string connectionString = connString;

            DataSet ds = null;

            SqlConnection con = null;
            try
            {
                con = new SqlConnection(connectionString);

                con.Open();

                SqlCommand com = new SqlCommand();
                com.Connection = con;
                com.CommandTimeout = 300;
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                if (procParams != null && procParams.Count != 0)
                {
                    foreach (SqlParameter param in procParams)
                    {
                        com.Parameters.Add(param);
                    }
                }

                SqlDataAdapter adap = new SqlDataAdapter(com);
                ds = new DataSet();

                adap.Fill(ds);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);

                con.Close();

                throw ex;
            }

            con.Close();

            return ds;
        }

        public static void ExecuteCommand(string procName, string ConnectionString, List<SqlParameter> procParams = null)
        {
            SqlConnection con = new SqlConnection(ConnectionString);

            con.Open();

            SqlCommand com = new SqlCommand();

            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = procName;

            if (procParams != null && procParams.Count != 0)
            {
                foreach (SqlParameter param in procParams)
                {
                    com.Parameters.Add(param);
                }
            }

            try
            {
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);

                con.Close();

                throw ex;
            }

            con.Close();
        }

        public static object ExecuteScalar(string procName, string ConnectionString, List<SqlParameter> procParams = null)
        {
            SqlConnection con = new SqlConnection(ConnectionString);

            con.Open();

            SqlCommand com = new SqlCommand();

            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = procName;

            if (procParams != null && procParams.Count != 0)
            {
                foreach (SqlParameter param in procParams)
                {
                    com.Parameters.Add(param);
                }
            }

            object result = null;

            try
            {
                result = com.ExecuteScalar();
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);

                con.Close();

                throw ex;
            }

            con.Close();

            return result;
        }

    }
}
