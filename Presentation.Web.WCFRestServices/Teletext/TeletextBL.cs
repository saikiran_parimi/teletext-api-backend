﻿using Presentation.WCFRestService.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;

namespace Presentation.Web.WCFRestServices
{
    public class TeletextBL
    {
        public static List<Slider> GetHomepageSlider()
        {
            List<Slider> sliderList = TeletextDL.GetHomepageSlider();
            List<ArtirixDataItem> artirixData = Utilities.GetArtirixHomepageData();
            foreach(Slider sl in sliderList)
            {
                int sliderTabIndex = sl.SliderTabIndex;
                sl.SliderDesktopImageOriginalFilePath = string.Format(ConfigurationManager.AppSettings["ImageCDNPath"], sl.SliderDesktopImageOriginalFilePath);
                sl.SliderMobileImageOriginalFilePath = string.Format(ConfigurationManager.AppSettings["ImageCDNPath"], sl.SliderMobileImageOriginalFilePath);
                foreach(SliderPlaceHolder pl in sl.PlaceHolders)
                {
                    int placeHolderIndex = pl.SliderPlaceholderTabIndex;
                    string targetPlaceHolder = string.Format("homepage_merchandisingbox{0}_slot{1}", sliderTabIndex, placeHolderIndex);
                    ArtirixDataItem dataItem = artirixData.FirstOrDefault(a => a.query.name == targetPlaceHolder);
                    if (dataItem != null)
                    {
                        pl.SliderPlaceholderText = dataItem.query.description.Replace("{price}", dataItem.minPrice.ToString());
                        pl.SliderPlaceholderLink = dataItem.query.websiteUrl;
                    }
                }
            }

            return sliderList;
        }

        public static OTP GetCustomerAuthOTP(string mobileNo)
        {
            OTP otp = new OTP();
            DataSet result = TeletextDL.GetCustomerAuthOTP(mobileNo);
            if (result.Tables[0].Rows.Count > 0)
            {
                otp.OTPValue = result.Tables[0].Rows[0]["OTP"].ToString();
                otp.ExpirationTime = (DateTime)result.Tables[0].Rows[0]["ExpirationTime"];
            }
            return otp;
        }
        public static AuthStatus AuthCustomerOTP(string mobileNo, string otpr)
        {
            AuthStatus otps = new AuthStatus();
            DataSet result = TeletextDL.AuthCustomerOTP(mobileNo, otpr);
            if (result.Tables[0].Rows.Count > 0)
            {
                otps.Status = result.Tables[0].Rows[0]["Status"].ToString();
            }
            return otps;
        }
        
    }
}