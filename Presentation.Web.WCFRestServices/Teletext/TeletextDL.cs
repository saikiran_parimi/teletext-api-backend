﻿using System;
using System.Collections.Generic;
using System.Data;
using Presentation.Web.WCFRestServices.DataAccessHelper;
using System.Configuration;
using System.Data.SqlClient;
using Presentation.WCFRestService.Model.Enum;
namespace Presentation.Web.WCFRestServices
{
    public class TeletextDL
    {
        public static List<Slider> GetHomepageSlider()
        {
            List<Slider> sliderList = new List<Slider>();
            try
            {
                DataSet ds = DataHelper.ExecuteQuery("iPro_sp_GetHomepageSliders", ConfigurationManager.AppSettings["CMSConnString"]);
                DataView dv = ds.Tables[0].DefaultView;
                dv.Sort = "SliderTabIndex asc";
                DataTable sortedDT = dv.ToTable();
                foreach (DataRow dr in sortedDT.Rows)
                {
                    Slider slider = new Slider();
                    slider.SliderId = dr["SliderId"] != System.DBNull.Value ? (int)dr["SliderId"] : 0;
                    slider.SliderTabIndex = dr["SliderTabIndex"] != System.DBNull.Value ? (int)dr["SliderTabIndex"] : 0;
                    slider.SliderHeading = dr["SliderHeading"] != System.DBNull.Value ? (string)dr["SliderHeading"] : "";
                    slider.SliderSubheading = dr["SliderSubheading"] != System.DBNull.Value ? (string)dr["SliderSubheading"] : "";
                    slider.SliderFooterText = dr["SliderFooterText"] != System.DBNull.Value ? (string)dr["SliderFooterText"] : "";
                    slider.SliderFooterLink = dr["SliderFooterLink"] != System.DBNull.Value ? (string)dr["SliderFooterLink"] : "";
                    slider.SliderDesktopImageId = dr["SliderDesktopImageId"] != System.DBNull.Value ? (int)dr["SliderDesktopImageId"] : 0;
                    slider.SliderDesktopImageName = dr["SliderDesktopImageName"] != System.DBNull.Value ? (string)dr["SliderDesktopImageName"] : "";
                    slider.SliderDesktopImageAltText = dr["SliderDesktopImageAltText"] != System.DBNull.Value ? (string)dr["SliderDesktopImageAltText"] : "";
                    slider.SliderDesktopImageOriginalFilePath = dr["SliderDesktopImageOriginalFilePath"] != System.DBNull.Value ? (string)dr["SliderDesktopImageOriginalFilePath"] : "";
                    slider.SliderDesktopImageId = dr["SliderMobileImageId"] != System.DBNull.Value ? (int)dr["SliderMobileImageId"] : 0;
                    slider.SliderMobileImageName = dr["SliderMobileImageName"] != System.DBNull.Value ? (string)dr["SliderMobileImageName"] : "";
                    slider.SliderMobileImageAltText = dr["SliderMobileImageAltText"] != System.DBNull.Value ? (string)dr["SliderMobileImageAltText"] : "";
                    slider.SliderMobileImageOriginalFilePath = dr["SliderMobileImageOriginalFilePath"] != System.DBNull.Value ? (string)dr["SliderMobileImageOriginalFilePath"] : "";
                    slider.SliderBotsNoFollow = dr["SliderBotsNoFollow"] != System.DBNull.Value ? Convert.ToInt32(dr["SliderBotsNoFollow"]) : 0;
                    slider.SliderTextColor = dr["SliderTextColor"] != System.DBNull.Value ? (string)dr["SliderTextColor"] : "";
                    slider.SliderCloseTextBGColor = dr["SliderCloseTextBGColor"] != System.DBNull.Value ? (string)dr["SliderCloseTextBGColor"] : "";
                    slider.SliderCloseArrowBGColor = dr["SliderCloseArrowBGColor"] != System.DBNull.Value ? (string)dr["SliderCloseArrowBGColor"] : "";
                    slider.SliderOpenTextBGColor = dr["SliderOpenTextBGColor"] != System.DBNull.Value ? (string)dr["SliderOpenTextBGColor"] : "";
                    slider.SliderOpenArrowBGColor = dr["SliderOpenArrowBGColor"] != System.DBNull.Value ? (string)dr["SliderOpenArrowBGColor"] : "";
                    slider.SliderOpenBoxBGColor = dr["SliderOpenBoxBGColor"] != System.DBNull.Value ? (string)dr["SliderOpenBoxBGColor"] : "";
                    slider.SliderOpenBoxOpacity = dr["SliderOpenBoxOpacity"] != System.DBNull.Value ? (double)dr["SliderOpenBoxOpacity"] : 0;
                    slider.PlaceHolders = new List<SliderPlaceHolder>();
                    DataRow[] drPlaceHolder = ds.Tables[1].Select("SliderId=" + slider.SliderId, "SliderPlaceholderTabIndex asc");
                    if (drPlaceHolder.Length > 0)
                    {
                        foreach (DataRow drPlcHolder in drPlaceHolder)
                        {
                            SliderPlaceHolder plc = new SliderPlaceHolder();
                            plc.SliderId = drPlcHolder["SliderId"] != System.DBNull.Value ? (int)drPlcHolder["SliderId"] : 0;
                            plc.SliderPlaceholderId = drPlcHolder["SliderPlaceholderId"] != System.DBNull.Value ? (int)drPlcHolder["SliderPlaceholderId"] : 0;
                            plc.SliderPlaceholderTabIndex = drPlcHolder["SliderPlaceholderTabIndex"] != System.DBNull.Value ? (int)drPlcHolder["SliderPlaceholderTabIndex"] : 0;
                            plc.SliderPlaceholderText = drPlcHolder["SliderPlaceholderText"] != System.DBNull.Value ? (string)drPlcHolder["SliderPlaceholderText"] : "";
                            plc.SliderPlaceholderLink = drPlcHolder["SliderPlaceholderLink"] != System.DBNull.Value ? (string)drPlcHolder["SliderPlaceholderLink"] : "";
                            slider.PlaceHolders.Add(plc);
                        }
                    }
                    sliderList.Add(slider);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return sliderList;
        }


        public static DataSet GetCustomerAuthOTP(string mobileNo)
        {
            List<Slider> sliderList = new List<Slider>();
            DataSet result = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                SqlParameter param = new SqlParameter();

                param = new SqlParameter("@MobileNum", SqlDbType.NVarChar);
                param.Size = 20;
                param.Value = mobileNo;
                paramList.Add(param);

                result = DataHelper.ExecuteQuery("iPro_spp_GetCustomerAuthenticationOTP", ConfigurationManager.AppSettings["CMSConnString"], paramList);      
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return result;
        }

        public static DataSet AuthCustomerOTP(string mobileNo, string otpr)
        {
            List<Slider> sliderList = new List<Slider>();
            DataSet result = null;
            try
            {
                List<SqlParameter> paramList = new List<SqlParameter>();
                SqlParameter param = new SqlParameter();

                param = new SqlParameter("@MobileNum", SqlDbType.NVarChar);
                param.Size = 20;
                param.Value = mobileNo;
                paramList.Add(param);

                param = new SqlParameter("@OTP", SqlDbType.NVarChar);
                param.Size = 20;
                param.Value = otpr;
                paramList.Add(param);

                result = DataHelper.ExecuteQuery("iPro_spp_CheckCustomerAuthenticationOTP", ConfigurationManager.AppSettings["CMSConnString"], paramList);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            return result;
        }
    }
}