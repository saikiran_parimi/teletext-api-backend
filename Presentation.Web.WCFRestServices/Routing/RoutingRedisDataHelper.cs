﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Routing;
using ServiceStack;
using ServiceStack.Redis;

namespace Presentation.Web.WCFRestServices.Routing
{
    public class RoutingRedisDataHelper
    {
        public static RoutingResponse GetRoutingDataByResortIds(List<string> resortIds)
        {
            RoutingResponse response = new RoutingResponse { valid = true };

            using (
                PooledRedisClientManager pooledRedisClientManager =
                    new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                {
                    redisClient.Db = Global.RedisDbForResortId;

                    List<string> redisResortData = new List<string>();

                    try
                    {
                        redisResortData = redisClient.GetValues(resortIds);
                    }
                    catch (Exception exception)
                    {
                        ErrorLogger.Log("\nException occured while retrieving data for resort id's.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                    }

                    foreach (string resortData in redisResortData)
                    {
                        try
                        {
                            if (!String.IsNullOrEmpty(resortData))
                            {
                                RoutingDataByResortId routingDataByResortId = JsonConvert.DeserializeObject<RoutingDataByResortId>(resortData);

                                if (routingDataByResortId == null)
                                {
                                    continue;
                                }
                                response.departureIds.AddRange(routingDataByResortId.departureIds.Except(response.departureIds).ToList());
                                response.dates.AddRange(routingDataByResortId.dates.Except(response.dates).ToList());
                                response.durations.AddRange(routingDataByResortId.duration.Except(response.durations).ToList());
                            }
                        }
                        catch (Exception exception)
                        {
                            ErrorLogger.Log("\nException occured while retrieving ResortId: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                        }
                    }
                }
            }

            return response;
        }

        public static RoutingResponse GetRoutingDataByAirportIds(List<string> airportIds)
        {
            RoutingResponse response = new RoutingResponse();

            using (
                PooledRedisClientManager pooledRedisClientManager =
                    new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                {
                    redisClient.Db = Global.RedisDbForAirportCode;

                    List<string> airportCodeRedisData = new List<string>();

                    try
                    {
                        airportCodeRedisData = redisClient.GetValues(airportIds);
                    }
                    catch (Exception exception)
                    {
                        ErrorLogger.Log("\nError occurred while retrieving data for " + String.Join(",", airportIds) + "' from redis.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                    }

                    foreach (string airportData in airportCodeRedisData)
                    {
                        try
                        {
                            if (!String.IsNullOrEmpty(airportData))
                            {
                                RoutingDataByAirportCode routingDataByAirportCode =
                                    JsonConvert.DeserializeObject<RoutingDataByAirportCode>(airportData);

                                if (routingDataByAirportCode == null)
                                {
                                    continue;
                                }
                                //response.departureIds.Add(routingDataByAirportCode.departureId);
                                response.dates.AddRange(routingDataByAirportCode.dates.Except(response.dates).ToList());
                                response.destinationIds.AddRange(routingDataByAirportCode.resortIds.Except(response.destinationIds).ToList());
                                response.durations.AddRange(routingDataByAirportCode.duration.Except(response.durations).ToList());
                            }
                        }
                        catch (Exception exception)
                        {
                            ErrorLogger.Log("\nException occured while retrieving AirportCode: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                        }
                    }

                }
            }
            return response;
        }

        public static RoutingResponse GetRoutingDataByResortIdsAndAirportIds(List<string> resortIds, List<string> airportIds)
        {
            RoutingResponse routingResponse = new RoutingResponse();

            List<string> redisKeyList = (from resortId in resortIds
                                         from airportId in airportIds
                                         select airportId + "_" + resortId).ToList();

            try
            {
                using (
                    PooledRedisClientManager pooledRedisClientManager =
                        new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                    {
                        redisClient.Db = Global.RedisDbForResortIdAndAirportCode;

                        List<string> resortIdAndAirportCodeRedisData = redisClient.GetValues(redisKeyList);

                        foreach (string resortIdAndAirportCodeData in resortIdAndAirportCodeRedisData)
                        {
                            try
                            {
                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId =
                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                        resortIdAndAirportCodeData);

                                if (routingDataByAirportCodeAndResortId == null)
                                {
                                    continue;
                                }

                                routingResponse.dates.AddRange(routingDataByAirportCodeAndResortId.dates.Except(routingResponse.dates));
                                routingResponse.durations.AddRange(routingDataByAirportCodeAndResortId.duration.Except(routingResponse.durations));
                            }
                            catch (Exception exception)
                            {
                                ErrorLogger.Log("\nException occured while retrieving AirportCode&ResortId: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("\nException occured while retrieving AirportCode&ResortId: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
            }

            return routingResponse;
        }

        public static RoutingResponse GetRoutingDataByResortIdsAndAirportIdsAndDates(List<string> resortIds, List<string> airportIds, List<string> datesList)
        {
            RoutingResponse routingResponse = new RoutingResponse();

            List<string> redisKeyList = (from resortId in resortIds
                                         from airportId in airportIds
                                         select airportId + "_" + resortId).ToList();

            try
            {
                using (
                    PooledRedisClientManager pooledRedisClientManager =
                        new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledRedisClientManager.GetClient())
                    {
                        redisClient.Db = Global.RedisDbForResortIdAndAirportCode;

                        List<string> resortIdAndAirportCodeRedisData = redisClient.GetValues(redisKeyList);

                        foreach (string resortIdAndAirportCodeData in resortIdAndAirportCodeRedisData)
                        {
                            try
                            {
                                RoutingDataByAirportCodeAndResortId routingDataByAirportCodeAndResortId =
                                    JsonConvert.DeserializeObject<RoutingDataByAirportCodeAndResortId>(
                                        resortIdAndAirportCodeData);

                                if (routingDataByAirportCodeAndResortId == null)
                                {
                                    continue;
                                }

                                foreach (string date in datesList)
                                {
                                    if (routingDataByAirportCodeAndResortId.datesAndDurations.ContainsKey(date))
                                    {
                                        routingResponse.durations.AddRange(routingDataByAirportCodeAndResortId.datesAndDurations[date].Except(routingResponse.durations));
                                        if (!routingResponse.dates.Contains(date))
                                        {
                                            routingResponse.dates.Add(date);
                                        }
                                    }
                                }

                            }
                            catch (Exception exception)
                            {
                                ErrorLogger.Log("\nException occured while retrieving AirportCode&ResortId&Dates: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                ErrorLogger.Log("\nException occured while retrieving AirportCode&ResortId&Dates: '___'.\nMessage: " + exception.Message + ".\nStack Trace: " + exception.StackTrace, LogLevel.GetRoutingApiError);
            }



            return routingResponse;
        }
    }
}