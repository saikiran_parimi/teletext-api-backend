﻿
using System;
using System.Web;
using System.Linq;
using System.Collections.Generic;
using Presentation.Web.WCFRestServices.RedisDataAccessHelper;
using System.Configuration;
using Presentation.WCFRestService.Model;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model.Misc.TTModel;
using System.Text;
using System.IO;

namespace Presentation.Web.WCFRestServices
{
    public class HotelService : IHotelService
    {
        public string GetHotelDataByMasterHotelId(string masterHotelId)
        {
            string json = string.Empty;
            string CacheExpiryindays = ConfigurationManager.AppSettings["CacheExpiryindays"].ToString();

            try
            {
                json = GetHotelDataFromRedis(masterHotelId);

                // check if RediCache has value for CacheCreateDt
                // if not get latest information from S3
                TTHotel hotel = new TTHotel();
                hotel = JsonConvert.DeserializeObject<TTHotel>(json);
                DateTime? CacheCreateDt = hotel._TTInfo.TripAdvisorCacheCreateDate;
                              
                if (!CacheCreateDt.HasValue)
                {
                    json  = UpdateTripAdvisorData(masterHotelId, hotel, ref json);                   
                }
                else
                {
                    // check if RediCache current date - CacheCreateDt > Cacheexpiry config value
                    // if true read latest inforamtion from S3
                    TimeSpan ts = DateTime.Now - hotel._TTInfo.TripAdvisorCacheCreateDate;
                    if (ts.Days > Convert.ToInt32(CacheExpiryindays))
                    {
                        json = UpdateTripAdvisorData(masterHotelId, hotel, ref json);   
                    }
                }
            }
            catch (System.ArgumentNullException ex)
            {  
                json = GetHotelDataAfterRedisInsertion(masterHotelId);               
            }
            catch (Exception ex)
            {
               
            }
            return json;
        }

        private string UpdateTripAdvisorData(string masterHotelId, TTHotel hotel, ref string json)
        {
            SetTripAdvisorDataFromS3(ref hotel);
            string hotelJson = JsonConvert.SerializeObject(hotel);
            if (UpdateHotelDataIntoRedis(masterHotelId, hotelJson))
            {
                json = hotelJson;                
            }
            return json;
        }

        private static string GetHotelDataFromRedis(string masterHotelId)
        {
            string json = string.Empty;
            try
            {
                json = RedisDataHelper.GetValueByKey(
                    masterHotelId,
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
            }
            catch (System.ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return json;
        }

        private string GetHotelDataAfterRedisInsertion(string masterHotelId)
        {
            string json = string.Empty;            
            try
            {
                json = GetHotelDataFromS3(masterHotelId);
                if (InsertHotelDataIntoRedis(masterHotelId, json))
                {
                    json = GetHotelDataFromRedis(masterHotelId);
                }
            }
            catch (Exception ex)
            {

            }
            return json;
        }

        private void SetTripAdvisorDataFromS3(ref TTHotel hotel)
        {
            string TAJsonName =string.Format(ConfigurationManager.AppSettings["TripAdvisorHotelDataFile"].ToString(), hotel._source.HotelId);
            try
            {
                var TAJson = string.Empty;

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForTripAdvisorData(ConfigurationManager.AppSettings["AWSTripAdvisorBucketName"], TAJsonName)))
                {
                    TAJson = reader.ReadToEnd();
                }
                TripAdvisorAPIresponseClass taRating = JsonConvert.DeserializeObject<TripAdvisorAPIresponseClass>(TAJson);
                hotel._TTInfo.TripAdvisorRating = taRating.rating;
                hotel._TTInfo.TripAdvisorReviewCount = taRating.reviews_count;
                hotel._TTInfo.TripAdvisorCacheCreateDate = DateTime.Now;
            }
            catch (Exception E)
            {
                hotel._TTInfo.TripAdvisorCacheCreateDate = DateTime.Now;
            }
        }

        private string GetHotelDataFromS3(string masterHotelId)
        {
            string jsonUpdatedTArating = string.Empty;
            try
            {
                string url = string.Format(ConfigurationManager.AppSettings["TTFilePath"].ToString(), masterHotelId + ".json");

                string json = Utilities.ExecuteGetWebRequest(url);

                TTHotel hotel = new TTHotel();

                hotel = JsonConvert.DeserializeObject<TTHotel>(json);
                
                SetTripAdvisorDataFromS3(ref hotel);
                                               
                jsonUpdatedTArating = JsonConvert.SerializeObject(hotel);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            return jsonUpdatedTArating;
        }

        private static bool InsertHotelDataIntoRedis(string masterHotelId, string json)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    result = RedisDataHelper.InsertValue(
                    masterHotelId,
                    json,
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return result;
        }

        private static bool UpdateHotelDataIntoRedis(string masterHotelId, string json)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    result = RedisDataHelper.UpdateValue(
                    masterHotelId,
                    json,
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        private static bool DeleteHotelDataFromRedis(string masterHotelId)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(masterHotelId))
                {
                    result = RedisDataHelper.DeleteValue(masterHotelId,                    
                    ConfigurationManager.AppSettings["RedisHostForHotelJson"],
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisPort"]),
                    Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForHotelJson"]),
                    (String.IsNullOrEmpty(ConfigurationManager.AppSettings["RedisPassword"]) ? null : ConfigurationManager.AppSettings["RedisPassword"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }


        public string GetMasterHotelIdByURL(string url)
        {
            string hotelId = string.Empty;
            HotelURL hotel = Global.AllHotelURLMap.FirstOrDefault(a => a.FriendlyURL == url);
            if (hotel != null)
            {
                hotelId = hotel.MasterHotelID.ToString();
            }
            return hotelId;   
        }

        public bool SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel)
        {
            bool result = false;
            try
            {
                Amazon.S3.IAmazonS3 client;
                using (client = new Amazon.S3.AmazonS3Client(
                    ConfigurationManager.AppSettings["HotelDataAWSAcessKeyId"],
                    ConfigurationManager.AppSettings["HotelDataAWSSceretAccessKeyId"], Amazon.RegionEndpoint.EUWest1))
                {   
                    Amazon.S3.Model.PutObjectRequest request = new Amazon.S3.Model.PutObjectRequest()
                    {
                        BucketName = ConfigurationManager.AppSettings["HotelDataBucket"],
                        Key = ConfigurationManager.AppSettings["HotelDataFolder"] + "/" + hotel._source.HotelId + ".json"
                    };
                    request.ContentType = "text/json";
                    request.InputStream = new System.IO.MemoryStream(System.Text.Encoding.UTF8.GetBytes(Newtonsoft.Json.JsonConvert.SerializeObject(hotel)));
                    Amazon.S3.Model.PutObjectResponse response2 = client.PutObject(request);
                }
                result = UpdateHotelDataIntoRedis(hotel._source.HotelId.ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(hotel));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
    }
}