﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices
{
    public interface IHotelService
    {
        string GetMasterHotelIdByURL(string url);
        string GetHotelDataByMasterHotelId(string masterHotelId);
        bool SetMasterHotelData(Presentation.WCFRestService.Model.Misc.TTModel.TTHotel hotel);
    }
}