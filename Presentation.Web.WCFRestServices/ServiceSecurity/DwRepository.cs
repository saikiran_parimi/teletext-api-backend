﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class DwRepository
    {
        MapperConfiguration mapperConfiguration;
        IMapper imapper;

        public DwRepository()
        {
            mapperConfiguration = new MapperConfiguration(q => q.AddProfile(new DataTableProfile()));
            imapper = mapperConfiguration.CreateMapper();
        }

        public List<TeletextUsers> GetUsers()
        {
            List<TeletextUsers> users;
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    var dt = new DataTable();
                    command.Connection = connection;
                    command.CommandText = $"exec sp_executesql N'SELECT * FROM {ConfigurationManager.AppSettings.Get("UserTable")}'";
                    var adapter = new SqlDataAdapter(command);
                    adapter.Fill(dt);
                    users = imapper.Map<List<DataRow>, List<TeletextUsers>>(dt.Rows.Cast<DataRow>().ToList());
                }
            }
            return users;
        }

        /// <summary>
        /// Validate the user wrt sign up data ( validation will happen on in memory data )
        /// </summary>
        /// <param name="email">type of string</param>
        /// <returns>status as boolean</returns>
        public bool ValidateUser(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException($"{nameof(email)} cann't be null");
            }

            var user = GetUsers();

            return user.Where(q => q.Email.Equals(email, StringComparison.InvariantCultureIgnoreCase)).Any();
        }

        /// <summary>
        /// Validate the user wrt sign up data ( validation will happen at database )
        /// </summary>
        /// <param name="email">type of string</param>
        /// <returns>status as boolean</returns>
        public bool CheckUser(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException($"{nameof(email)} cann't be null");
            }

            var status = false;
            var query = $"exec sp_executesql N'SELECT CAST((CASE WHEN ( SELECT COUNT(*) FROM {ConfigurationManager.AppSettings.Get("UserTable")} WHERE Email = @email  ) > 0 THEN 1 ELSE 0 END) AS BIT) AS [Status]', N'@email NVARCHAR(400)', '{email}'";
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                using (var command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = query;
                    connection.Open();
                    status = (bool)command.ExecuteScalar();
                    connection.Close();
                }
            }
            return status;
        }

        public bool IsTokenExpired(string iat)
        {
            if (string.IsNullOrEmpty(iat))
            {
                throw new ArgumentNullException($"{nameof(iat)} cann't be null");
            }
            long time = (int)(DateTime.UtcNow.Subtract(DateTime.Parse(iat))).TotalSeconds;
            if ( time <= int.Parse(ConfigurationManager.AppSettings["TimeForValidatingDomain"]))
            {
                ErrorLogger.Log(string.Format("{0}", "With in token time"), WCFRestService.Model.Enum.LogLevel.Information);
                return true;
            }
            return false;
        }
    }
}