﻿using OfficeOpenXml;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Linq;

namespace Presentation.Web.WCFRestServices.ServiceSecurity
{
    public class Common
    {
        public static byte[] Base64UrlDecode(string arg)
        {
            string s = arg;
            s = s.Replace('-', '+');
            s = s.Replace('_', '/');
            switch (s.Length % 4)
            {
                case 0: break;
                case 2: s += "=="; break;
                case 3: s += "="; break;
                default:
                    throw new Exception(
                "Illegal base64url string!");
            }
            return Convert.FromBase64String(s);
        }

        public static string SignedKey
        {
            get
            {
                return ConfigurationManager.AppSettings["signedKey"];
            }
            set
            {
                value = ConfigurationManager.AppSettings["signedKey"];
            }
        }

        public static long ToUnixTime(DateTime dateTime)
        {
            return (int)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static string Countries()
        { 
            var fileBytes = File.ReadAllBytes(Path.Combine(HttpRuntime.AppDomainAppPath, @"data\TopDogCountries.xlsx"));

            var dt = new DataTable(); var flag = false;

            dt.Columns.Add(new DataColumn("Code", typeof(string)));
            dt.Columns.Add(new DataColumn("Name", typeof(string)));

            using (var ms = new MemoryStream(fileBytes))
            {
                using (var package = new ExcelPackage(ms))
                {
                    if (package.Workbook.Worksheets.Count > 0)
                    {
                        var sheet = package.Workbook.Worksheets["toddog countries names"];
                        for (int i = sheet.Dimension.Start.Row; i <= sheet.Dimension.End.Row; i++)
                        {
                            if (flag)
                            {
                                var dr = dt.NewRow();
                                for (int j = sheet.Dimension.Start.Column; j <= sheet.Dimension.End.Column; j++)
                                {
                                    if (j < 3)
                                    {
                                        if (j == 1)
                                        {
                                            dr["Code"] = sheet.Cells[i, j].Value;
                                        }
                                        else if (j == 2)
                                        {
                                            dr["Name"] = sheet.Cells[i, j].Value;
                                        }
                                    }
                                }
                                dt.Rows.Add(dr); dt.AcceptChanges();
                            }
                            else { flag = true; }
                        }
                    }
                }
            }

            var results = (from dr in dt.AsEnumerable() select new { CountryName = dr["Name"], CountryCode = dr["Code"] }).ToList();

            return Newtonsoft.Json.JsonConvert.SerializeObject(results);
        }
    }
}