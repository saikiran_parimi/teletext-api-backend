﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.MyBooking
{
    public static class TopDogHelperModule
    {
        public static string GetAirportName(string airportCode)
        {
            string airportName = "";            
            string fileName = HttpRuntime.AppDomainAppPath + @"data\AirportMaster.xml";            
            DataSet dsAirport = new DataSet();

            if (System.IO.File.Exists(fileName))
            {
                dsAirport.ReadXml(fileName);

                foreach (DataRow row in dsAirport.Tables[0].Rows)
                {
                    if (row["code"].ToString().ToLower().Equals(airportCode.ToLower()))
                    {
                        airportName = row["name"].ToString();
                        break;
                    }
                }
            }
            return airportName;
        }

        public static string GetDestinationName(string airportCode)
        {
            string destinationName = "";
            string fileName = HttpRuntime.AppDomainAppPath + @"data\AirportCodeDestinationMapping.xml";
            DataSet dsAirport = new DataSet();

            if (System.IO.File.Exists(fileName))
            {
                dsAirport.ReadXml(fileName);

                foreach (DataRow row in dsAirport.Tables[0].Rows)
                {
                    if (row["code"].ToString().ToLower().Equals(airportCode.ToLower()))
                    {
                        destinationName = row["destinationName"].ToString();
                        break;
                    }
                }
            }
            return destinationName;
        }
    }
}