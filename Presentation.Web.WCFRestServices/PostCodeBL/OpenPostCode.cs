﻿using System;
using Presentation.WCFRestService.Model;
using Newtonsoft.Json;
using System.Configuration;
using Presentation.WCFRestService.Model.Enum;

namespace Presentation.Web.WCFRestServices
{
    /* this class will responsible to get data from flight stats */
    public class OpenPostCode : IPostCodeAddresses
    {
        /* implemented on Flight status (flights departing on date) */
        public PostCodeResponse GetAddressesByPostCode(string postCode)
        {
            PostCodeResponse item = null;
            try
            {
                string url = string.Format(ConfigurationManager.AppSettings["PostCode_URL"], postCode, ConfigurationManager.AppSettings["PostCode_appKey"]);
                var json = String.Empty;               
                json = Utilities.GetJson<PostCodeResponse>(url);
                item = JsonConvert.DeserializeObject<PostCodeResponse>(json);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }

            return item;
        }
    }
}