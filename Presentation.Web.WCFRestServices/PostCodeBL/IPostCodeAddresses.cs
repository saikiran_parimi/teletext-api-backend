﻿using Presentation.WCFRestService.Model;

namespace Presentation.Web.WCFRestServices
{
    interface IPostCodeAddresses
    {
        PostCodeResponse GetAddressesByPostCode(string postCode);
    }
}
