﻿using Amazon.S3.Model;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Presentation.WCFRestService.Model;
using Presentation.WCFRestService.Model.Enum;
using Presentation.WCFRestService.Model.Misc;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Presentation.Web.WCFRestServices.MyBooking
{
    public class TopDogManager
    {
        public delegate void TopDogPaymentPost(MakePayment makePayment);
        public static string NotQualityChecked = "Your documents are currently being processed and will be available from 48 hours after the time you made your booking. But don't worry, we'll email you to let you know when your documents are ready for you to download";
        public static string PendingForProofs = "Pending For Proofs";

        public static MyBookings GetBasket(string BookingReference, string Surname, string DepartureDate, bool checkAuditCompleted, bool GetBookingFromTD)
        {
            Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket = null;
            MyBookings myBookingDetails = new MyBookings();

            try
            {
                if (!checkAuditCompleted || GetBookingFromTD)
                {
                    throw new Exception();
                }
                var documents = DownloadObjectsFromS3(ConfigurationManager.AppSettings["BookingAWSBucketName"], BookingReference);
                var basketXML = documents.Where(p => p.Key.Contains(".xml"));
                if (basketXML == null || basketXML.Count() == 0)
                {
                    throw new Exception();
                }
                Stream data = Utilities.DownloadS3Object(ConfigurationManager.AppSettings["BookingAWSBucketName"], basketXML.OrderByDescending(p => p.LastModified).FirstOrDefault().Key);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Presentation.Web.WCFRestServices.TopDogBasketService.Basket), "http://topdog.uk.net/BlackBox/BasketOperations");
                data.Position = 0;
                basket = (Presentation.Web.WCFRestServices.TopDogBasketService.Basket)x.Deserialize(data);
                var leadPassenger = basket.Passengers.Where(a => a.BasketLeadPassenger == true);
                if (leadPassenger != null && leadPassenger.Count() > 0)
                {
                    if (leadPassenger.FirstOrDefault().Surname.ToLower() != Surname.ToLower() && basket.Client.Surname.ToLower() != Surname.ToLower())
                    {
                        myBookingDetails.error = "Sorry, we can't find a booking with those details. Please check the details and try again.";
                        return myBookingDetails;
                    }

                    var products = basket.Products;
                    if (products != null && products.Count() > 0)
                    {
                        var basketProducts = products.Where(p => p.FromDate == DepartureDate);
                        if (basketProducts == null || basketProducts.Count() == 0)
                        {
                            myBookingDetails.error = "Sorry, we can't find a booking with those details. Please check the details and try again.";
                            return myBookingDetails;
                        }
                    }
                }
                myBookingDetails = GetBookingDetails(basket, BookingReference, Surname, DepartureDate, checkAuditCompleted);
                myBookingDetails.Etag = basketXML.OrderByDescending(p => p.LastModified).FirstOrDefault().ETag.Replace("\"", "");
                Task.Run(() => GetBasketFromTopDog(BookingReference, Surname, DepartureDate, true, checkAuditCompleted));
            }
            catch (Exception ex)
            {
                myBookingDetails = GetBasketFromTopDog(BookingReference, Surname, DepartureDate, false, checkAuditCompleted);
            }

            return myBookingDetails;
        }

        public static MyBookings SubmitDocumentsToTD(string BookingReference, string Surname, string DepartureDate, HttpFileCollection fileCollection)
        {
            MyBookings myBookingDetails = new MyBookings();

            for (int i = 0; i < fileCollection.Count; i++)
            {
                TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
                TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
                string sessionID = string.Empty;
                Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket = new TopDogBasketService.Basket();
                TopDogBasketService.AddIdentityDocumentResponse response = null;
                try
                {
                    TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                    TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                    if (BookingReference.ToLower().StartsWith("tt"))
                    {
                        startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                        startSession.Password = ConfigurationManager.AppSettings["Password"];
                        startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                    }
                    else
                    {
                        startSession.UserId = ConfigurationManager.AppSettings["TTOnlineUserId"];
                        startSession.Password = ConfigurationManager.AppSettings["TTOnlinePassword"];
                        startSession.LanguageCode = ConfigurationManager.AppSettings["TTOnlineLanguageCode"];
                    }

                    startSession1.StartSessionRequest = startSession;
                    TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                    var startSessionResponse = sessionResponse.StartSessionResponse;
                    sessionID = startSessionResponse.SessionId;

                    TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                    TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                    getBasketRequest.SessionId = sessionID;
                    getBasketRequest.BookingReference = BookingReference;
                    getBasketRequest.ClientSurname = Surname;
                    getBasketRequest.DepartDate = DepartureDate;
                    getBasketRequest1.GetBasketRequest = getBasketRequest;
                    var basketResponse = basketClient.GetBasket(getBasketRequest);
                    basket = basketResponse.Basket;

                    TopDogBasketService.AddIdentityDocumentRequest1 addIdentityDocumentRequest1 = new TopDogBasketService.AddIdentityDocumentRequest1();
                    TopDogBasketService.AddIdentityDocumentRequest addIdentityDocumentRequest = new TopDogBasketService.AddIdentityDocumentRequest();
                    addIdentityDocumentRequest.SessionId = sessionID;
                    addIdentityDocumentRequest.Document = new TopDogBasketService.IdentityDocument();
                    if (fileCollection.AllKeys[i].ToLower().Contains("license"))
                    {
                        addIdentityDocumentRequest.Document.Name = "Driver's License";
                    }
                    else if (fileCollection.AllKeys[i].ToLower().Contains("passport"))
                    {
                        addIdentityDocumentRequest.Document.Name = "Passport Copies";
                    }
                    else
                    {
                        addIdentityDocumentRequest.Document.Name = "Utility Bill";
                    }

                    MemoryStream target = new MemoryStream();
                    fileCollection[i].InputStream.CopyTo(target);
                    byte[] data = target.ToArray();
                    addIdentityDocumentRequest.Document.File = data;
                    response = basketClient.AddIdentityDocument(addIdentityDocumentRequest);
                    if (response.Errors != null && response.Errors.Count() > 0)
                    {
                        myBookingDetails.error = "internal server error";
                    }
                }
                catch (Exception ex1)
                {
                    myBookingDetails.error = "internal server error";
                    ErrorLogger.Log(ex1.ToString(), LogLevel.Error);
                }
                finally
                {
                    TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                    TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                    endSessionRequest.SessionId = sessionID;
                    endSessionRequest1.EndSessionRequest = endSessionRequest;
                    var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                    var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
                }
            }

            return myBookingDetails;
        }

        public static MyBookings SubmitOnlineCheckInInfo(string BookingReference, string Surname,
            string DepartureDate, WCFRestService.Model.AdvancePassenger.TopDogAdvPassengers passengersInfo)
        {
            MyBookings myBookingDetails = new MyBookings(); TopDogBlackBox.StartSessionRequest1 startSession1;
            TopDogBlackBox.StartSessionRequest startSession;
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
            string sessionID = string.Empty;
            TopDogBasketService.Basket basket = new TopDogBasketService.Basket();
            try
            {
                startSession1 = new TopDogBlackBox.StartSessionRequest1();
                startSession = new TopDogBlackBox.StartSessionRequest();


                if (BookingReference.ToLower().StartsWith("tt"))
                {
                    startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                    startSession.Password = ConfigurationManager.AppSettings["Password"];
                    startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                }
                else
                {
                    startSession.UserId = ConfigurationManager.AppSettings["TTOnlineUserId"];
                    startSession.Password = ConfigurationManager.AppSettings["TTOnlinePassword"];
                    startSession.LanguageCode = ConfigurationManager.AppSettings["TTOnlineLanguageCode"];
                }

                startSession1.StartSessionRequest = startSession;

                TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                var startSessionResponse = sessionResponse.StartSessionResponse;
                sessionID = startSessionResponse.SessionId;

                TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                getBasketRequest.BookingReference = BookingReference;
                getBasketRequest.ClientSurname = Surname;
                getBasketRequest.DepartDate = DepartureDate;
                getBasketRequest1.GetBasketRequest = getBasketRequest;
                var basketResponse = basketClient.GetBasket(getBasketRequest);
                basket = basketResponse.Basket;

                TopDogBasketService.UpdatePassengerRequest updatePassengerRequest = new TopDogBasketService.UpdatePassengerRequest();
                updatePassengerRequest.Passengers = new TopDogBasketService.UpdatedPassenger[passengersInfo.Passengers.Count];
                updatePassengerRequest.SessionId = startSessionResponse.SessionId;


                for (int i = 0; i < passengersInfo.Passengers.Count; i++)
                {
                    if (basket != null)
                    {
                        foreach (var passenger in basket.Passengers)
                        {
                            if (passenger.FirstName != null && passenger.Surname != null
                                    && passengersInfo.Passengers[i].PassengerFullName != null)
                            {
                                if ((passenger.FirstName + "" + passenger.Surname).Replace(" ", "").Equals(passengersInfo.Passengers[i].PassengerFullName.Replace(" ", "")))
                                {
                                    updatePassengerRequest.Passengers[i] = new TopDogBasketService.UpdatedPassenger();
                                    updatePassengerRequest.Passengers[i].BasketLeadPassenger = passenger.BasketLeadPassenger;
                                    updatePassengerRequest.Passengers[i].Gender = passenger.Gender;
                                    updatePassengerRequest.Passengers[i].FirstName = passenger.FirstName;
                                    updatePassengerRequest.Passengers[i].Surname = passenger.Surname;
                                    updatePassengerRequest.Passengers[i].EmailAddress = passenger.EmailAddress;
                                    updatePassengerRequest.Passengers[i].PassengerId = passenger.PassengerId;
                                    updatePassengerRequest.Passengers[i].Nationality = passengersInfo.Passengers[i].Nationality;
                                    updatePassengerRequest.Passengers[i].PassportExpirationDate = passengersInfo.Passengers[i].PassportExpiryDate;
                                    updatePassengerRequest.Passengers[i].PassportIssueDate = passengersInfo.Passengers[i].PassportIssuedDate;
                                    updatePassengerRequest.Passengers[i].PassportNumber = passengersInfo.Passengers[i].PassportNumber;
                                    updatePassengerRequest.Passengers[i].PassportPlaceOfIssue = passengersInfo.Passengers[i].PlaceOfIssue;
                                    updatePassengerRequest.Passengers[i].BirthDate = passengersInfo.Passengers[i].DateOfBirth;
                                    updatePassengerRequest.Passengers[i].PersonAgeCode = passenger.PersonAgeCode;
                                    var dob = DateTime.Parse(passengersInfo.Passengers[i].DateOfBirth, CultureInfo.InvariantCulture);
                                    updatePassengerRequest.Passengers[i].Age = new TopDogBasketService.IntWrapper()
                                    {
                                        Value = (DateTime.Now.Year - dob.Year)
                                    };
                                    updatePassengerRequest.Passengers[i].Title = passenger.Title;
                                }
                            }
                        }
                    }
                }

                var response = basketClient.UpdatePassenger(updatePassengerRequest);

                if (response.Errors == null)
                {
                    //StartBookingProcess                        
                    TopDogBasketService.StartBookingProcessRequest1 startBookingProcessRequest1 = new TopDogBasketService.StartBookingProcessRequest1();
                    TopDogBasketService.StartBookingProcessRequest startBookingProcessRequest = new TopDogBasketService.StartBookingProcessRequest();
                    startBookingProcessRequest.SessionId = sessionID;
                    startBookingProcessRequest1.StartBookingProcessRequest = startBookingProcessRequest;
                    TopDogBasketService.StartBookingProcessResponse startBookingProcessResponse = basketClient.StartBookingProcess(startBookingProcessRequest);

                    //ContinueBookingProcess
                    string continueBookingProcessedMessage = string.Empty;
                    TopDogBasketService.ContinueBookingProcessRequest1 continueBookingProcessRequest1 = new TopDogBasketService.ContinueBookingProcessRequest1();
                    TopDogBasketService.ContinueBookingProcessRequest continueBookingProcessRequest = new TopDogBasketService.ContinueBookingProcessRequest();
                    continueBookingProcessRequest.SessionId = sessionID;
                    continueBookingProcessRequest1.ContinueBookingProcessRequest = continueBookingProcessRequest;

                    for (int index1 = 0; !continueBookingProcessedMessage.Contains("All operations are completed") && index1 < 60; index1++)
                    {
                        TopDogBasketService.ContinueBookingProcessResponse continueBookingProcessResponse = basketClient.ContinueBookingProcess(continueBookingProcessRequest);
                        continueBookingProcessedMessage = continueBookingProcessResponse.BookingProcessStatus.StatusMessage;
                        System.Threading.Thread.Sleep(1000);
                    }

                    if (!continueBookingProcessedMessage.Contains("All operations are completed"))
                    {
                        myBookingDetails.error = continueBookingProcessedMessage;
                    }

                    //FinishBookingProcess                        
                    string finishBookingProcessedMessage = string.Empty;
                    TopDogBasketService.FinishBookingProcessRequest1 finishBookingProcessRequest1 = new TopDogBasketService.FinishBookingProcessRequest1();
                    TopDogBasketService.FinishBookingProcessRequest finishBookingProcessRequest = new TopDogBasketService.FinishBookingProcessRequest();
                    finishBookingProcessRequest.SessionId = sessionID;
                    finishBookingProcessRequest1.FinishBookingProcessRequest = finishBookingProcessRequest;

                    TopDogBasketService.FinishBookingProcessResponse finishBookingProcessResponse = null;
                    finishBookingProcessResponse = basketClient.FinishBookingProcess(finishBookingProcessRequest);
                    finishBookingProcessedMessage = finishBookingProcessResponse.BookingProcessStatus.StatusMessage;

                    if (!finishBookingProcessedMessage.Contains("Booking process is finished"))
                    {
                        myBookingDetails.error = finishBookingProcessedMessage;
                    }
                }
                else
                {
                    myBookingDetails.error = response.Errors != null && response.Errors.Count() > 0 ? string.Join(",", response.Errors) : "Failed Posting to TopDog";
                }
            }
            catch (Exception ex)
            {
                myBookingDetails.error = "internal server error";
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            finally
            {
                TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                endSessionRequest.SessionId = sessionID;
                endSessionRequest1.EndSessionRequest = endSessionRequest;
                var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
            }

            return myBookingDetails;
        }

        public static MyBookings SubmitOnlineCheckInInfo(string BookingReference, string Surname, string DepartureDate, HttpFileCollection fileCollection)
        {
            MyBookings myBookingDetails = new MyBookings();

            for (int i = 0; i < fileCollection.Count; i++)
            {
                TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
                TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
                string sessionID = string.Empty;
                Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket = new TopDogBasketService.Basket();
                TopDogBasketService.AddIdentityDocumentResponse response = null;
                try
                {
                    TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                    TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                    if (BookingReference.ToLower().StartsWith("tt"))
                    {
                        startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                        startSession.Password = ConfigurationManager.AppSettings["Password"];
                        startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                    }
                    else
                    {
                        startSession.UserId = ConfigurationManager.AppSettings["TTOnlineUserId"];
                        startSession.Password = ConfigurationManager.AppSettings["TTOnlinePassword"];
                        startSession.LanguageCode = ConfigurationManager.AppSettings["TTOnlineLanguageCode"];
                    }

                    startSession1.StartSessionRequest = startSession;
                    TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                    var startSessionResponse = sessionResponse.StartSessionResponse;
                    sessionID = startSessionResponse.SessionId;

                    TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                    TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                    getBasketRequest.SessionId = sessionID;
                    getBasketRequest.BookingReference = BookingReference;
                    getBasketRequest.ClientSurname = Surname;
                    getBasketRequest.DepartDate = DepartureDate;
                    getBasketRequest1.GetBasketRequest = getBasketRequest;
                    var basketResponse = basketClient.GetBasket(getBasketRequest);
                    basket = basketResponse.Basket;

                    TopDogBlackBox.AdvancedPassengerInfoRequest1 advancedPassengerInfoRequest1 = new TopDogBlackBox.AdvancedPassengerInfoRequest1();
                    TopDogBlackBox.AdvancedPassengerInfoRequest advancedPassengerInfoRequest = new TopDogBlackBox.AdvancedPassengerInfoRequest();
                    advancedPassengerInfoRequest.SessionId = sessionID;
                    //advancedPassengerInfoRequest.APIPassengers = new TopDogBlackBox.APIPassengersAPIPassenger()[2];
                    //TopDogBlackBox.APIPassengersAPIPassenger apiPassanger = new TopDogBlackBox.APIPassengersAPIPassenger();
                    //apiPassanger.

                    //addIdentityDocumentRequest.Document = new TopDogBasketService.IdentityDocument();
                    //if (fileCollection.AllKeys[i].ToLower().Contains("license"))
                    //{
                    //    addIdentityDocumentRequest.Document.Name = "Driver's License";
                    //}
                    //else if (fileCollection.AllKeys[i].ToLower().Contains("passport"))
                    //{
                    //    addIdentityDocumentRequest.Document.Name = "Passport Copies";
                    //}
                    //else
                    //{
                    //    addIdentityDocumentRequest.Document.Name = "Utility Bill";
                    //}

                    //MemoryStream target = new MemoryStream();
                    //fileCollection[i].InputStream.CopyTo(target);
                    //byte[] data = target.ToArray();
                    //addIdentityDocumentRequest.Document.File = data;
                    //response = basketClient.AddIdentityDocument(addIdentityDocumentRequest);
                    //if (response.Errors != null && response.Errors.Count() > 0)
                    //{
                    //    myBookingDetails.error = "internal server error";
                    //}
                }
                catch (Exception ex1)
                {
                    myBookingDetails.error = "internal server error";
                    ErrorLogger.Log(ex1.ToString(), LogLevel.Error);
                }
                finally
                {
                    TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                    TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                    endSessionRequest.SessionId = sessionID;
                    endSessionRequest1.EndSessionRequest = endSessionRequest;
                    var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                    var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
                }
            }

            return myBookingDetails;
        }

        private static MyBookings GetBasketFromTopDog(string BookingReference, string Surname, string DepartureDate, bool isAsync, bool checkAuditCompleted)
        {
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
            string sessionID = string.Empty;
            MyBookings myBookingDetails = new MyBookings();
            Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket = new TopDogBasketService.Basket();
            TopDogBasketService.GetBasketResponse response = null;
            try
            {
                TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                startSession.Password = ConfigurationManager.AppSettings["Password"];
                startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                startSession1.StartSessionRequest = startSession;
                TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                var startSessionResponse = sessionResponse.StartSessionResponse;
                sessionID = startSessionResponse.SessionId;

                TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                getBasketRequest.BookingReference = BookingReference;
                getBasketRequest.ClientSurname = Surname;
                getBasketRequest.DepartDate = DepartureDate;
                getBasketRequest1.GetBasketRequest = getBasketRequest;
                response = basketClient.GetBasket(getBasketRequest);
                basket = response.Basket;
            }
            catch (Exception ex1)
            {
                myBookingDetails.error = "internal server error";
                ErrorLogger.Log(ex1.ToString(), LogLevel.Error);
            }
            finally
            {
                TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                endSessionRequest.SessionId = sessionID;
                endSessionRequest1.EndSessionRequest = endSessionRequest;
                var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
            }

            if (!isAsync)
            {
                if (basket == null && response.Errors.Length != 0 || basket.Products == null)
                {
                    try
                    {
                        if (response.Errors[0].Contains("Concurrent modification error."))
                            myBookingDetails.error = "Sorry, we are unable to display your booking at the moment. Please try again later.";
                        else if (response.Errors[0].Contains("Failed to retrieve basket"))
                            myBookingDetails.error = "Sorry, we can't find a booking with those details. Please check the details and try again.";
                    }
                    catch
                    {
                        myBookingDetails.error = "Sorry, we are unable to display your booking at the moment. Please try again later.";
                    }
                }
                else
                {
                    myBookingDetails = GetBookingDetails(basket, BookingReference, Surname, DepartureDate, checkAuditCompleted);
                }
            }

            return myBookingDetails;
        }

        private static MyBookings GetBookingDetails(Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket, string BookingReference, string Surname, string DepartureDate, bool checkQualityChecked)
        {
            MyBookings myBookingDetails = new MyBookings();
            if (checkQualityChecked)
            {
                if (basket.QualityCheckedStatus.ToLower() == "pending for proofs")
                {
                    myBookingDetails.status = PendingForProofs;
                    return myBookingDetails;
                }

                if (basket.QualityCheckedStatus.ToLower() != "audit completed")
                {
                    myBookingDetails.error = NotQualityChecked;
                    return myBookingDetails;
                }
            }

            int flightcount = 1;
            myBookingDetails.passengers = new List<Passenger>();
            List<Hotelproduct> hotelProdList = new List<Hotelproduct>();
            List<Flightproduct> flightProdList = new List<Flightproduct>();
            List<Transferproduct> transprodList = new List<Transferproduct>();
            myBookingDetails.cost = new ProductCost();
            myBookingDetails.cost.hotelGrossPrice = GetHotelPriceForGetBasket(basket);
            myBookingDetails.cost.flightGrossPrice = GetFlightPriceForGetBasket(basket, myBookingDetails);
            myBookingDetails.cost.transportGrossPrice = GetTransferPriceForGetBasket(basket);
            myBookingDetails.cost.paymentCharges = GetPaymentCharges(basket);
            myBookingDetails.flightSupplimentList = new List<FlightSuppliment>();

            var list = new List<KeyValuePair<string, string>>();

            Hotelproduct hotelProd = null;
            Flightproduct flightProd = null;
            Transferproduct transProd = null;
            int accomodationCount = 0;
            if (basket != null)
            {
                myBookingDetails.cost.totalPrice = basket.TotalPrice;
                myBookingDetails.footerAddbaggagesExists = true;
                myBookingDetails.footerAirportHotelExists = true;
                myBookingDetails.footerAirportParkingExists = true;
                myBookingDetails.footerHotelsParkingExists = true;
                myBookingDetails.showATOLCertificate = true;
                var flightSuppliers = new List<string>();

                #region Passenger
                for (int passenger = 0; passenger < basket.Passengers.Length; passenger++)
                {
                    Passenger passengerObj = new Passenger();

                    passengerObj.passengerId = basket.Passengers[passenger].PassengerId;
                    passengerObj.isLeadPassenger = basket.Passengers[passenger].BasketLeadPassenger;
                    passengerObj.title = basket.Passengers[passenger].Title;
                    passengerObj.firstName = basket.Passengers[passenger].FirstName;
                    passengerObj.secondName = basket.Passengers[passenger].SecondName;
                    passengerObj.surname = basket.Passengers[passenger].Surname;
                    passengerObj.age = basket.Passengers[passenger].Age.Value;
                    myBookingDetails.passengers.Add(passengerObj);
                }
                #endregion

                #region payments
                double paymentrefund = 0.0;
                double paymentreceived = 0.0;
                double paymentcharges = 0.0;
                if (basket.Payments != null)
                {
                    for (int pay = 0; pay < basket.Payments.Length; pay++)
                    {
                        if (basket.Payments[pay].Refund == false && basket.Payments[pay].PaymentStatus == 0)
                        {
                            paymentreceived += basket.Payments[pay].PaymentValue;
                            paymentcharges += basket.Payments[pay].PaymentCharge;
                        }
                        if (basket.Payments[pay].Refund == true)
                        {
                            paymentrefund += basket.Payments[pay].PaymentValue;
                        }
                    }
                    if (paymentreceived >= paymentrefund)
                    {
                        paymentreceived -= paymentrefund;
                    }

                    Payment payment = new Payment();
                    list.Add(new KeyValuePair<string, string>("Payment received", Math.Round(paymentreceived, 2).ToString("0.00")));
                    list.Add(new KeyValuePair<string, string>("Total Price", Math.Round(basket.TotalPrice, 2).ToString("0.00")));
                    list.Add(new KeyValuePair<string, string>("Payment charges", Math.Round(paymentcharges, 2).ToString("0.00")));
                    myBookingDetails.payments = payment;
                }
                else
                {
                    Payment payment = new Payment();
                    list.Add(new KeyValuePair<string, string>("Payment received", "0.00"));
                    list.Add(new KeyValuePair<string, string>("Total Price", Math.Round(basket.TotalPrice, 2).ToString("0.00")));
                    myBookingDetails.payments = payment;
                }
                #endregion



                #region Product
                if (basket.Products != null)
                {
                    for (int product = 0; product < basket.Products.Length; product++)
                    {
                        #region ifcond
                        if ((int)basket.Products[product].StatusCode == 6)
                        {
                            switch ((int)basket.Products[product].ProductType)
                            {
                                case 0:
                                    hotelProd = new Hotelproduct();
                                    hotelProd.topDogProductId = basket.Products[product].ProductId;
                                    hotelProd.supplierRef = basket.Products[product].OperatorBookRef;
                                    hotelProd.CheckInDate = basket.Products[product].FromDate;
                                    hotelProd.CheckOutDate = basket.Products[product].ToDate;
                                    hotelProd.hotelCode = basket.Products[product].ProductDetails.HotelProduct.HotelCode;
                                    hotelProd.hotelName = basket.Products[product].ProductDetails.HotelProduct.HotelName;
                                    hotelProd.category = basket.Products[product].ProductDetails.HotelProduct.Category;
                                    hotelProd.roomType = basket.Products[product].ProductDetails.HotelProduct.RoomType;
                                    hotelProd.boardType = basket.Products[product].ProductDetails.HotelProduct.BoardType;
                                    hotelProd.Rating = basket.Products[product].ProductDetails.HotelProduct.Category.ToLower().Replace(" key", "").Replace("*", "").Replace(" star", "");
                                    hotelProd.resortName = basket.Products[product].ProductDetails.HotelProduct.ResortName;
                                    if (hotelProd.resortName.ToLower().Contains("airport"))
                                    {
                                        myBookingDetails.footerAirportHotelExists = false;
                                    }
                                    hotelProd.adultsCount = basket.Products[product].AdultsCount;
                                    hotelProd.childrenCount = basket.Products[product].ChildrenCount;
                                    hotelProd.infantsCount = basket.Products[product].InfantsCount;
                                    hotelProd.clientNotes = basket.Products[product].ClientNotes;
                                    accomodationCount++;

                                    hotelProdList.Add(hotelProd);

                                    myBookingDetails.hotelDetails = hotelProdList;
                                    break;

                                case 1:

                                    FlightSuppliment FlightSupp = new FlightSuppliment();
                                    FlightSupp.topDogProductId = basket.Products[product].ProductId;
                                    FlightSupp.supplements = new List<Supplements>();

                                    if (basket.Products[product].Supplements != null)
                                    {
                                        for (int i = 0; i < basket.Products[product].Supplements.Length; i++)
                                        {
                                            TopDogBasketService.ProductSupplement prdSupp = basket.Products[product].Supplements[i];
                                            Supplements supp = new Supplements();
                                            supp.Code = prdSupp.Code;
                                            supp.Description = prdSupp.Description;
                                            supp.FullDescription = prdSupp.FullDescription;
                                            supp.TypeSpecified = prdSupp.TypeSpecified;
                                            supp.CurrencyCode = prdSupp.CurrencyCode;
                                            supp.TotalPrice = prdSupp.TotalPrice;
                                            supp.GrossPrice = prdSupp.GrossPrice;
                                            supp.NetPrice = prdSupp.NetPrice;
                                            supp.ClientCommissionRate = prdSupp.ClientCommissionRate;
                                            supp.ClientCommission = prdSupp.ClientCommission;
                                            supp.VatRate = prdSupp.VatRate;
                                            supp.ClientVAT = prdSupp.ClientVAT;
                                            supp.TotalVAT = prdSupp.TotalVAT;
                                            if (prdSupp.TypeSpecified)
                                            {
                                                supp.Type = (int)prdSupp.Type;
                                            }

                                            if (prdSupp.AdultPrices != null)
                                            {
                                                supp.AdultPrices = new FlightPerPaxPrice();
                                                supp.AdultPrices.PerPaxPrice = prdSupp.AdultPrices.PerPaxPrice;
                                                supp.AdultPrices.Quantity = prdSupp.AdultPrices.Quantity;
                                            }

                                            if (prdSupp.ChildPrices != null)
                                            {
                                                supp.ChildPrices = new FlightPerPaxPrice();
                                                supp.ChildPrices.PerPaxPrice = prdSupp.ChildPrices.PerPaxPrice;
                                                supp.ChildPrices.Quantity = prdSupp.ChildPrices.Quantity;
                                            }

                                            if (prdSupp.InfantPrices != null)
                                            {
                                                supp.InfantPrices = new FlightPerPaxPrice();
                                                supp.InfantPrices.PerPaxPrice = prdSupp.InfantPrices.PerPaxPrice;
                                                supp.InfantPrices.Quantity = prdSupp.InfantPrices.Quantity;
                                            }

                                            FlightSupp.supplements.Add(supp);
                                        }
                                    }


                                    myBookingDetails.flightSupplimentList.Add(FlightSupp);

                                    for (int j = 0; j < basket.Products[product].ProductDetails.FlightProduct.Itinerary.Length; j++)
                                    {
                                        for (int k = 0; k < basket.Products[product].ProductDetails.FlightProduct.Itinerary[j].Length; k++)
                                        {
                                            flightProd = new Flightproduct();
                                            flightProd.supplierRef = basket.Products[product].OperatorBookRef;
                                            flightProd.ConfirmationCardLastFourDigits = basket.Products[product].ProductDetails.FlightProduct.ConfirmationCardLastFourDigits;
                                            var supplierId = basket.Products[product].SupplierId;
                                            flightSuppliers.Add(basket.Products[product].SupplierId);
                                            string supplierName = string.Empty;
                                            string supplierCheckinLink = string.Empty;
                                            GetSupplierDetails(supplierId, ref supplierName, ref supplierCheckinLink);
                                            flightProd.SupplierName = supplierName;
                                            if (!string.IsNullOrEmpty(supplierCheckinLink))
                                            {
                                                var supplierIndexes = supplierCheckinLink.Split('#');
                                                var leadPassanger = myBookingDetails.passengers.Where(p => p.isLeadPassenger == true);
                                                flightProd.SupplierCheckinLink = string.Format("http://www.teletextholidays.co.uk{0}?r={1}&c={2}&sn={3}#{4}", supplierIndexes[0], flightProd.supplierRef, flightProd.ConfirmationCardLastFourDigits, leadPassanger.FirstOrDefault().surname, (supplierIndexes.Count() > 1 ? supplierIndexes[1] : string.Empty));
                                            }
                                            flightProd.topDogProductId = basket.Products[product].ProductId;
                                            flightProd.id = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].Id;
                                            flightProd.idSpecified = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].IdSpecified;
                                            flightProd.flightNumber = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].FlightNumber;
                                            string flightNumber = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].FlightNumber;
                                            flightProd.operatingAirlineCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].OperatingAirlineCode;
                                            flightProd.departureAirportCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureAirportCode;
                                            flightProd.departureAirportName = TopDogHelperModule.GetAirportName(flightProd.departureAirportCode);
                                            flightProd.arrivalAirportCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].ArrivalAirportCode;
                                            flightProd.arrivalAirportName = TopDogHelperModule.GetAirportName(flightProd.arrivalAirportCode);
                                            flightProd.departureDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime);

                                            FlightStats flightStats = new FlightStats();
                                            var flightDepartureDetails = flightStats.GetFlightScheduleByFlightNumberAndFlightDate(flightProd.flightNumber, Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime).Day, Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime).Month, Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime).Year);
                                            flightProd.Airline = !string.IsNullOrEmpty(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].OperatingAirlineCode) ? GetAirLineName(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].OperatingAirlineCode, flightDepartureDetails) : string.Empty;
                                            var scheduledFlights = flightDepartureDetails != null && flightDepartureDetails.scheduledFlights != null && flightDepartureDetails.scheduledFlights.Count() > 0 ? flightDepartureDetails.scheduledFlights.Where(p => p.flightNumber == GetFlightNumber(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].FlightNumber.Replace(" ", "")) && p.departureAirportFsCode == basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureAirportCode) : null;
                                            var flightEquipmentIataCode = scheduledFlights != null && scheduledFlights.Count() > 0 ? scheduledFlights.FirstOrDefault().flightEquipmentIataCode : string.Empty;
                                            flightProd.DepartureTerminal = scheduledFlights != null && scheduledFlights.Count() > 0 ? scheduledFlights.FirstOrDefault().departureTerminal : string.Empty;

                                            if (!string.IsNullOrEmpty(flightEquipmentIataCode))
                                            {
                                                var flightEquipment = flightDepartureDetails.appendix != null && flightDepartureDetails.appendix.equipments != null && flightDepartureDetails.appendix.equipments.Count() > 0 ? flightDepartureDetails.appendix.equipments.Where(p => p.iata == flightEquipmentIataCode) : null;
                                                flightProd.AirlineType = flightEquipment != null && flightEquipment.Count() > 0 ? flightEquipment.FirstOrDefault().name : string.Empty;
                                            }

                                            var departureDate = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime);
                                            flightProd.arrivalDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].ArrivalDateTime);
                                            flightProd.adultsCount = basket.Products[product].AdultsCount;
                                            flightProd.childrenCount = basket.Products[product].ChildrenCount;
                                            flightProd.infantsCount = basket.Products[product].InfantsCount;
                                            flightProd.FreeHandBagCount = basket.Products[product].AdultsCount + basket.Products[product].ChildrenCount;
                                            var flightScheduleByFlightNumberAndFlightDateURL = "http://contentservice.teletextholidays.co.uk/GetJsonService.svc/GetFlightScheduleByFlightNumberAndFlightDate?flightNumber=" + flightNumber + "&day=" + departureDate.ToString("dd") + "&month=" + departureDate.ToString("MM") + "&year=" + departureDate.ToString("yyyy");
                                            var flightScheduleByFlightNumberAndFlightDateURLResponse = Utilities.ExecuteGetWebRequest(flightScheduleByFlightNumberAndFlightDateURL);
                                            var flightScheduleByFlightNumberAndFlightDateURLResponseObject = JsonConvert.DeserializeObject<FlightScheduleResponse>(flightScheduleByFlightNumberAndFlightDateURLResponse);
                                            if (flightScheduleByFlightNumberAndFlightDateURLResponseObject != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.airlines != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.equipments != null)
                                            {
                                                flightProd.operatingAirlineType = flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.airlines[0].name + "(" + flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.equipments[0].name + ")";
                                            }
                                            else
                                            {
                                                XmlDocument xml = new XmlDocument();
                                                xml.Load(HttpRuntime.AppDomainAppPath + @"data\AirlinesIATACode.xml");
                                                XmlNodeList xnList = xml.SelectNodes(string.Format("/Airlines/Airline[@IATACode='{0}']", flightProd.operatingAirlineCode));
                                                if (xnList.Count > 0)
                                                {
                                                    flightProd.operatingAirlineType = xnList[0].Attributes["AirlineName"].Value;

                                                }
                                            }
                                            if (basket.Products[product].Supplements != null)
                                            {
                                                for (int i = 0; i < basket.Products[product].Supplements.Length; i++)
                                                {
                                                    TopDogBasketService.ProductSupplement prdSupp = basket.Products[product].Supplements[i];

                                                    if (prdSupp.TypeSpecified)
                                                    {
                                                        int typ = (int)prdSupp.Type;
                                                        if (typ == 1)
                                                        {
                                                            if (string.IsNullOrEmpty(flightProd.AdditionalCheckInBag))
                                                            {
                                                                flightProd.AdditionalCheckInBag = prdSupp.Description;
                                                            }
                                                            else
                                                            {
                                                                flightProd.AdditionalCheckInBag = flightProd.AdditionalCheckInBag + ", " + prdSupp.Description;
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            flightcount++;
                                            flightProdList.Add(flightProd);
                                        }
                                        if (j == 0)
                                        {
                                            if (basket.Products[product].ProductDetails.FlightProduct.Itinerary.Length > 1 || basket.Products[product].ProductDetails.FlightProduct.Itinerary[0].Length > 1)
                                            {
                                                list.Add(new KeyValuePair<string, string>("Flight", Math.Round(basket.Products[product].ProductPrice.TotalPrice, 2).ToString("0.00")));
                                            }
                                            else
                                            {
                                                list.Add(new KeyValuePair<string, string>("Flight (" + flightProd.departureAirportCode + "-" + flightProd.arrivalAirportCode + ")", Math.Round(basket.Products[product].ProductPrice.TotalPrice, 2).ToString("0.00")));
                                            }
                                        }
                                    }
                                    myBookingDetails.flightDetails = flightProdList;

                                    break;

                                case 4:
                                    if (basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.ArrivalJourney != null)
                                    {

                                        transProd = new Transferproduct();
                                        transProd.topDogProductId = basket.Products[product].ProductId;
                                        transProd.supplierRef = basket.Products[product].OperatorBookRef;
                                        transProd.transferName = basket.Products[product].ProductDetails.TransferProduct.Name;
                                        transProd.transferType = "Arrival";
                                        transProd.pickUpLocation = TopDogHelperModule.GetAirportName(basket.Products[product].ProductDetails.TransferProduct.Airport) + " (" + basket.Products[product].ProductDetails.TransferProduct.Airport + ")";
                                        transProd.transferMinutes = basket.Products[product].ProductDetails.TransferProduct.TransferMinutes;
                                        transProd.pickUpDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.ArrivalJourney.PickupDateTime);
                                        transProd.dropLocation = basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.ArrivalJourney.Accomodation.Name;
                                        transProd.clientNotes = basket.Products[product].ClientNotes;
                                        transprodList.Add(transProd);
                                    }
                                    if (basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.DepartureJourney != null)
                                    {
                                        transProd = new Transferproduct();
                                        transProd.supplierRef = basket.Products[product].OperatorBookRef;
                                        transProd.transferName = basket.Products[product].ProductDetails.TransferProduct.Name;
                                        transProd.transferType = "Return";
                                        transProd.dropLocation = TopDogHelperModule.GetAirportName(basket.Products[product].ProductDetails.TransferProduct.Airport) + " (" + basket.Products[product].ProductDetails.TransferProduct.Airport + ")";
                                        transProd.transferMinutes = basket.Products[product].ProductDetails.TransferProduct.TransferMinutes;
                                        transProd.pickUpDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.DepartureJourney.PickupDateTime);
                                        transProd.pickUpLocation = basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.DepartureJourney.Accomodation.Name;
                                        transProd.clientNotes = basket.Products[product].ClientNotes;
                                        transprodList.Add(transProd);
                                    }
                                    list.Add(new KeyValuePair<string, string>("Transfer", Math.Round(basket.Products[product].ProductPrice.TotalPrice, 2).ToString("0.00")));

                                    myBookingDetails.transferDetails = transprodList;
                                    break;
                            }
                        }
                        #endregion
                    }

                    if (myBookingDetails.flightDetails != null && myBookingDetails.flightDetails.Count() > 0)
                    {
                        var flightProducts = myBookingDetails.flightDetails.OrderBy(p => p.arrivalDateTime).ToList();
                        myBookingDetails.DestinationName = TopDogHelperModule.GetDestinationName(flightProducts[(flightProducts.Count / 2) - 1].arrivalAirportCode);
                        BuildAncillaries(myBookingDetails.flightDetails, basket, myBookingDetails);
                        CheckUniqueFlightAdditionalDetails(myBookingDetails.flightDetails);
                    }
                    else
                    {
                        myBookingDetails.footerAddbaggagesExists = false;
                        myBookingDetails.footerAirportHotelExists = false;
                        myBookingDetails.footerAirportParkingExists = false;
                        myBookingDetails.footerHotelsParkingExists = false;
                        myBookingDetails.showATOLCertificate = false;
                    }
                }

                #endregion

                #region charge

                double price = 0.0;
                double recurringPaymentCharge = 0.0;

                if (basket.Charges != null && basket.Charges.Length > 0 && myBookingDetails.payments != null)
                {
                    for (int charge = 0; charge < basket.Charges.Length; charge++)
                    {
                        if (basket.Charges[charge].Code == "NSP" && basket.Charges[charge].StatusCode == 0)
                        {
                            price += basket.Charges[charge].Price.Price;
                        }
                        if (basket.Charges[charge].Code == "RPHF" && basket.Charges[charge].StatusCode == 0)
                        {
                            recurringPaymentCharge += basket.Charges[charge].Price.Price;
                        }
                    }

                    price = Math.Round(price, 2);
                    list.Add(new KeyValuePair<string, string>("Recurring Payment Charge", Math.Round(recurringPaymentCharge, 2).ToString("0.00")));
                    int count = 0;
                    for (int prod = 0; prod < basket.Products.Length && count < accomodationCount; prod++)
                    {
                        if (basket.Products[prod].ProductType == 0 && (int)basket.Products[prod].StatusCode == 6)
                        {
                            list.Add(new KeyValuePair<string, string>("Accomodation (Room " + (count + 1) + ")", Math.Round(basket.Products[prod].ProductPrice.TotalPrice + (price / accomodationCount), 2).ToString("0.00")));
                            count++;
                        }
                    }
                    myBookingDetails.payments.charges = list;

                }
                #endregion

                #region Clients

                Client client = new Client();
                client.title = basket.Client.Title;
                client.firstName = basket.Client.FirstName;
                client.surname = basket.Client.Surname;
                myBookingDetails.client = client;

                #endregion

                #region generalobj

                myBookingDetails.bookingDate = Convert.ToDateTime(basket.BookingDate);
                myBookingDetails.bookingReference = basket.BookingReference;
                myBookingDetails.status = basket.Status;

                #endregion

                #region Balance

                list.Add(new KeyValuePair<string, string>("Final installment due date", basket.Balance.BalanceDue));
                list.Add(new KeyValuePair<string, string>("Balance due", Math.Round(basket.Balance.BalanceValue, 2).ToString("0.00")));
                #endregion

                #region Deposit
                if (myBookingDetails.payments != null)
                {
                    Deposit deposit = new Deposit();
                    deposit.depositValue = Math.Round(basket.Deposit.DepositValue, 2);
                    deposit.depositDue = basket.Deposit.DepositDue;
                    myBookingDetails.payments.deposit = deposit;
                }
                #endregion

                #region LowDeposit
                if (myBookingDetails.payments != null)
                {
                    Lowdeposit lowDepo = new Lowdeposit();
                    lowDepo.lowDepositDue = basket.LowDeposit.LowDepositDue;
                    lowDepo.lowDepositValue = Math.Round(basket.LowDeposit.LowDepositValue, 2);
                    myBookingDetails.payments.lowDeposit = lowDepo;
                }

                if (basket.Balance.BalanceValue > 0)
                {
                    if (basket.LowDeposit != null && basket.LowDeposit.LowDepositValue > 0)
                    {
                        myBookingDetails.isLowDepositBooking = true;
                        myBookingDetails.lowDepositBalance = "£" + (Math.Round(basket.Balance.BalanceValue, 2).ToString("F"));
                        var installmentPlan = GetInstallmentPlan(BookingReference, Surname, DepartureDate);

                        var lowDepositInstallmentPlan = basket.Payments.Where(p => p.PaymentStatus != TopDogBasketService.PaymentStatus.CANCELLED && p.PaymentStatus != TopDogBasketService.PaymentStatus.NEW).Skip(1).Select(p => new LowDepositInstallmentPlan
                        {
                            Amount = Math.Round(p.PaymentValue - p.PaymentCharge, 2).ToString("F"),
                            AmountType = "Installment",
                            PaymentDate = Convert.ToDateTime(p.PaymentDate),
                            PaymentStatus = "Paid"
                        }).OrderBy(a => a.PaymentDate).ToList();

                        if (lowDepositInstallmentPlan != null && lowDepositInstallmentPlan.Count > 0)
                        {
                            lowDepositInstallmentPlan.FirstOrDefault().AmountType = "Deposit";
                        }

                        var installmentLowDepositInstallmentPlan = installmentPlan.Select(p => new LowDepositInstallmentPlan
                        {
                            Amount = Math.Round(p.Amount, 2).ToString("F"),
                            AmountType = p.AmountType.ToLower() == "deposit" ? "Deposit" : "Installment",
                            PaymentDate = Convert.ToDateTime(p.PaymentDate),
                            PaymentStatus = string.Empty
                        }).OrderBy(a => a.PaymentDate).ToList();

                        lowDepositInstallmentPlan.AddRange(installmentLowDepositInstallmentPlan);
                        lowDepositInstallmentPlan.Last().AmountType = "Final Payment";

                        lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date < DateTime.Now.Date).ToList().ForEach(i =>
                        {
                            i.PaymentStatus = "Overdue";
                        });

                        if (lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date) != null && lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date).Count() > 0)
                        {
                            if (lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date).Count() == 1)
                            {
                                var firstInstallment = lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date).ToList().FirstOrDefault();
                                firstInstallment.PaymentStatus = "Next Payment";
                                firstInstallment.AmountType = "Final Payment";
                            }
                            else
                            {
                                lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date).ToList().FirstOrDefault().PaymentStatus = "Next Payment";
                                var lastInstallment = lowDepositInstallmentPlan.Where(p => p.PaymentStatus == "" && p.PaymentDate.Date >= DateTime.Now.Date).ToList().Last();
                                lastInstallment.PaymentStatus = "";
                                lastInstallment.AmountType = "Final Payment";
                            }
                        }

                        myBookingDetails.lowDepositInstallmentPlan = lowDepositInstallmentPlan;

                        var priorDaysValue = (Convert.ToDateTime(DepartureDate) - Convert.ToDateTime(basket.Balance.BalanceDue).Date).TotalDays;
                        myBookingDetails.priorDays = priorDaysValue.ToString();
                        myBookingDetails.priorDaysInBtm = priorDaysValue.ToString();
                        myBookingDetails.finalInstallment = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(basket.Balance.BalanceDue));

                        var payments = basket.Payments.Where(p => p.PaymentStatus != TopDogBasketService.PaymentStatus.CANCELLED && p.PaymentStatus != TopDogBasketService.PaymentStatus.NEW);
                        if (payments != null && payments.Count() > 0)
                        {
                            var paymentObject = payments.ToList()[0];
                            if (paymentObject.UseForAutoPayments)
                            {
                                var cardName = FindCardNameBasedOnCardType(paymentObject.CreditCard.CardType);
                                myBookingDetails.cardType = cardName;
                                myBookingDetails.cardNumber = "XXXX XXXX XXXX " + paymentObject.CreditCard.CardNumber.Substring(paymentObject.CreditCard.CardNumber.Length - 4, 4);
                                var image = FindCardImageNameBasedOnCardType(paymentObject.CreditCard.CardType);
                                myBookingDetails.cardName = image;
                                myBookingDetails.isAutoPayments = paymentObject.UseForAutoPayments;
                            }
                        }


                        myBookingDetails.paymentPlanFAQs = GetPaymentPlanFAQs();
                    }
                }

                #endregion

                #region BookedBy

                Bookedby bookBy = new Bookedby();
                bookBy.name = basket.BookedBy.Name;
                bookBy.emailAddress = basket.BookedBy.EmailAddress;
                myBookingDetails.bookedBy = bookBy;

                #endregion

                if (myBookingDetails.flightDetails != null && myBookingDetails.flightDetails.Count > 0 && flightSuppliers.Count > 0)
                {
                    var path = (HttpRuntime.AppDomainAppPath + @"data\AirlineSuppliers.xml");
                    string[] suppliers = XElement.Load(path).Elements().Select(q => q.FirstAttribute.Value).ToArray();
                    suppliers = suppliers ?? new string[0];

                    myBookingDetails.SubmitDocuments = (from supplier in flightSuppliers let g = suppliers where g.Contains(supplier) select supplier).Any();
                }
            }

            return myBookingDetails;
        }

        private static List<PaymentPlanFAQ> GetPaymentPlanFAQs()
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpRuntime.AppDomainAppPath + @"data\PaymentPlanFAQs.xml");
            XmlNodeList faqList = xml.SelectNodes("/FAQs/FAQ");
            PaymentPlanFAQ paymentPlanFAQ = null;
            List<PaymentPlanFAQ> paymentPlanFAQs = new List<PaymentPlanFAQ>();

            foreach (XmlNode faq in faqList)
            {
                paymentPlanFAQ = new PaymentPlanFAQ();
                paymentPlanFAQ.Name = faq.Attributes["name"].Value;
                paymentPlanFAQ.Link = faq.Attributes["link"].Value;
                paymentPlanFAQs.Add(paymentPlanFAQ);
            }

            return paymentPlanFAQs;
        }

        private static string GetFlightNumber(string flightNumber)
        {
            string flightCode = string.Empty;
            if (Char.IsLetter(flightNumber, 2))
                flightCode = flightNumber.Substring(3, flightNumber.Length - 3);
            else
                flightCode = flightNumber.Substring(2, flightNumber.Length - 2);
            return flightCode;
        }

        public static string GetAirLineName(string code, FlightScheduleResponse flightDepartureDetails)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpRuntime.AppDomainAppPath + @"data\AirlinesIATACode.xml");
            XmlNodeList xnList = xml.SelectNodes(string.Format("/Airlines/Airline[@IATACode='{0}']", code));
            string name = "";
            if (xnList.Count > 0)
            {
                name = xnList[0].Attributes["AirlineName"].Value;
            }
            else
            {
                var airlines = flightDepartureDetails != null && flightDepartureDetails.appendix != null && flightDepartureDetails.appendix.airlines != null && flightDepartureDetails.appendix.airlines.Count() > 0 ? flightDepartureDetails.appendix.airlines.Where(p => p.iata == code && p.active) : null;
                name = (airlines != null && airlines.Count() > 0) ? airlines.FirstOrDefault().name : string.Empty;
            }

            return name;
        }
        private static string FindCardNameBasedOnCardType(string cardType)
        {
            switch (cardType)
            {
                case "AM": return "American Express";
                case "SWI": return "Maestro";
                case "MC":
                    return "Master Credit";
                case "MCD":
                    return "Master Debit";
                case "EL": return "Visa Electron";
                case "VIS": return "Visa Credit";
                case "VISD": return "Visa Debit";
            }

            return string.Empty;
        }

        private static string FindCardImageNameBasedOnCardType(string cardType)
        {
            switch (cardType)
            {
                case "AM": return "amex";
                case "SWI": return "maestro";
                case "MC":
                    return "mastercard";
                case "MCD":
                    return "mastercard";
                case "EL": return "visa_electron";
                case "VIS": return "visa";
                case "VISD": return "visa";
            }

            return string.Empty;
        }

        public static TopDogPayments.ClientPaymentPlanElement[] GetInstallmentPlan(string BookingReference, string Surname, string DepartureDate)
        {
            string key = BookingReference + Surname + DepartureDate + "_PaymentPlan";
            TopDogPayments.ClientPaymentPlanElement[] basket = null;
            basket = GetPayments(BookingReference, Surname, DepartureDate);
            return basket;
        }

        private static float GetHotelPriceForGetBasket(TopDogBasketService.Basket basket)
        {
            float totalPrice = 0;

            IEnumerable<TopDogBasketService.BasketProduct> hotelProduct = null;

            if (basket.Products.Length > 0)
            {
                var hotelProducts = basket.Products.Where(a => a.ProductType.ToString() == "HotelProduct");

                hotelProduct = hotelProducts != null && hotelProducts.Count() > 0 ? hotelProducts.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED") : null;

                if (hotelProduct != null && hotelProduct.Count() > 0)
                {
                    var nspPrice = GetNSPPrice(basket);

                    foreach (var product in hotelProduct)
                    {

                        totalPrice = totalPrice + product.ProductPrice.TotalPrice;
                        if (product.Supplements != null && product.Supplements.Count() > 0)
                        {
                            foreach (var supplement in product.Supplements)
                            {
                                totalPrice = totalPrice + supplement.TotalPrice;
                            }
                        }
                    }
                    totalPrice = totalPrice + nspPrice;
                }
            }

            return totalPrice;
        }

        private static float GetNSPPrice(TopDogBasketService.Basket basket)
        {
            if (basket.Charges != null && basket.Charges.Count() > 0)
            {
                float totalPrice = 0;
                foreach (var charge in basket.Charges.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED" && p.Code == "NSP"))
                {
                    totalPrice = totalPrice + charge.Price.Price;
                }
                return totalPrice;
            }

            return 0;
        }

        private static float GetTransferPriceForGetBasket(TopDogBasketService.Basket basket)
        {
            float totalPrice = 0;

            if (basket.Products.Length > 0)
            {
                var transferProducts = basket.Products.Where(a => a.ProductType.ToString() == "TransferProduct");
                var transferProduct = transferProducts != null && transferProducts.Count() > 0 ? transferProducts.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED") : null;

                if (transferProduct != null && transferProduct.Count() > 0)
                {
                    foreach (var product in transferProduct)
                    {

                        totalPrice = totalPrice + product.ProductPrice.TotalPrice;
                        if (product.Supplements != null && product.Supplements.Count() > 0)
                        {
                            foreach (var supplement in product.Supplements)
                            {
                                totalPrice = totalPrice + supplement.TotalPrice;
                            }
                        }
                    }
                }
            }
            return totalPrice;
        }

        private static float GetFlightPriceForGetBasket(TopDogBasketService.Basket basket, MyBookings myBookingDetails)
        {
            float totalPrice = 0;

            IEnumerable<TopDogBasketService.BasketProduct> flightProduct = null;

            if (basket.Products.Length > 0)
            {
                var flightProducts = basket.Products.Where(a => a.ProductType.ToString() == "FlightProduct");
                flightProduct = flightProducts != null && flightProducts.Count() > 0 ? flightProducts.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED") : null;

                if (flightProduct != null && flightProduct.Count() > 0)
                {
                    foreach (var product in flightProduct)
                    {
                        if (product.ProductDetails.FlightProduct.Itinerary != null && product.ProductDetails.FlightProduct.Itinerary.Count() > 0)
                        {
                            totalPrice = totalPrice + product.ProductPrice.TotalPrice;
                            if (product.Supplements != null && product.Supplements.Count() > 0)
                            {
                                foreach (var supplement in product.Supplements)
                                {
                                    if (supplement.Type.ToString() == "BAGGAGE")
                                    {
                                        if (supplement.TotalPrice > 0)
                                        {
                                            myBookingDetails.footerAddbaggagesExists = false;
                                        }
                                    }
                                    totalPrice = totalPrice + supplement.TotalPrice;
                                }
                            }
                        }
                    }
                }
            }
            return totalPrice;
        }

        public static TopDogBasketService.CreatedDocument[] GetDocuments(string BookingReference, string Surname, string DepartureDate)
        {
            try
            {
                var bookingDetails = GetBasket(BookingReference, Surname, DepartureDate, true, false);


                List<string> requiredDocuments = new List<string>();
                if (bookingDetails == null || !string.IsNullOrEmpty(bookingDetails.error))
                {
                    return null;
                }
                else
                {
                    if (bookingDetails.error == NotQualityChecked)
                    {
                        return null;
                    }
                    else
                    {
                        requiredDocuments.Add("INVOICE");
                        requiredDocuments.Add("ATOL_CERTIFICATE_PAGE");
                        requiredDocuments.Add("PAYMENTS_SUMMARY");
                        requiredDocuments.Add("IMPORTANT_INFORMATION");
                        bool fullyPaid = false;

                        foreach (var charge in bookingDetails.payments.charges)
                        {
                            if (charge.Key == "Balance due")
                            {
                                if (double.Parse(charge.Value) <= 0.00)
                                    fullyPaid = true;
                            }
                        }

                        if (fullyPaid)
                        {
                            if (bookingDetails.flightDetails != null && bookingDetails.flightDetails.Count() > 0)
                            {
                                requiredDocuments.Add("TOD_LETTER");
                            }

                            if (bookingDetails.hotelDetails != null && bookingDetails.hotelDetails.Count() > 0)
                            {
                                requiredDocuments.Add("ACCOMMODATION_VOUCHER");
                            }

                            if (bookingDetails.transferDetails != null && bookingDetails.transferDetails.Count() > 0)
                            {
                                requiredDocuments.Add("TRANSFER_VOUCHER");
                            }
                        }

                    }
                }

                var createdDocuments = GetDocumentsFromS3(requiredDocuments, BookingReference, Surname, DepartureDate);
                return createdDocuments;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }

            return null;
        }

        public static Presentation.Web.WCFRestServices.TopDogBasketService.CreatedDocument[] GetDocumentsFromTOPDOG(List<string> requiredDocuments, string BookingReference, string Surname, string DepartureDate)
        {
            string sessionID = string.Empty;
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();

            try
            {

                TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                startSession.Password = ConfigurationManager.AppSettings["Password"];
                startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                startSession1.StartSessionRequest = startSession;
                TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                var startSessionResponse = sessionResponse.StartSessionResponse;
                sessionID = startSessionResponse.SessionId;

                TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                getBasketRequest.BookingReference = BookingReference;
                getBasketRequest.ClientSurname = Surname;
                getBasketRequest.DepartDate = DepartureDate;
                getBasketRequest1.GetBasketRequest = getBasketRequest;
                TopDogBasketService.GetBasketResponse response = basketClient.GetBasket(getBasketRequest);
                var basket = response.Basket;

                TopDogBasketService.CreateInvoiceDocumentsRequest1 createInvoiceDocumentsRequest1 = new TopDogBasketService.CreateInvoiceDocumentsRequest1();
                TopDogBasketService.CreateInvoiceDocumentsRequest createInvoiceDocumentsRequest = new TopDogBasketService.CreateInvoiceDocumentsRequest();
                createInvoiceDocumentsRequest.SessionId = sessionID;
                createInvoiceDocumentsRequest.CustomerDocument = true;
                createInvoiceDocumentsRequest.BookingReferences = new string[] { BookingReference };
                createInvoiceDocumentsRequest.DocumentPages = requiredDocuments.ToArray();
                createInvoiceDocumentsRequest.DoNotCheckProductsStatuses = true;
                var createInvoiceDocumentsResponse = basketClient.CreateInvoiceDocuments(createInvoiceDocumentsRequest);
                var createdDocuments = createInvoiceDocumentsResponse.CreatedDocuments;
                return createdDocuments;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            finally
            {
                TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                endSessionRequest.SessionId = sessionID;
                endSessionRequest1.EndSessionRequest = endSessionRequest;
                var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
            }

            return null;
        }


        public static Presentation.Web.WCFRestServices.TopDogBasketService.CreatedDocument[] GetDocumentsFromS3(List<string> requiredDocuments, string BookingReference, string Surname, string DepartureDate)
        {
            var documents = DownloadObjectsFromS3(ConfigurationManager.AppSettings["BookingAWSBucketName"], BookingReference);
            var pdfFiles = documents.Where(p => p.Key.Contains(".pdf"));
            if (pdfFiles == null || pdfFiles.Count() == 0)
            {
                return GetDocumentsFromTOPDOG(requiredDocuments, BookingReference, Surname, DepartureDate);
            }

            List<Presentation.Web.WCFRestServices.TopDogBasketService.CreatedDocument> createdDocuments = new List<TopDogBasketService.CreatedDocument>();
            Presentation.Web.WCFRestServices.TopDogBasketService.CreatedDocument createdDocument = new TopDogBasketService.CreatedDocument();
            List<Presentation.Web.WCFRestServices.TopDogBasketService.PrintedDocument> printedDocuments = new List<TopDogBasketService.PrintedDocument>();
            Presentation.Web.WCFRestServices.TopDogBasketService.PrintedDocument printedDocument = new TopDogBasketService.PrintedDocument();


            foreach (var requiredDocument in requiredDocuments)
            {
                if (requiredDocument == "TOD_LETTER")
                {
                    var flightVouchers = documents.Where(p => p.Key.Contains("Flight-Vouchers") || p.Key.Contains("E-Tickets-Details") || p.Key.Contains("Tickets-Letter"));
                    if (flightVouchers != null && flightVouchers.Count() > 0)
                    {
                        var latestFlightVouchers = flightVouchers.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();

                        foreach (var flightVoucher in latestFlightVouchers)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.E-TicketsDetails.Combined";
                            printedDocument.DocumentType.Description = "Combined E-Ticket Details";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + flightVoucher.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }

                }
                else if (requiredDocument == "ACCOMMODATION_VOUCHER")
                {
                    var accomVouchers = documents.Where(p => p.Key.Contains("Accommodation-Voucher"));
                    if (accomVouchers != null && accomVouchers.Count() > 0)
                    {
                        var latestAccomVouchers = accomVouchers.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var accomVoucher in latestAccomVouchers)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.Accommodation.Voucher.Supplier";
                            printedDocument.DocumentType.Description = "Accommodation Voucher";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + accomVoucher.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }

                }
                else if (requiredDocument == "TRANSFER_VOUCHER")
                {
                    var transferVouchers = documents.Where(p => p.Key.Contains("Transfer-Voucher"));
                    if (transferVouchers != null && transferVouchers.Count() > 0)
                    {
                        var latestTransferVouchers = transferVouchers.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var transferVoucher in latestTransferVouchers)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.TransferVoucher";
                            printedDocument.DocumentType.Description = "Transfer Voucher";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + transferVoucher.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }
                }
                else if (requiredDocument == "PAYMENTS_SUMMARY")
                {
                    var paymentSummaries = documents.Where(p => p.Key.Contains("Payment-Invoice") || p.Key.Contains("Payments-Summary"));
                    if (paymentSummaries != null && paymentSummaries.Count() > 0)
                    {
                        var latestPaymentSummaries = paymentSummaries.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var paymentSummary in latestPaymentSummaries)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.PaymentsSummary";
                            printedDocument.DocumentType.Description = "Payment Summary Page - Confirmation Invoice";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + paymentSummary.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }
                }
                else if (requiredDocument == "ATOL_CERTIFICATE_PAGE")
                {
                    var atolCerts = documents.Where(p => p.Key.Contains("ATOL-Certification") || p.Key.Contains("ATOL-Certificate"));
                    if (atolCerts != null && atolCerts.Count() > 0)
                    {
                        var latestAtolCerts = atolCerts.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var atolCert in latestAtolCerts)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "ATOL-CERTIFICATE";
                            printedDocument.DocumentType.Description = "ATOL Certificate";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + atolCert.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }
                }
                else if (requiredDocument == "INVOICE")
                {
                    var invoiceDocuments = documents.Where(p => p.Key.Contains("Flight-Invoice"));
                    if (invoiceDocuments != null && invoiceDocuments.Count() > 0)
                    {
                        var latestInvoiceDocs = invoiceDocuments.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var latestInvoiceDoc in latestInvoiceDocs)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.ATOL.Receipt";
                            printedDocument.DocumentType.Description = "Flight Invoice";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + latestInvoiceDoc.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }

                    invoiceDocuments = documents.Where(p => p.Key.Contains("Accommodation-Invoice"));
                    if (invoiceDocuments != null && invoiceDocuments.Count() > 0)
                    {
                        var latestInvoiceDocs = invoiceDocuments.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var latestInvoiceDoc in latestInvoiceDocs)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.RSI.Accom";
                            printedDocument.DocumentType.Description = "Accommodation Invoice";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + latestInvoiceDoc.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }

                    invoiceDocuments = documents.Where(p => p.Key.Contains("Transfer-Invoice"));
                    if (invoiceDocuments != null && invoiceDocuments.Count() > 0)
                    {
                        var latestInvoiceDocs = invoiceDocuments.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var latestInvoiceDoc in latestInvoiceDocs)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.RSI.Transfer";
                            printedDocument.DocumentType.Description = "Transfer Invoice";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + latestInvoiceDoc.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }

                    invoiceDocuments = documents.Where(p => p.Key.Contains("Additional-Invoice"));
                    if (invoiceDocuments != null && invoiceDocuments.Count() > 0)
                    {
                        var latestInvoiceDocs = invoiceDocuments.GroupBy(p => p.LastModified.Date).Select(group => group.OrderByDescending(doc => doc.LastModified)).FirstOrDefault();
                        foreach (var latestInvoiceDoc in latestInvoiceDocs)
                        {
                            printedDocument = new TopDogBasketService.PrintedDocument();
                            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
                            printedDocument.DocumentType.Code = "Admin.RSI.AdditionalExtras";
                            printedDocument.DocumentType.Description = "Additional Invoice";
                            printedDocument.DocumentFormat = "PDF";
                            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/" + latestInvoiceDoc.Key;
                            printedDocuments.Add(printedDocument);
                        }
                    }
                }
            }

            printedDocument = new TopDogBasketService.PrintedDocument();
            printedDocument.DocumentType = new TopDogBasketService.PrintedDocumentType();
            printedDocument.DocumentType.Code = "Admin.ImportantInformation";
            printedDocument.DocumentType.Description = "Important Information";
            printedDocument.DocumentFormat = "PDF";
            printedDocument.DocumentUrl = "https://teletext-bookings.s3.amazonaws.com/ImportantInformation.pdf";
            printedDocuments.Add(printedDocument);

            createdDocument.DocumentList = printedDocuments.ToArray();
            createdDocuments.Add(createdDocument);
            return createdDocuments.ToArray();

        }

        public static MyBookings GetQuickQuoteDetails(string BookingReference, string DepartureDate)
        {
            Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket = null;
            MyBookings myBookingDetails = new MyBookings();

            try
            {
                var documents = DownloadObjectsFromS3(ConfigurationManager.AppSettings["BookingAWSBucketName"], BookingReference);
                var basketXML = documents.Where(p => p.Key.Contains(".xml"));
                if (basketXML == null || basketXML.Count() == 0)
                {
                    throw new Exception();
                }

                Stream quickQuoteDetails = Utilities.DownloadS3Object(ConfigurationManager.AppSettings["BookingAWSBucketName"], basketXML.OrderByDescending(p => p.LastModified).FirstOrDefault().Key);
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Presentation.Web.WCFRestServices.TopDogBasketService.Basket), "http://topdog.uk.net/BlackBox/BasketOperations");
                byte[] bytes = new byte[quickQuoteDetails.Length];
                quickQuoteDetails.Read(bytes, 0, (int)quickQuoteDetails.Length);

                string quickQuoteString = Encoding.ASCII.GetString(bytes);
                var basketData = quickQuoteString.Replace("<QuickQuote ", "<Basket ").Replace("</QuickQuote>", "</Basket>");
                quickQuoteDetails = new MemoryStream();
                StreamWriter writer = new StreamWriter(quickQuoteDetails);
                writer.Write(basketData);
                writer.Flush();
                quickQuoteDetails.Position = 0;

                basket = (Presentation.Web.WCFRestServices.TopDogBasketService.Basket)x.Deserialize(quickQuoteDetails);
                var products = basket.Products;
                if (products != null && products.Count() > 0)
                {
                    if (!products.FirstOrDefault().FromDate.Contains(DepartureDate))
                    {
                        myBookingDetails.error = "Sorry, we can't find a quote details with those details. Please check the details and try again.";
                        return myBookingDetails;
                    }
                }
                myBookingDetails = GetQuickQuoteDetails(basket, DepartureDate);
                Task.Run(() => GetQuickQuoteDetailsFromTopDog(BookingReference, DepartureDate, true));
            }
            catch (Exception ex)
            {
                myBookingDetails = GetQuickQuoteDetailsFromTopDog(BookingReference, DepartureDate, false);
            }

            return myBookingDetails;
        }

        public static MyBookings GetQuickQuoteDetailsFromTopDog(string BookingReference, string DepartureDate, bool isAsync)
        {
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
            string sessionID = string.Empty;

            try
            {
                TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                startSession.Password = ConfigurationManager.AppSettings["Password"];
                startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                startSession1.StartSessionRequest = startSession;
                TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                var startSessionResponse = sessionResponse.StartSessionResponse;
                sessionID = startSessionResponse.SessionId;

                TopDogBasketService.GetQuickQuoteDetailsRequest1 getQuickQuoteRequest1 = new TopDogBasketService.GetQuickQuoteDetailsRequest1();
                TopDogBasketService.GetQuickQuoteDetailsRequest getQuickQuoteRequest = new TopDogBasketService.GetQuickQuoteDetailsRequest();
                getQuickQuoteRequest.SessionId = sessionID;
                getQuickQuoteRequest.Reference = BookingReference;

                getQuickQuoteRequest1.GetQuickQuoteDetailsRequest = getQuickQuoteRequest;
                TopDogBasketService.GetQuickQuoteDetailsResponse response = basketClient.GetQuickQuoteDetails(getQuickQuoteRequest);
                var basket = response.QuickQuote;
                MyBookings myBookingDetails = new MyBookings();
                if (!isAsync)
                {
                    if (basket == null && response.Errors.Length != 0 || basket.Products == null)
                    {
                        try
                        {
                            if (response.Errors[0].Contains("Concurrent modification error."))
                                myBookingDetails.error = "Sorry, we are unable to display your quote details at the moment. Please try again later.";
                            else if (response.Errors[0].Contains("but 0 objects found"))
                                myBookingDetails.error = "Sorry, we can't find a quote details with those details. Please check the details and try again.";
                        }
                        catch
                        {
                            myBookingDetails.error = "Sorry, we are unable to display your quote details at the moment. Please try again later.";
                        }
                    }
                    else
                    {
                        myBookingDetails = GetQuickQuoteDetails(basket, DepartureDate);

                    }
                }
                return myBookingDetails;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }
            finally
            {
                TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                endSessionRequest.SessionId = sessionID;
                endSessionRequest1.EndSessionRequest = endSessionRequest;
                var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
            }

            return null;
        }

        public static MyBookings GetQuickQuoteDetails(Presentation.Web.WCFRestServices.TopDogBasketService.Basket basket, string DepartureDate)
        {
            MyBookings myBookingDetails = new MyBookings();
            /*     Departure Date Validation start        */
            DateTime topDogDepDate = GetDepartureDateFromTopDogResponse(basket);

            DateTime depDate = new DateTime(Convert.ToInt32(DepartureDate.Split('-')[0]), Convert.ToInt32(DepartureDate.Split('-')[1]), Convert.ToInt32(DepartureDate.Split('-')[2]));

            if (!depDate.Equals(new DateTime(topDogDepDate.Year, topDogDepDate.Month, topDogDepDate.Day)))
            {
                myBookingDetails.error = "Departure date is not matching";
                return myBookingDetails;
            }
            /*     Departure Date Validation End          */
            else
            {
                List<Hotelproduct> hotelProdList = new List<Hotelproduct>();
                List<Flightproduct> flightProdList = new List<Flightproduct>();
                List<Transferproduct> transprodList = new List<Transferproduct>();
                var list = new List<KeyValuePair<string, string>>();
                myBookingDetails.cost = new ProductCost();
                myBookingDetails.cost.hotelGrossPrice = GetHotelPriceForQQDetails(basket);
                myBookingDetails.cost.paymentCharges = GetPaymentCharges(basket);
                myBookingDetails.flightSupplimentList = new List<FlightSuppliment>();
                Hotelproduct hotelProd = null;
                Flightproduct flightProd = null;
                Transferproduct transProd = null;
                int accomodationCount = 0;

                if (basket != null)
                {
                    myBookingDetails.cost.totalPrice = basket.TotalPrice;

                    #region Passenger
                    if (basket.Passengers != null)
                    {
                        myBookingDetails.passengers = new List<Passenger>();
                        for (int passenger = 0; passenger < basket.Passengers.Length; passenger++)
                        {
                            Passenger passengerObj = new Passenger();

                            passengerObj.passengerId = basket.Passengers[passenger].PassengerId;
                            passengerObj.isLeadPassenger = basket.Passengers[passenger].BasketLeadPassenger;
                            passengerObj.title = basket.Passengers[passenger].Title;
                            passengerObj.firstName = basket.Passengers[passenger].FirstName;
                            passengerObj.secondName = basket.Passengers[passenger].SecondName;
                            passengerObj.surname = basket.Passengers[passenger].Surname;
                            passengerObj.age = basket.Passengers[passenger].Age.Value;
                            myBookingDetails.passengers.Add(passengerObj);
                        }
                    }
                    #endregion

                    #region payments
                    if (basket.Payments != null)
                    {
                        double paymentrefund = 0.0;
                        double paymentreceived = 0.0;
                        double paymentcharges = 0.0;
                        for (int pay = 0; pay < basket.Payments.Length; pay++)
                        {
                            if (basket.Payments[pay].Refund == false && basket.Payments[pay].PaymentStatus == 0)
                            {
                                paymentreceived += basket.Payments[pay].PaymentValue;
                                paymentcharges += basket.Payments[pay].PaymentCharge;
                            }
                            if (basket.Payments[pay].Refund == true)
                            {
                                paymentrefund += basket.Payments[pay].PaymentValue;
                            }
                        }
                        if (paymentreceived >= paymentrefund)
                        {
                            paymentreceived -= paymentrefund;
                        }

                        Payment payment = new Payment();
                        list.Add(new KeyValuePair<string, string>("Payment received", Math.Round(paymentreceived, 2).ToString("0.00")));
                        list.Add(new KeyValuePair<string, string>("Payment charges", Math.Round(paymentcharges, 2).ToString("0.00")));
                        list.Add(new KeyValuePair<string, string>("Total Price", Math.Round(basket.TotalPrice, 2).ToString("0.00")));
                        myBookingDetails.payments = payment;
                    }
                    #endregion


                    #region Product
                    if (basket.Products != null)
                    {
                        for (int product = 0; product < basket.Products.Length; product++)
                        {
                            #region ifcond
                            if ((int)basket.Products[product].StatusCode == 6 || (int)basket.Products[product].StatusCode == 12)
                            {
                                switch ((int)basket.Products[product].ProductType)
                                {
                                    case 0:
                                        hotelProd = new Hotelproduct();
                                        hotelProd.topDogProductId = basket.Products[product].ProductId;
                                        hotelProd.supplierRef = basket.Products[product].OperatorBookRef;
                                        hotelProd.CheckInDate = basket.Products[product].FromDate;
                                        hotelProd.CheckOutDate = basket.Products[product].ToDate;
                                        hotelProd.hotelCode = basket.Products[product].ProductDetails.HotelProduct.HotelCode;
                                        hotelProd.hotelName = basket.Products[product].ProductDetails.HotelProduct.HotelName;
                                        hotelProd.category = basket.Products[product].ProductDetails.HotelProduct.Category;
                                        hotelProd.roomType = basket.Products[product].ProductDetails.HotelProduct.RoomType;
                                        hotelProd.boardType = basket.Products[product].ProductDetails.HotelProduct.BoardType;
                                        hotelProd.resortName = basket.Products[product].ProductDetails.HotelProduct.ResortName;
                                        hotelProd.adultsCount = basket.Products[product].AdultsCount;
                                        hotelProd.childrenCount = basket.Products[product].ChildrenCount;
                                        hotelProd.infantsCount = basket.Products[product].InfantsCount;
                                        hotelProd.clientNotes = basket.Products[product].ClientNotes;
                                        accomodationCount++;

                                        hotelProdList.Add(hotelProd);

                                        myBookingDetails.hotelDetails = hotelProdList;
                                        break;

                                    case 1:

                                        FlightSuppliment FlightSupp = new FlightSuppliment();
                                        FlightSupp.topDogProductId = basket.Products[product].ProductId;
                                        FlightSupp.supplements = new List<Supplements>();

                                        if (basket.Products[product].Supplements != null)
                                        {
                                            for (int i = 0; i < basket.Products[product].Supplements.Length; i++)
                                            {
                                                TopDogBasketService.ProductSupplement prdSupp = basket.Products[product].Supplements[i];
                                                Supplements supp = new Supplements();
                                                supp.Code = prdSupp.Code;
                                                supp.Description = prdSupp.Description;
                                                supp.FullDescription = prdSupp.FullDescription;
                                                supp.TypeSpecified = prdSupp.TypeSpecified;
                                                supp.CurrencyCode = prdSupp.CurrencyCode;
                                                supp.TotalPrice = prdSupp.TotalPrice;
                                                supp.GrossPrice = prdSupp.GrossPrice;
                                                supp.NetPrice = prdSupp.NetPrice;
                                                supp.ClientCommissionRate = prdSupp.ClientCommissionRate;
                                                supp.ClientCommission = prdSupp.ClientCommission;
                                                supp.VatRate = prdSupp.VatRate;
                                                supp.ClientVAT = prdSupp.ClientVAT;
                                                supp.TotalVAT = prdSupp.TotalVAT;
                                                if (prdSupp.TypeSpecified)
                                                {
                                                    supp.Type = (int)prdSupp.Type;
                                                }

                                                if (prdSupp.AdultPrices != null)
                                                {
                                                    supp.AdultPrices = new FlightPerPaxPrice();
                                                    supp.AdultPrices.PerPaxPrice = prdSupp.AdultPrices.PerPaxPrice;
                                                    supp.AdultPrices.Quantity = prdSupp.AdultPrices.Quantity;
                                                }

                                                if (prdSupp.ChildPrices != null)
                                                {
                                                    supp.ChildPrices = new FlightPerPaxPrice();
                                                    supp.ChildPrices.PerPaxPrice = prdSupp.ChildPrices.PerPaxPrice;
                                                    supp.ChildPrices.Quantity = prdSupp.ChildPrices.Quantity;
                                                }

                                                if (prdSupp.InfantPrices != null)
                                                {
                                                    supp.InfantPrices = new FlightPerPaxPrice();
                                                    supp.InfantPrices.PerPaxPrice = prdSupp.InfantPrices.PerPaxPrice;
                                                    supp.InfantPrices.Quantity = prdSupp.InfantPrices.Quantity;
                                                }

                                                FlightSupp.supplements.Add(supp);
                                            }
                                        }
                                        myBookingDetails.cost.flightGrossPrice = myBookingDetails.cost.flightGrossPrice + basket.Products[product].GrossPrice;

                                        myBookingDetails.flightSupplimentList.Add(FlightSupp);

                                        for (int j = 0; j < basket.Products[product].ProductDetails.FlightProduct.Itinerary.Length; j++)
                                        {

                                            for (int k = 0; k < basket.Products[product].ProductDetails.FlightProduct.Itinerary[j].Length; k++)
                                            {
                                                flightProd = new Flightproduct();
                                                flightProd.topDogProductId = basket.Products[product].ProductId;
                                                flightProd.supplierRef = basket.Products[product].OperatorBookRef;
                                                flightProd.id = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].Id;
                                                flightProd.idSpecified = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].IdSpecified;
                                                flightProd.flightNumber = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].FlightNumber;
                                                string flightNumber = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].FlightNumber;
                                                flightProd.operatingAirlineCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].OperatingAirlineCode;
                                                flightProd.departureAirportCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureAirportCode;
                                                flightProd.departureAirportName = TopDogHelperModule.GetAirportName(flightProd.departureAirportCode);
                                                flightProd.arrivalAirportCode = basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].ArrivalAirportCode;
                                                flightProd.arrivalAirportName = TopDogHelperModule.GetAirportName(flightProd.arrivalAirportCode);
                                                flightProd.departureDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime);
                                                flightProd.arrivalDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].ArrivalDateTime);
                                                var departureDate = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime);
                                                flightProd.adultsCount = basket.Products[product].AdultsCount;
                                                flightProd.childrenCount = basket.Products[product].ChildrenCount;
                                                flightProd.infantsCount = basket.Products[product].InfantsCount;
                                                var flightScheduleByFlightNumberAndFlightDateURL = "http://contentservice.teletextholidays.co.uk/GetJsonService.svc/GetFlightScheduleByFlightNumberAndFlightDate?flightNumber=" + flightNumber + "&day=" + departureDate.ToString("dd") + "&month=" + departureDate.ToString("MM") + "&year=" + departureDate.ToString("yyyy");
                                                var flightScheduleByFlightNumberAndFlightDateURLResponse = Utilities.ExecuteGetWebRequest(flightScheduleByFlightNumberAndFlightDateURL);
                                                var flightScheduleByFlightNumberAndFlightDateURLResponseObject = JsonConvert.DeserializeObject<FlightScheduleResponse>(flightScheduleByFlightNumberAndFlightDateURLResponse);
                                                if (flightScheduleByFlightNumberAndFlightDateURLResponseObject != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.airlines != null && flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.equipments != null)
                                                {
                                                    flightProd.operatingAirlineType = flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.airlines[0].name + "(" + flightScheduleByFlightNumberAndFlightDateURLResponseObject.appendix.equipments[0].name + ")";
                                                }
                                                flightProd.FreeHandBagCount = basket.Products[product].AdultsCount + basket.Products[product].ChildrenCount;
                                                if (basket.Products[product].Supplements != null)
                                                {
                                                    for (int i = 0; i < basket.Products[product].Supplements.Length; i++)
                                                    {
                                                        TopDogBasketService.ProductSupplement prdSupp = basket.Products[product].Supplements[i];

                                                        if (prdSupp.TypeSpecified)
                                                        {
                                                            int typ = (int)prdSupp.Type;
                                                            if (typ == 1)
                                                            {
                                                                if (string.IsNullOrEmpty(flightProd.AdditionalCheckInBag))
                                                                {
                                                                    flightProd.AdditionalCheckInBag = prdSupp.Description;
                                                                }
                                                                else
                                                                {
                                                                    flightProd.AdditionalCheckInBag = flightProd.AdditionalCheckInBag + ", " + prdSupp.Description;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                list.Add(new KeyValuePair<string, string>(" Flight (" + flightProd.departureAirportCode + "-" + flightProd.arrivalAirportCode + ")", Math.Round(basket.Products[product].ProductPrice.TotalPrice, 2).ToString("0.00")));

                                                flightProdList.Add(flightProd);
                                            }
                                        }
                                        myBookingDetails.flightDetails = flightProdList;

                                        break;

                                    case 4:
                                        transProd = new Transferproduct();
                                        transProd.topDogProductId = basket.Products[product].ProductId;
                                        transProd.supplierRef = basket.Products[product].OperatorBookRef;
                                        transProd.transferName = basket.Products[product].ProductDetails.TransferProduct.Name;
                                        transProd.transferType = "Arrival";
                                        transProd.pickUpLocation = TopDogHelperModule.GetAirportName(basket.Products[product].ProductDetails.TransferProduct.Airport) + " (" + basket.Products[product].ProductDetails.TransferProduct.Airport + ")";
                                        transProd.transferMinutes = basket.Products[product].ProductDetails.TransferProduct.TransferMinutes;
                                        transProd.pickUpDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.ArrivalJourney.PickupDateTime);
                                        transProd.dropLocation = basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.ArrivalJourney.Accomodation.Name;
                                        transProd.clientNotes = basket.Products[product].ClientNotes;
                                        myBookingDetails.cost.transportGrossPrice = myBookingDetails.cost.transportGrossPrice + basket.Products[product].GrossPrice;
                                        transprodList.Add(transProd);

                                        transProd = new Transferproduct();
                                        transProd.topDogProductId = basket.Products[product].ProductId;
                                        transProd.supplierRef = basket.Products[product].OperatorBookRef;
                                        transProd.transferName = basket.Products[product].ProductDetails.TransferProduct.Name;
                                        transProd.transferType = "Return";
                                        transProd.dropLocation = TopDogHelperModule.GetAirportName(basket.Products[product].ProductDetails.TransferProduct.Airport) + " (" + basket.Products[product].ProductDetails.TransferProduct.Airport + ")"; ;
                                        transProd.transferMinutes = basket.Products[product].ProductDetails.TransferProduct.TransferMinutes;
                                        transProd.pickUpDateTime = Convert.ToDateTime(basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.DepartureJourney.PickupDateTime);
                                        transProd.pickUpLocation = basket.Products[product].ProductDetails.TransferProduct.TransferExtraInfo.DepartureJourney.Accomodation.Name;
                                        transProd.clientNotes = basket.Products[product].ClientNotes;
                                        transprodList.Add(transProd);

                                        list.Add(new KeyValuePair<string, string>(" Transfer", Math.Round(basket.Products[product].ProductPrice.TotalPrice, 2).ToString("0.00")));
                                        myBookingDetails.transferDetails = transprodList;
                                        break;
                                }
                            }
                            #endregion
                        }

                        if (myBookingDetails.flightDetails != null && myBookingDetails.flightDetails.Count() > 0)
                        {
                            var flightProducts = myBookingDetails.flightDetails.OrderBy(p => p.arrivalDateTime).ToList();
                            myBookingDetails.DestinationName = TopDogHelperModule.GetDestinationName(flightProducts[(flightProducts.Count / 2) - 1].arrivalAirportCode);
                        }
                    }
                    #endregion

                    #region charge

                    double price = 0.0;

                    if (basket.Charges != null)
                    {

                        for (int charge = 0; charge < basket.Charges.Length; charge++)
                        {
                            if (basket.Charges[charge].Code == "NSP" && basket.Charges[charge].StatusCode == 0)
                            {
                                price += basket.Charges[charge].Price.Price;
                            }
                        }


                        price = Math.Round(price, 2);
                        int count = 0;
                        if (basket.Products != null)
                        {
                            for (int prod = 0; prod < basket.Products.Length && count < accomodationCount; prod++)
                            {
                                if (basket.Products[prod].ProductType == 0 && (int)basket.Products[prod].StatusCode == 6)
                                {
                                    list.Add(new KeyValuePair<string, string>("Accomodation (Room " + (count + 1) + ")", Math.Round(basket.Products[prod].ProductPrice.TotalPrice + (price / accomodationCount), 2).ToString("0.00")));
                                    count++;
                                }
                            }
                        }
                    }
                    if (basket.Payments != null)
                    {
                        myBookingDetails.payments.charges = list;
                    }

                    #endregion

                    #region Clients

                    Client client = new Client();
                    client.title = basket.Client.Title;
                    client.firstName = basket.Client.FirstName;
                    client.surname = basket.Client.Surname;
                    myBookingDetails.client = client;

                    #endregion

                    #region generalobj

                    myBookingDetails.bookingDate = Convert.ToDateTime(basket.BookingDate);
                    myBookingDetails.bookingReference = basket.BookingReference;
                    myBookingDetails.status = basket.Status;

                    #endregion

                    #region Balance

                    list.Add(new KeyValuePair<string, string>("Final installment due date", basket.Balance.BalanceDue));
                    list.Add(new KeyValuePair<string, string>("Balance Due", Math.Round(basket.Balance.BalanceValue, 2).ToString("0.00")));
                    #endregion

                    #region Deposit

                    Deposit deposit = new Deposit();
                    deposit.depositValue = Math.Round(basket.Deposit.DepositValue, 2);
                    deposit.depositDue = basket.Deposit.DepositDue;
                    if (basket.Payments != null)
                        myBookingDetails.payments.deposit = deposit;
                    #endregion

                    #region LowDeposit

                    Lowdeposit lowDepo = new Lowdeposit();
                    lowDepo.lowDepositDue = basket.LowDeposit.LowDepositDue;
                    lowDepo.lowDepositValue = Math.Round(basket.LowDeposit.LowDepositValue, 2);
                    if (basket.Payments != null)
                        myBookingDetails.payments.lowDeposit = lowDepo;
                    #endregion

                    #region BookedBy

                    Bookedby bookBy = new Bookedby();
                    bookBy.name = basket.BookedBy.Name;
                    bookBy.emailAddress = basket.BookedBy.EmailAddress;
                    myBookingDetails.bookedBy = bookBy;

                    #endregion

                }
            }

            return myBookingDetails;

        }

        private static float GetHotelPriceForQQDetails(TopDogBasketService.Basket basket)
        {
            float totalPrice = 0;

            IEnumerable<TopDogBasketService.BasketProduct> hotelProduct = null;

            if (basket.Products.Length > 0)
            {
                var hotelProducts = basket.Products.Where(a => a.ProductType.ToString() == "HotelProduct");

                hotelProduct = hotelProducts != null && hotelProducts.Count() > 0 ? hotelProducts.Where(p => p.StatusCode.ToString() == "QUOTED") : null;

                if (hotelProduct != null && hotelProduct.Count() > 0)
                {
                    var nspPrice = GetNSPPrice(basket);

                    foreach (var product in hotelProduct)
                    {

                        totalPrice = totalPrice + product.ProductPrice.TotalPrice;
                        if (product.Supplements != null && product.Supplements.Count() > 0)
                        {
                            foreach (var supplement in product.Supplements)
                            {
                                totalPrice = totalPrice + supplement.TotalPrice;
                            }
                        }
                    }
                    totalPrice = totalPrice + nspPrice;
                }
            }

            return totalPrice;
        }

        private static float GetPaymentCharges(TopDogBasketService.Basket basket)
        {
            float totalPrice = 0;
            if (basket.Charges != null && basket.Charges.Count() > 0)
            {

                foreach (var charge in basket.Charges.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED"))
                {
                    if (charge.Code.ToString() != "NSP")
                    {
                        totalPrice = totalPrice + charge.Price.Price;
                    }
                }
            }
            return totalPrice;
        }

        private static DateTime GetDepartureDateFromTopDogResponse(TopDogBasketService.Basket basket)
        {
            DateTime depDate = new DateTime(2100, 1, 1);
            DateTime minFlightDt = new DateTime(2100, 1, 1);
            DateTime minHotelDt = new DateTime(2100, 1, 1);
            DateTime hotelDepDate;
            DateTime flightDepDate;


            if (basket.Products != null)
            {
                for (int product = 0; product < basket.Products.Length; product++)
                {
                    if ((int)basket.Products[product].StatusCode == 6 || (int)basket.Products[product].StatusCode == 12)
                    {
                        switch ((int)basket.Products[product].ProductType)
                        {
                            case 0:
                                hotelDepDate = new DateTime(Convert.ToInt32(basket.Products[product].FromDate.Split('-')[0]), Convert.ToInt32(basket.Products[product].FromDate.Split('-')[1]), Convert.ToInt32(basket.Products[product].FromDate.Split('-')[2]));
                                if (minHotelDt > hotelDepDate)
                                {
                                    minHotelDt = hotelDepDate;
                                }

                                break;

                            case 1:
                                for (int j = 0; j < basket.Products[product].ProductDetails.FlightProduct.Itinerary.Length; j++)
                                {

                                    for (int k = 0; k < basket.Products[product].ProductDetails.FlightProduct.Itinerary[j].Length; k++)
                                    {
                                        flightDepDate = Convert.ToDateTime(basket.Products[product].ProductDetails.FlightProduct.Itinerary[j][k].DepartureDateTime);
                                        if (minFlightDt > flightDepDate)
                                        {
                                            minFlightDt = flightDepDate;
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            if (!minFlightDt.Equals(depDate))
            {
                depDate = minFlightDt;
            }
            else
            {
                depDate = minHotelDt;
            }
            return depDate;
        }

        public static TopDogPayments.ClientPaymentPlanElement[] GetPayments(string BookingReference, string Surname, string DepartureDate)
        {
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogPayments.PaymentsOperationsClient paymentOperationsClient = new TopDogPayments.PaymentsOperationsClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
            string sessionID = string.Empty;

            try
            {
                TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                startSession.Password = ConfigurationManager.AppSettings["Password"];
                startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                startSession1.StartSessionRequest = startSession;
                TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                var startSessionResponse = sessionResponse.StartSessionResponse;
                sessionID = startSessionResponse.SessionId;

                TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                getBasketRequest.SessionId = sessionID;
                getBasketRequest.BookingReference = BookingReference;
                getBasketRequest.ClientSurname = Surname;
                getBasketRequest.DepartDate = DepartureDate;
                getBasketRequest1.GetBasketRequest = getBasketRequest;
                TopDogBasketService.GetBasketResponse response = basketClient.GetBasket(getBasketRequest);
                var basket = response.Basket;

                TopDogPayments.getClientPaymentPlanRequest1 getClientPaymentPlanRequest1 = new TopDogPayments.getClientPaymentPlanRequest1();
                TopDogPayments.GetClientPaymentPlanRequest getClientPaymentPlanRequest = new TopDogPayments.GetClientPaymentPlanRequest();
                getClientPaymentPlanRequest.SessionId = sessionID;
                var getClientPaymentPlanResponse = paymentOperationsClient.getClientPaymentPlan(getClientPaymentPlanRequest);
                var clientPaymentPlanElements = getClientPaymentPlanResponse.ClientPaymentPlanElements;
                return clientPaymentPlanElements;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                endSessionRequest.SessionId = sessionID;
                endSessionRequest1.EndSessionRequest = endSessionRequest;
                var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
            }

            return null;
        }

        public static void GetSupplierDetails(string supplierId, ref string supplierName, ref string supplierCheckinLink)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(HttpRuntime.AppDomainAppPath + @"data\SupplierDetails.xml");
            XmlNodeList xnList = xml.SelectNodes(string.Format("/Suppliers/Supplier[@id='{0}']", supplierId));
            if (xnList.Count > 0)
            {
                supplierName = xnList[0].Attributes["name"].Value;
                supplierCheckinLink = xnList[0].Attributes["checkinLink"].Value;
            }
        }

        public static List<S3Object> DownloadObjectsFromS3(string awsBucketName, string keyName)
        {

            string awsAccessKeyId = ConfigurationManager.AppSettings["AWSAcessKeyId"];
            string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSSceretAccessKeyId"];
            Stream imageStream = new MemoryStream();
            try
            {
                using (var client = Amazon.AWSClientFactory.CreateAmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                {
                    ListObjectsRequest request1 = new ListObjectsRequest();
                    request1.BucketName = awsBucketName;
                    request1.Prefix = keyName;
                    ListObjectsResponse response1 = client.ListObjects(request1);
                    return response1.S3Objects;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static string GetClientEmail(TopDogBasketService.Basket basket)
        {
            if (basket.Client != null && basket.Client.Emails != null && basket.Client.Emails.Count() > 0)
            {
                return basket.Client.Emails[0].Value;
            }

            return string.Empty;
        }

        private static void CheckUniqueFlightAdditionalDetails(List<Flightproduct> flightDetails)
        {
            var distinctList = flightDetails.Select(p => new { p.adultsCount, p.AdditionalCheckInBag, p.childrenCount, p.infantsCount }).Distinct();
            if (distinctList.Count() <= 1)
            {
                flightDetails.Take(flightDetails.Count - 1).ToList().ForEach(p =>
                {
                    p.HideAdditionalInfo = true;
                });
            }
        }

        private static void BuildAncillaries(List<Flightproduct> flightDetails, TopDogBasketService.Basket basket, MyBookings bookingDetails)
        {
            IFormatProvider culture = new CultureInfo("en-US", true);
            var departureAirport = flightDetails.OrderBy(a => a.departureDateTime).FirstOrDefault();
            var arrivingTime = flightDetails.OrderByDescending(a => a.arrivalDateTime).Select(a => a.arrivalDateTime).FirstOrDefault();
            var emailAddress = GetClientEmail(basket);
            var roomType = GetPassengersRoomType(basket);


            string parking = string.Format("http://trip.holidayextras.co.uk/partners/c/availability/?agentgroup=SS&channel=I&agent=WK394&product=cp&request=1&profile=teletext&domain=app.holidayextras.co.uk&lang=en&selectProduct=cp&Location={0}&ArrivalTime={1}&ArrivalDate={2}&DepartTime={3}&DepartDate={4}&Email={5}&flight_number={6}&rule_set=teletext&LaunchID=TeletextMMBparking", departureAirport.departureAirportCode, departureAirport.departureDateTime.AddHours(-2).ToString("HH:mm"), String.Format("{0:dd/MM/yy}", departureAirport.departureDateTime), arrivingTime.AddHours(1).ToString("HH:mm"), String.Format("{0:dd/MM/yy}", arrivingTime), emailAddress, departureAirport.flightNumber);

            string hotelwithCarParking = string.Empty;
            if (!string.IsNullOrEmpty(roomType))
            {
                hotelwithCarParking = string.Format("http://trip.holidayextras.co.uk/partners/c/availability/?agentgroup=SS&channel=I&agent=WK394&product=hcp&request=1&profile=teletext&domain=app.holidayextras.co.uk&lang=en&selectProduct=hcp&Location={0}&ArrivalTime={1}&ArrivalDate={2}&DepartTime={3}&DepartDate={4}&RoomType[0]={5}&Email={6}&flight_number={7}&rule_set=teletext&LaunchID=TeletextMMBhotelparking", departureAirport.departureAirportCode, departureAirport.departureDateTime.ToString("HH:mm"), String.Format("{0:dd/MM/yy}", departureAirport.departureDateTime), arrivingTime.ToString("HH:mm"), String.Format("{0:dd/MM/yy}", arrivingTime), roomType, emailAddress, departureAirport.flightNumber);
            }
            else
            {
                hotelwithCarParking = "http://www.holidayextras.co.uk/teletextholidays/airport-hotels-parking.html";
            }

            string hotelOnly = string.Empty;

            if (!string.IsNullOrEmpty(roomType))
            {
                hotelOnly = string.Format("http://trip.holidayextras.co.uk/partners/c/availability/?agentgroup=SS&channel=I&agent=WK394&product=ho&request=1&profile=teletext&domain=app.holidayextras.co.uk&lang=en&selectProduct=ho&Location={0}&ArrivalTime={1}&ArrivalDate={2}&DepartTime={3}&DepartDate={4}&RoomType[0]={5}&Email={6}&flight_number={7}&rule_set=teletext&LaunchID=TeletextMMBhotel", departureAirport.departureAirportCode, departureAirport.departureDateTime.ToString("HH:mm"), String.Format("{0:dd/MM/yy}", departureAirport.departureDateTime.AddDays(-1)), arrivingTime.ToString("HH:mm"), String.Format("{0:dd/MM/yy}", arrivingTime), roomType, emailAddress, departureAirport.flightNumber);
            }
            else
            {
                hotelOnly = "http://www.holidayextras.co.uk/teletextholidays/airport-hotels.html";
            }

            bookingDetails.airportName = departureAirport.departureAirportName;
            bookingDetails.airportParkingLink = parking;
            bookingDetails.airPortHotel = hotelOnly;
            bookingDetails.airportHotelAndParking = hotelwithCarParking;

            var parkingProducts = basket.Products.Where(p => p.ProductType.ToString() == "ParkingProduct");
            var parkingProduct = parkingProducts != null && parkingProducts.Count() > 0 ? parkingProducts.Where(p => p.StatusCode.ToString() == "BKG_ACCOMPLISHED") : null;

            if (parkingProduct != null)
            {
                bookingDetails.footerHotelsParkingExists = false;
                bookingDetails.footerAirportParkingExists = false;
            }
        }

        private static string GetPassengersRoomType(TopDogBasketService.Basket basket)
        {
            var numberOfAdults = basket.Passengers.Where(a => a.PersonAgeCode == "A");
            var numberOfChilds = basket.Passengers.Where(a => a.PersonAgeCode == "C");

            var numOfAdults = numberOfAdults != null || numberOfAdults.Count() > 0 ? numberOfAdults.Count().ToString() : "0";
            var numOfChilds = numberOfChilds != null || numberOfChilds.Count() > 0 ? numberOfChilds.Count().ToString() : "0";

            switch (Convert.ToInt32(numOfAdults))
            {
                case 1:
                    if (Convert.ToInt32(numOfChilds) == 0)
                    {
                        return "S10";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 1)
                    {
                        return "T11";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 2)
                    {
                        return "T12";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 3)
                    {
                        return "F13";
                    }
                    break;
                case 2:
                    if (Convert.ToInt32(numOfChilds) == 0)
                    {
                        return "T20";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 1)
                    {
                        return "T21";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 2)
                    {
                        return "F22";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 3)
                    {
                        return "F23";
                    }
                    break;
                case 3:
                    if (Convert.ToInt32(numOfChilds) == 0)
                    {
                        return "T30";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 1)
                    {
                        return "F31";
                    }
                    else if (Convert.ToInt32(numOfChilds) == 2)
                    {
                        return "F32";
                    }
                    break;
            }

            return string.Empty;
        }

        public void AddPayment(MakePayment makePayment)
        {
            TopDogBlackBox.BlackBoxPortTypeClient blackBox = new TopDogBlackBox.BlackBoxPortTypeClient();
            TopDogBasketService.BasketOperationsPortTypeClient basketClient = new TopDogBasketService.BasketOperationsPortTypeClient();
            string sessionID = string.Empty;
            bool succeeded = false;
            string topDogError = string.Empty;

            for (int index = 0; index < 3 && !succeeded; index++)
            {
                try
                {
                    TopDogBlackBox.StartSessionRequest1 startSession1 = new TopDogBlackBox.StartSessionRequest1();
                    TopDogBlackBox.StartSessionRequest startSession = new TopDogBlackBox.StartSessionRequest();
                    startSession.UserId = ConfigurationManager.AppSettings["UserId"];
                    startSession.Password = ConfigurationManager.AppSettings["Password"];
                    startSession.LanguageCode = ConfigurationManager.AppSettings["LanguageCode"];
                    startSession1.StartSessionRequest = startSession;
                    TopDogBlackBox.StartSessionResponse1 sessionResponse = blackBox.StartSession(startSession1);
                    var startSessionResponse = sessionResponse.StartSessionResponse;
                    sessionID = startSessionResponse.SessionId;

                    TopDogBasketService.GetBasketRequest1 getBasketRequest1 = new TopDogBasketService.GetBasketRequest1();
                    TopDogBasketService.GetBasketRequest getBasketRequest = new TopDogBasketService.GetBasketRequest();
                    getBasketRequest.SessionId = sessionID;
                    getBasketRequest.BookingReference = makePayment.RefNo;
                    getBasketRequest.ClientSurname = makePayment.LastName;
                    getBasketRequest.DepartDate = String.Format("{0:yyyy-MM-dd}", makePayment.DepartureDate);
                    getBasketRequest1.GetBasketRequest = getBasketRequest;
                    TopDogBasketService.GetBasketResponse response = basketClient.GetBasket(getBasketRequest);
                    var basket = response.Basket;


                    TopDogBasketService.AddPaymentRequest1 addPaymentRequest1 = new TopDogBasketService.AddPaymentRequest1();
                    TopDogBasketService.AddPaymentRequest addPaymentRequest = new TopDogBasketService.AddPaymentRequest();

                    addPaymentRequest.SessionId = sessionID;
                    addPaymentRequest.Amount = Convert.ToSingle(makePayment.Amount);
                    addPaymentRequest.PaymentType = TopDogBasketService.PaymentType.CREDIT_CARD;
                    addPaymentRequest.PaymentReference = makePayment.merchantRefNum;
                    addPaymentRequest.PaymentDate = String.Format("{0:yyyy-MM-dd HH:mm}", DateTime.Now);
                    addPaymentRequest.AmountSpecified = true;
                    addPaymentRequest.AuthorisationCode = makePayment.PaySafeVPSAuthCode;
                    addPaymentRequest.Notes = string.Empty;
                    addPaymentRequest.Manual = new TopDogBasketService.BoolWrapper();
                    addPaymentRequest.Manual.Value = true;
                    addPaymentRequest.CardDetails = new TopDogBasketService.CardDetails();
                    addPaymentRequest.CardDetails.CreditCard = new TopDogBasketService.CreditCard();
                    addPaymentRequest.CardDetails.CreditCard.CardNumber = makePayment.CardNumber;
                    addPaymentRequest.CardDetails.CreditCard.ExpireDate = String.Format("{0}{1}", makePayment.ExpiryMonth, makePayment.ExpiryYear.Substring(2, 2));
                    addPaymentRequest.CardDetails.CreditCard.NameOnCard = makePayment.NameOnCard;
                    addPaymentRequest.CardDetails.CreditCard.CardType = FindTOPDOGCardType(FindCardType(makePayment.CardNumber, makePayment.SelectedCardType));
                    addPaymentRequest.CardDetails.CreditCard.SecurityNumber = makePayment.CCVNumber;


                    addPaymentRequest1.AddPaymentRequest = addPaymentRequest;
                    TopDogBasketService.AddPaymentResponse addPaymentResponse = basketClient.AddPayment(addPaymentRequest);
                    var addPaymentResponseStatus = addPaymentResponse.Status;

                    if (addPaymentResponse.Errors == null)
                    {
                        //StartBookingProcess                        
                        TopDogBasketService.StartBookingProcessRequest1 startBookingProcessRequest1 = new TopDogBasketService.StartBookingProcessRequest1();
                        TopDogBasketService.StartBookingProcessRequest startBookingProcessRequest = new TopDogBasketService.StartBookingProcessRequest();
                        startBookingProcessRequest.SessionId = sessionID;
                        startBookingProcessRequest1.StartBookingProcessRequest = startBookingProcessRequest;
                        TopDogBasketService.StartBookingProcessResponse startBookingProcessResponse = basketClient.StartBookingProcess(startBookingProcessRequest);

                        //ContinueBookingProcess
                        string continueBookingProcessedMessage = string.Empty;
                        TopDogBasketService.ContinueBookingProcessRequest1 continueBookingProcessRequest1 = new TopDogBasketService.ContinueBookingProcessRequest1();
                        TopDogBasketService.ContinueBookingProcessRequest continueBookingProcessRequest = new TopDogBasketService.ContinueBookingProcessRequest();
                        continueBookingProcessRequest.SessionId = sessionID;
                        continueBookingProcessRequest1.ContinueBookingProcessRequest = continueBookingProcessRequest;

                        for (int index1 = 0; !continueBookingProcessedMessage.Contains("All operations are completed") && index1 < 60; index1++)
                        {
                            TopDogBasketService.ContinueBookingProcessResponse continueBookingProcessResponse = basketClient.ContinueBookingProcess(continueBookingProcessRequest);
                            continueBookingProcessedMessage = continueBookingProcessResponse.BookingProcessStatus.StatusMessage;
                            System.Threading.Thread.Sleep(1000);
                        }

                        if (!continueBookingProcessedMessage.Contains("All operations are completed"))
                        {
                            topDogError = continueBookingProcessedMessage;
                        }

                        //FinishBookingProcess                        
                        string finishBookingProcessedMessage = string.Empty;
                        TopDogBasketService.FinishBookingProcessRequest1 finishBookingProcessRequest1 = new TopDogBasketService.FinishBookingProcessRequest1();
                        TopDogBasketService.FinishBookingProcessRequest finishBookingProcessRequest = new TopDogBasketService.FinishBookingProcessRequest();
                        finishBookingProcessRequest.SessionId = sessionID;
                        finishBookingProcessRequest1.FinishBookingProcessRequest = finishBookingProcessRequest;

                        TopDogBasketService.FinishBookingProcessResponse finishBookingProcessResponse = null;
                        finishBookingProcessResponse = basketClient.FinishBookingProcess(finishBookingProcessRequest);
                        finishBookingProcessedMessage = finishBookingProcessResponse.BookingProcessStatus.StatusMessage;

                        if (finishBookingProcessedMessage.Contains("Booking process is finished"))
                        {
                            succeeded = true;
                            topDogError = "Successfully Posted";
                        }
                        else
                        {
                            topDogError = finishBookingProcessedMessage;
                        }
                    }
                    else
                    {
                        topDogError = addPaymentResponse.Errors != null && addPaymentResponse.Errors.Count() > 0 ? string.Join(",", addPaymentResponse.Errors) : "Failed Posting to TopDog";
                    }

                    index++;
                }
                catch (Exception ex)
                {
                    topDogError = ex.InnerException != null ? ex.InnerException.ToString() : ex.Message;
                }
                finally
                {
                    if (!string.IsNullOrEmpty(sessionID))
                    {
                        TopDogBlackBox.EndSessionRequest1 endSessionRequest1 = new TopDogBlackBox.EndSessionRequest1();
                        TopDogBlackBox.EndSessionRequest endSessionRequest = new TopDogBlackBox.EndSessionRequest();
                        endSessionRequest.SessionId = sessionID;
                        endSessionRequest1.EndSessionRequest = endSessionRequest;
                        var endSessionResponse = blackBox.EndSession(endSessionRequest1);
                        var sessionIDEND = endSessionResponse.EndSessionResponse.SessionId;
                    }
                }
            }

            MakePayment objpay = GetPaymentDetails(makePayment.merchantRefNum);

            if (!succeeded)
            {
                var emailBody = string.Format("Lead Booker: {0} <br><br> Booking Ref: {1} <br><br> Departure Date: {2} <br><br> Amount: £{3} <br><br>SagePay Authorization Code: {4}<br><br>SagePay VPS authcode: {5}<br><br>Last 4 digits Card Number: {6}", makePayment.LastName, makePayment.RefNo, String.Format("{0:yyyy-MM-dd}", makePayment.DepartureDate), makePayment.Amount, makePayment.PaysafeAuthorisationCode, makePayment.PaySafeVPSAuthCode, makePayment.CardNumber.Substring(makePayment.CardNumber.Length - 4, 4));
                SendTopDogPaymentFailedEmail(ConfigurationManager.AppSettings["TopDogFailedContactEmail"].ToString(), "Payment posting to TopDog is Failed: " + makePayment.RefNo, emailBody);
                objpay.IsTopDogPostingSuccess = false;
            }
            else
            {
                objpay.IsTopDogPostingSuccess = true;
            }

            objpay.TopDogError = topDogError;
            MakePaymentProcess(objpay);
        }

        public static void SendTopDogPaymentFailedEmail(String EmailTo, String Subject, string mailbodyextr)
        {
            bool mailSend = Utilities.SendEmail(Subject, mailbodyextr, EmailTo);
        }

        public static MakePayment GetPaymentDetails(string transactionId)
        {
            MakePayment makePayment = null;

            List<MySqlParameter> procParams = new List<MySqlParameter>();

            MySqlParameter PageVersionpara = new MySqlParameter("@TransID", MySqlDbType.VarChar);
            PageVersionpara.Size = 50;
            PageVersionpara.Value = transactionId;
            procParams.Add(PageVersionpara);

            DataSet ds = ExecuteQuery("iPro_sp_GetPaymentDetails", procParams);
            DataRowCollection rows = ds.Tables[0].Rows;

            if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
            {
                makePayment = new MakePayment();
                makePayment.PaymentId = (int)ds.Tables[0].Rows[0]["PaymentId"];
                makePayment.RefNo = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["RefNo"]) ? (string)ds.Tables[0].Rows[0]["RefNo"] : string.Empty;
                makePayment.FirstName = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["FirstName"]) ? (string)ds.Tables[0].Rows[0]["FirstName"] : string.Empty;
                makePayment.LastName = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["LastName"]) ? (string)ds.Tables[0].Rows[0]["LastName"] : string.Empty;
                makePayment.Email = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["Email"]) ? (string)ds.Tables[0].Rows[0]["Email"] : string.Empty;
                makePayment.Amount = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["Amount"]) ? (decimal)ds.Tables[0].Rows[0]["Amount"] : 0;
                makePayment.TotalDueAmount = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["TotalDueAmount"]) ? (decimal)ds.Tables[0].Rows[0]["TotalDueAmount"] : 0;
                makePayment.IsSuccessful = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["IsSuccessful"]) ? (bool)ds.Tables[0].Rows[0]["IsSuccessful"] : false;
                makePayment.PaymentDate = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["PaymentDate"]) ? String.Format("{0:yyyy-MM-dd HH:mm}", Convert.ToDateTime(ds.Tables[0].Rows[0]["PaymentDate"])) : System.DateTime.Now.ToShortDateString();
                makePayment.DepartureDate = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["DepartureDate"]) ? Convert.ToDateTime(ds.Tables[0].Rows[0]["DepartureDate"]).ToShortDateString() : System.DateTime.Now.ToShortDateString();
                makePayment.CardCharges = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["CardCharges"]) ? (decimal)ds.Tables[0].Rows[0]["CardCharges"] : 0;
                makePayment.PaymentDueDate = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["PaymentDueDate"]) ? Convert.ToDateTime(ds.Tables[0].Rows[0]["PaymentDueDate"]).ToShortDateString() : System.DateTime.Now.ToShortDateString();
                makePayment.merchantRefNum = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["TransactionID"]) ? (string)ds.Tables[0].Rows[0]["TransactionID"] : string.Empty;
                makePayment.PaysafeAuthorisationCode = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["SagePayUniqueID"]) ? (string)ds.Tables[0].Rows[0]["SagePayUniqueID"] : string.Empty;
                makePayment.paymentError = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["paymentError"]) ? (string)ds.Tables[0].Rows[0]["paymentError"] : string.Empty;
                makePayment.TopDogError = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["TopDogError"]) ? (string)ds.Tables[0].Rows[0]["TopDogError"] : string.Empty;
                makePayment.IsTopDogPostingSuccess = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["IsTopDogPostingSuccess"]) ? (bool)ds.Tables[0].Rows[0]["IsTopDogPostingSuccess"] : false;
                makePayment.CardId = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["CardId"]) ? (int)ds.Tables[0].Rows[0]["CardId"] : 0;
                makePayment.Platform = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["Platform"]) ? (string)ds.Tables[0].Rows[0]["Platform"] : string.Empty;
                makePayment.Source = !DBNull.Value.Equals(ds.Tables[0].Rows[0]["Src"]) ? (string)ds.Tables[0].Rows[0]["Src"] : string.Empty;
            }

            return makePayment;
        }

        static DataSet ExecuteQuery(string procName, List<MySqlParameter> procParams = null)
        {
            DataSet ds = null;
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ttWeb"].ToString()))
            {
                con.Open();

                MySqlCommand com = new MySqlCommand();
                com.Connection = con;

                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                if (procParams != null && procParams.Count != 0)
                {
                    foreach (MySqlParameter param in procParams)
                    {
                        com.Parameters.Add(param);
                    }
                }

                MySqlDataAdapter adap = new MySqlDataAdapter(com);
                ds = new DataSet();

                try
                {
                    adap.Fill(ds);
                }
                catch (Exception ex)
                {
                    con.Close();

                    throw ex;
                }

                con.Close();
            }
            return ds;
        }

        public static void ExecuteCommand(string procName, List<MySqlParameter> procParams = null)
        {
            MySqlTransaction sqlTrans = null;
            using (MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["ttWeb"].ToString()))
            {
                con.Open();

                sqlTrans = con.BeginTransaction();

                MySqlCommand com = new MySqlCommand();

                com.Connection = con;
                com.Transaction = sqlTrans;
                com.CommandType = CommandType.StoredProcedure;
                com.CommandText = procName;

                if (procParams != null && procParams.Count != 0)
                {
                    foreach (MySqlParameter param in procParams)
                    {
                        com.Parameters.Add(param);
                    }
                }

                try
                {
                    com.ExecuteNonQuery();
                    sqlTrans.Commit();
                }
                catch (Exception ex)
                {
                    sqlTrans.Rollback();
                    con.Close();
                    throw ex;
                }

                con.Close();
            }
        }

        public static MakePayment MakePaymentProcess(MakePayment makePayment)
        {
            List<MySqlParameter> procParams = new List<MySqlParameter>();
            MySqlParameter param;
            MySqlParameter paramResult;

            param = new MySqlParameter("@PaymentUniquerId", MySqlDbType.Int32);
            param.Value = makePayment.PaymentId;
            procParams.Add(param);

            param = new MySqlParameter("@RefNo", MySqlDbType.VarChar);
            param.Size = 100;
            param.Value = makePayment.RefNo;
            procParams.Add(param);

            param = new MySqlParameter("@FirstName", MySqlDbType.VarChar);
            param.Size = 100;
            param.Value = makePayment.FirstName;
            procParams.Add(param);

            param = new MySqlParameter("@LastName", MySqlDbType.VarChar);
            param.Size = 100;
            param.Value = makePayment.LastName;
            procParams.Add(param);

            param = new MySqlParameter("@Email", MySqlDbType.VarChar);
            param.Size = 200;
            param.Value = makePayment.Email;
            procParams.Add(param);

            param = new MySqlParameter("@Amount", MySqlDbType.Decimal);
            param.Value = makePayment.Amount;
            procParams.Add(param);

            param = new MySqlParameter("@TotalDueAmount", MySqlDbType.Decimal);
            param.Value = makePayment.TotalDueAmount;
            procParams.Add(param);

            param = new MySqlParameter("@CardCharges", MySqlDbType.Decimal);
            param.Value = makePayment.CardCharges;
            procParams.Add(param);

            param = new MySqlParameter("@IsSuccessful", MySqlDbType.Bit);
            param.Value = makePayment.IsSuccessful;
            procParams.Add(param);

            param = new MySqlParameter("@PaymentDate", MySqlDbType.DateTime);
            param.Value = Convert.ToDateTime(makePayment.PaymentDate);
            procParams.Add(param);

            param = new MySqlParameter("@DepartureDate", MySqlDbType.DateTime);
            param.Value = makePayment.DepartureDate;
            procParams.Add(param);

            param = new MySqlParameter("@PaymentDueDate", MySqlDbType.DateTime);
            param.Value = makePayment.PaymentDueDate;
            procParams.Add(param);

            param = new MySqlParameter("@TransactionID", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = makePayment.merchantRefNum;
            procParams.Add(param);

            param = new MySqlParameter("@SagePayUniqueID", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = makePayment.PaysafeAuthorisationCode;
            procParams.Add(param);

            param = new MySqlParameter("@paymentError", MySqlDbType.VarChar);
            param.Size = 10000;
            param.Value = makePayment.paymentError;
            procParams.Add(param);

            param = new MySqlParameter("@TopDogError", MySqlDbType.VarChar);
            param.Size = 10000;
            param.Value = makePayment.TopDogError;
            procParams.Add(param);

            param = new MySqlParameter("@Src", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = makePayment.Source;
            procParams.Add(param);

            param = new MySqlParameter("@Platform", MySqlDbType.VarChar);
            param.Size = 50;
            param.Value = makePayment.Platform;
            procParams.Add(param);

            param = new MySqlParameter("@IsTopDogPostingSuccess", MySqlDbType.Bit);
            param.Value = makePayment.IsTopDogPostingSuccess;
            procParams.Add(param);

            param = new MySqlParameter("@CardID", MySqlDbType.Int32);
            param.Value = makePayment.CardId;
            procParams.Add(param);

            // @RESULT

            paramResult = new MySqlParameter("@PaymentIdOut", MySqlDbType.Int32);
            paramResult.Direction = ParameterDirection.Output;

            procParams.Add(paramResult);

            try
            {
                ExecuteCommand("iPro_spp_MakePaymentProcess", ConfigurationManager.ConnectionStrings["ttWeb"].ConnectionString, procParams);

                if (int.Parse(paramResult.Value.ToString()) > 0)
                {
                    makePayment.PaymentId = int.Parse(paramResult.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return makePayment;
        }

        public static void ExecuteCommand(string procName, string ConnectionString, List<MySqlParameter> procParams = null)
        {
            MySqlConnection con = new MySqlConnection(ConnectionString);

            con.Open();

            MySqlCommand com = new MySqlCommand();

            com.Connection = con;
            com.CommandType = CommandType.StoredProcedure;
            com.CommandText = procName;

            if (procParams != null && procParams.Count != 0)
            {
                foreach (MySqlParameter param in procParams)
                {
                    com.Parameters.Add(param);
                }
            }

            try
            {
                com.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                con.Close();

                throw ex;
            }

            con.Close();
        }

        //public static MakePayment MakePaymentProcess(MakePayment makePayment)
        //{
        //    DataTable makePaymentDataTable = new DataTable();
        //    makePaymentDataTable.Columns.AddRange(new DataColumn[18] {
        //                                new DataColumn("PaymentId", typeof(int)),
        //                                new DataColumn("RefNo", typeof(string)),
        //                                new DataColumn("FirstName", typeof(string)),
        //                                new DataColumn("LastName", typeof(string)),
        //                                new DataColumn("Email", typeof(string)),
        //                                new DataColumn("CardCharges", typeof(decimal)),
        //                                new DataColumn("Amount", typeof(decimal)),
        //                                new DataColumn("TotalDueAmount", typeof(decimal)),
        //                                new DataColumn("IsSuccessful", typeof(byte)),
        //                                new DataColumn("PaymentDate", typeof(DateTime)),
        //                                new DataColumn("DepartureDate", typeof(DateTime)),
        //                                new DataColumn("PaymentDueDate", typeof(DateTime)),
        //                                new DataColumn("TransactionID", typeof(string)),
        //                                new DataColumn("SagePayUniqueID", typeof(string)),
        //                                new DataColumn("paymentError", typeof(string)),
        //                                new DataColumn("TopDogError", typeof(string)),
        //                            new DataColumn("IsTopDogPostingSuccess", typeof(bool)),
        //                              new DataColumn("CardID", typeof(int))
        //        });

        //    makePaymentDataTable.Rows.Add(
        //                            makePayment.PaymentId,
        //                            makePayment.RefNo,
        //                            makePayment.FirstName,
        //                            makePayment.LastName,
        //                            makePayment.Email,
        //                            makePayment.CardCharges,
        //                            makePayment.Amount,
        //                            makePayment.TotalDueAmount,
        //                            makePayment.IsSuccessful,
        //                            makePayment.PaymentDate,
        //                            makePayment.DepartureDate,
        //                            makePayment.PaymentDueDate,
        //                            makePayment.TransactionID,
        //                            makePayment.PaysafeAuthorisationCode,
        //                            makePayment.paymentError,
        //                            makePayment.TopDogError,
        //                            makePayment.IsTopDogPostingSuccess,
        //                            makePayment.CardId
        //                            );

        //    List<MySqlParameter> procParams = new List<MySqlParameter>();

        //    MySqlParameter param = new MySqlParameter("@MakePaymentType", SqlDbType.Structured);
        //    param.Value = makePaymentDataTable;
        //    procParams.Add(param);

        //    param = new MySqlParameter("@PaymentId", SqlDbType.Int);
        //    param.Direction = ParameterDirection.Output;
        //    procParams.Add(param);

        //    ExecuteCommand("iPro_spp_MakePaymentProcess", procParams);
        //    if (makePayment.PaymentId == 0)
        //    {
        //        makePayment.PaymentId = (int)param.Value;
        //    }
        //    return makePayment;
        //}

        public static bool SendTakePaymentConfirmationEmail(String EmailTo, String Subject, string transactionid, MakePayment paymentDetails)
        {
            String mailbodyextr = GenerateHtmlFromUrl(HttpRuntime.AppDomainAppPath + @"data\PaymentPage.html", paymentDetails);
            decimal cardCharge = 0;
            bool isPercentType = false;
            GetCardPercentage(paymentDetails.CardId, ref cardCharge, ref isPercentType);
            string finalAmount = string.Empty;
            if (isPercentType)
            {
                finalAmount = Math.Round((Convert.ToDecimal(paymentDetails.Amount) + Convert.ToDecimal(paymentDetails.Amount) * Convert.ToDecimal(paymentDetails.CardCharges / 100)), 2).ToString("0.00");
            }
            else
            {
                finalAmount = Math.Round((Convert.ToDecimal(paymentDetails.Amount) + Convert.ToDecimal(paymentDetails.CardCharges)), 2).ToString("0.00");
            }
            var custName = paymentDetails.FirstName + " " + paymentDetails.LastName;
            var bookingDate = Convert.ToDateTime(paymentDetails.PaymentDate).ToString("dd MMMM yyyy");
            mailbodyextr = mailbodyextr.Replace("{ReferenceNumber}", paymentDetails.RefNo).Replace("{Amount}", finalAmount).Replace("{EmailAddress}", paymentDetails.Email).Replace("{CustomerName}", custName).Replace("{PaymentDate}", bookingDate);
            bool mailSend = Utilities.SendEmail(Subject, mailbodyextr, EmailTo);
            return mailSend;
        }

        public static String GenerateHtmlFromUrl(String path, MakePayment paymentDetails)
        {
            string paymentConfirmationEmail = System.IO.File.ReadAllText(path);
            ListDictionary replacements = new ListDictionary();
            replacements.Add("{PaymentDate}", Convert.ToDateTime(paymentDetails.PaymentDate).ToString("dddd dd MMMM yyyy"));
            replacements.Add("{CustomerName}", paymentDetails.FirstName + " " + paymentDetails.LastName);
            replacements.Add("{ReferenceNumber}", paymentDetails.RefNo);
            replacements.Add("{EmailAddress}", paymentDetails.Email);

            decimal cardCharge = 0;
            bool isPercentType = false;
            string amount = string.Empty;
            GetCardPercentage(paymentDetails.CardId, ref cardCharge, ref isPercentType);
            if (isPercentType)
            {
                amount = Math.Round((Convert.ToDecimal(paymentDetails.Amount) + Convert.ToDecimal(paymentDetails.Amount) * Convert.ToDecimal(paymentDetails.CardCharges / 100)), 2).ToString("0.00");
            }
            else
            {
                amount = Math.Round((Convert.ToDecimal(paymentDetails.Amount) + Convert.ToDecimal(paymentDetails.CardCharges)), 2).ToString("0.00");
            }
            replacements.Add("{Amount}", amount);
            return Convert.ToString(paymentConfirmationEmail);
        }

        public static void GetCardPercentage(int cardId, ref decimal cardCharge, ref bool isPercentType)
        {
            var cardDetails = GetCreditCardCharges();
            var cardCharges = cardDetails.Where(p => p.CardID == cardId);
            if (cardCharges != null && cardCharges.Count() > 0)
            {
                cardCharge = cardCharges.FirstOrDefault().CardCharges;
                isPercentType = cardCharges.FirstOrDefault().IsPercentType;
            }
        }

        public static List<CardCharge> GetCreditCardCharges()
        {
            string key = "PaymentCardCharges";
            List<CardCharge> cardCharges = null;

            if (HttpRuntime.Cache.Get(key) != null)
            {
                cardCharges = (List<CardCharge>)HttpRuntime.Cache.Get(key);
            }
            else
            {
                cardCharges = new List<CardCharge>();
                CardCharge cardCharge = null;

                List<SqlParameter> procParams = new List<SqlParameter>();

                DataSet ds = ExecuteQuery("iPro_sp_GetCardCharges");
                DataRowCollection rows = ds.Tables[0].Rows;

                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int index = ds.Tables[0].Rows.Count - 1; index >= 0; index--)
                    {
                        cardCharge = new CardCharge();
                        cardCharge.CardID = (int)ds.Tables[0].Rows[index]["CardID"];
                        cardCharge.CardType = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["CardType"]) ? (string)ds.Tables[0].Rows[index]["CardType"] : string.Empty;
                        cardCharge.MultiCom = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["MultiCom"]) ? (string)ds.Tables[0].Rows[index]["MultiCom"] : string.Empty;
                        cardCharge.ThreeDSecapy = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["ThreeDSecapy"]) ? (string)ds.Tables[0].Rows[index]["ThreeDSecapy"] : string.Empty;
                        cardCharge.Secpay = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["Secpay"]) ? (string)ds.Tables[0].Rows[index]["Secpay"] : string.Empty;
                        cardCharge.CardCharges = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["CardCharges"]) ? (decimal)ds.Tables[0].Rows[index]["CardCharges"] : 0;
                        cardCharge.IsPercentType = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["IsPercentType"]) ? (bool)ds.Tables[0].Rows[index]["IsPercentType"] : false;
                        cardCharge.SagePay = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["SagePay"]) ? (string)ds.Tables[0].Rows[index]["SagePay"] : string.Empty;
                        cardCharges.Add(cardCharge);
                    }
                }

                HttpRuntime.Cache.Insert(key, cardCharges);
            }

            return cardCharges;
        }

        public static List<CardCharge> GetCreditCardChargesTesting()
        {
            string key = "PaymentCardChargesTesting";
            List<CardCharge> cardCharges = null;

            if (HttpRuntime.Cache.Get(key) != null)
            {
                cardCharges = (List<CardCharge>)HttpRuntime.Cache.Get(key);
            }
            else
            {
                cardCharges = new List<CardCharge>();
                CardCharge cardCharge = null;

                List<SqlParameter> procParams = new List<SqlParameter>();

                DataSet ds = ExecuteQuery("iPro_sp_GetCardChargesTesting");
                DataRowCollection rows = ds.Tables[0].Rows;

                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {
                    for (int index = ds.Tables[0].Rows.Count - 1; index >= 0; index--)
                    {
                        cardCharge = new CardCharge();
                        cardCharge.CardID = (int)ds.Tables[0].Rows[index]["CardID"];
                        cardCharge.CardType = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["CardType"]) ? (string)ds.Tables[0].Rows[index]["CardType"] : string.Empty;
                        cardCharge.MultiCom = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["MultiCom"]) ? (string)ds.Tables[0].Rows[index]["MultiCom"] : string.Empty;
                        cardCharge.ThreeDSecapy = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["ThreeDSecapy"]) ? (string)ds.Tables[0].Rows[index]["ThreeDSecapy"] : string.Empty;
                        cardCharge.Secpay = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["Secpay"]) ? (string)ds.Tables[0].Rows[index]["Secpay"] : string.Empty;
                        cardCharge.CardCharges = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["CardCharges"]) ? (decimal)ds.Tables[0].Rows[index]["CardCharges"] : 0;
                        cardCharge.IsPercentType = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["IsPercentType"]) ? (bool)ds.Tables[0].Rows[index]["IsPercentType"] : false;
                        cardCharge.SagePay = !DBNull.Value.Equals(ds.Tables[0].Rows[index]["SagePay"]) ? (string)ds.Tables[0].Rows[index]["SagePay"] : string.Empty;
                        cardCharges.Add(cardCharge);
                    }
                }

                HttpRuntime.Cache.Insert(key, cardCharges);
            }

            return cardCharges;
        }

        public static decimal GetFinalAmount(string cardType, string amount, string cardNum)
        {
            decimal totalCharge = Convert.ToDecimal(amount);
            decimal cardCharge = 0;
            bool isPercentType = false;
            GetCardPercentage(cardType, cardNum, ref cardCharge, ref isPercentType);
            if (isPercentType)
            {
                totalCharge = Convert.ToDecimal(amount) + Convert.ToDecimal(amount) * Convert.ToDecimal(cardCharge / 100);
            }
            else
            {
                totalCharge = Convert.ToDecimal(amount) + cardCharge;
            }

            return Math.Round(totalCharge, 2, MidpointRounding.AwayFromZero);
        }

        public static decimal GetFinalAmount(string cardType, string amount)
        {
            decimal totalCharge = Convert.ToDecimal(amount);
            decimal cardCharge = 0;
            bool isPercentType = false;
            GetCardPercentage(cardType, ref cardCharge, ref isPercentType);
            if (isPercentType)
            {
                totalCharge = Convert.ToDecimal(amount) + Convert.ToDecimal(amount) * Convert.ToDecimal(cardCharge / 100);
            }
            else
            {
                totalCharge = Convert.ToDecimal(amount) + cardCharge;
            }

            return Math.Round(totalCharge, 2, MidpointRounding.AwayFromZero);
        }

        public static void GetCardPercentage(string cardType, string cardNum, ref decimal cardCharge, ref bool isPercentType)
        {
            var cardDetails = GetCreditCardCharges();
            var typeOfcard = FindCardType(cardNum, cardType);
            var cardCharges = cardDetails.Where(p => p.CardType == typeOfcard);
            if (cardCharges != null && cardCharges.Count() > 0)
            {
                cardCharge = cardCharges.FirstOrDefault().CardCharges;
                isPercentType = cardCharges.FirstOrDefault().IsPercentType;
            }
        }

        public static void GetCardPercentage(string cardType, ref decimal cardCharge, ref bool isPercentType)
        {
            var cardDetails = GetCreditCardCharges();
            var cardCharges = cardDetails.Where(p => p.SagePay == cardType);
            if (cardCharges != null && cardCharges.Count() > 0)
            {
                cardCharge = cardCharges.FirstOrDefault().CardCharges;
                isPercentType = cardCharges.FirstOrDefault().IsPercentType;
            }
        }

        public static int GetCardId(string cardType)
        {
            var cardDetails = TopDogManager.GetCreditCardCharges();
            var cardCharges = cardDetails.Where(p => p.CardType == cardType);
            int creditCardId = 0;
            if (cardCharges != null && cardCharges.Count() > 0)
            {
                creditCardId = cardCharges.FirstOrDefault().CardID;
            }

            return creditCardId;
        }

        public static string FindCardType(string cardNumber, string cardType)
        {
            var cardCharges = GetCreditCardCharges();
            var cardCharge = cardCharges.Where(p => p.SagePay == cardType);
            if (cardCharge != null && cardCharge.Count() > 0)
            {
                return cardCharge.FirstOrDefault().CardType;
            }
            else
            {
                if (cardNumber.StartsWith("34") || cardNumber.StartsWith("37"))
                {
                    return "American Express";
                }
                else if (cardNumber.StartsWith("5018") || cardNumber.StartsWith("5020") || cardNumber.StartsWith("5038") || cardNumber.StartsWith("5893") || cardNumber.StartsWith("6304") || cardNumber.StartsWith("6759") || cardNumber.StartsWith("6761") || cardNumber.StartsWith("6762") || cardNumber.StartsWith("6763"))
                {
                    return "Maestro";
                }
                else if (cardNumber.StartsWith("51") || cardNumber.StartsWith("52") || cardNumber.StartsWith("53") || cardNumber.StartsWith("54") || cardNumber.StartsWith("55"))
                {
                    if (cardType == "Credit")
                    {
                        return "Master Card";
                    }
                    else
                    {
                        return "Master Card Debit";
                    }
                }
                else if (cardNumber.StartsWith("4026") || cardNumber.StartsWith("417500") || cardNumber.StartsWith("4508") || cardNumber.StartsWith("4844") || cardNumber.StartsWith("4913") || cardNumber.StartsWith("4917"))
                {
                    return "Visa Electron";
                }
                else if (cardNumber.StartsWith("4"))
                {
                    if (cardType == "Credit")
                    {
                        return "Visa Credit";
                    }
                    else
                    {
                        return "Visa Debit";
                    }

                }
            }

            return string.Empty;
        }

        private static string FindTOPDOGCardType(string cardName)
        {
            switch (cardName)
            {
                case "American Express": return "AM";
                case "Maestro": return "SWI";
                case "Master Card":
                    return "MC";
                case "Master Card Debit":
                    return "MCD";
                case "Visa Electron": return "EL";
                case "Visa Credit": return "VIS";
                case "Visa Debit": return "VISD";
            }

            return string.Empty;
        }
    }
}
