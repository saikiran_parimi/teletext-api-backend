﻿using System;
using System.Collections.Generic;
using System.Linq;
using Presentation.WCFRestService.Model.Reviews;
using System.Configuration;
using System.IO;
using Newtonsoft.Json;
using Amazon.S3;
using Amazon.S3.Model;
using System.Text;
using Presentation.WCFRestService.Model.Enum;
using System.ServiceModel.Web;

namespace Presentation.Web.WCFRestServices.Reviews
{
    public class ReviewController
    {
        public static ResponseSummary UpdateTrustPilotReviewObject(ReviewInformation reviewInfo, List<TrustPilotReviews> trustPilotReviewsList)
        {
            ResponseSummary apiResponse = new ResponseSummary();
            try
            {
                string awsBucketName = "";
                string awsPublishedReviewsInputFolderKeyName = "";
                string awsUnPublishedReviewsInputFolderKeyName = "";
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSSecretAccessKeyId"];
                var reviewsList = "";
                IAmazonS3 client;

                // Whether review should be published or unpublished
                // toPublish = true, if review has to be published, else false
                bool toPublish = !(reviewInfo.isPublished);

                #region TrustPilotReviews
                if (reviewInfo.reviewProviderName == "TrustPilot")
                {
                    List<TrustPilotReviews> trustPilotReviews = new List<TrustPilotReviews>();
                    TrustPilotReviews deletedTrustPilotReview = null;

                    awsBucketName = ConfigurationManager.AppSettings["TrustPilotReviewsBucket"];
                    awsPublishedReviewsInputFolderKeyName = ConfigurationManager.AppSettings["TrustPilotPublishedReviewsJSON"];
                    awsUnPublishedReviewsInputFolderKeyName = ConfigurationManager.AppSettings["TrustPilotUnPublishedReviewsJSON"];

                    // Unpublish the review
                    if (!toPublish)
                    {
                        foreach (TrustPilotReviews review in trustPilotReviewsList)
                        {
                            if (review.id == reviewInfo.reviewId)
                            {
                                deletedTrustPilotReview = review;
                                break;
                            }
                        }
                        trustPilotReviewsList.RemoveAll(review => review.id == reviewInfo.reviewId);

                        // Remove the review and Update the Published Trust Pilot Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsPublishedReviewsInputFolderKeyName
                            };
                            request.ContentType = "application/json";
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(trustPilotReviewsList)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                        }

                        // Fetch the Unpublished Feefo Reviews
                        reviewsList = "";

                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsUnPublishedReviewsInputFolderKeyName)))
                        {
                            reviewsList = reader.ReadToEnd();
                        }
                        trustPilotReviews = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(reviewsList);

                        deletedTrustPilotReview.isPublished = false;

                        trustPilotReviews.Add(deletedTrustPilotReview);

                        // Update the UnPublished Trust Pilot Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsUnPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(trustPilotReviews)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                            else
                            {
                                apiResponse.errorMessage = "Success";
                                return apiResponse;
                            }
                        }
                    }
                    // Publish the review
                    else
                    {
                        foreach (TrustPilotReviews review in trustPilotReviewsList)
                        {
                            if (review.id == reviewInfo.reviewId)
                            {
                                deletedTrustPilotReview = review;
                                break;
                            }
                        }
                        trustPilotReviewsList.RemoveAll(review => review.id == reviewInfo.reviewId);

                        // Remove the review and Update the UnPublished Trust Pilot Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsUnPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(trustPilotReviewsList)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                        }

                        // Fetch the Published Trust Pilot Reviews
                        reviewsList = "";

                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsPublishedReviewsInputFolderKeyName)))
                        {
                            reviewsList = reader.ReadToEnd();
                        }
                        trustPilotReviews = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(reviewsList);

                        deletedTrustPilotReview.isPublished = true;

                        trustPilotReviews.Add(deletedTrustPilotReview);

                        // Update the Published Trust Pilot Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(trustPilotReviews)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                            else
                            {
                                apiResponse.errorMessage = "Success";
                                apiResponse.errorCode = 0;
                                return apiResponse;
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Object Not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                apiResponse.errorMessage = "Error updating the review status";
                apiResponse.errorCode = -1;
                return apiResponse;
            }
            return apiResponse;
        }

        public static ResponseSummary UpdateFeefoReviewObject(ReviewInformation reviewInfo, List<FeefoReviews> feefoReviewsList)
        {
            ResponseSummary apiResponse = new ResponseSummary();
            try
            {
                string awsBucketName = "";
                string awsPublishedReviewsInputFolderKeyName = "";
                string awsUnPublishedReviewsInputFolderKeyName = "";
                string awsAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSAcessKeyId"];
                string awsSceretAccessKeyId = ConfigurationManager.AppSettings["AWSReviewsAWSSecretAccessKeyId"];
                var reviewsList = "";
                IAmazonS3 client;

                // Whether review should be published or unpublished
                // toPublish = true, if review has to be published, else false
                bool toPublish = !(reviewInfo.isPublished);

                #region FeefoReviews
                if (reviewInfo.reviewProviderName == "Feefo")
                {
                    List<FeefoReviews> feefoReviews = null;
                    FeefoReviews deletedFeefoReview = null;

                    awsBucketName = ConfigurationManager.AppSettings["FeefoReviewsBucket"];
                    awsPublishedReviewsInputFolderKeyName = ConfigurationManager.AppSettings["FeefoPublishedReviewsJSON"];
                    awsUnPublishedReviewsInputFolderKeyName = ConfigurationManager.AppSettings["FeefoUnPublishedReviewsJSON"];

                    // Unpublish the review
                    if (!toPublish)
                    {
                        foreach (FeefoReviews review in feefoReviewsList)
                        {
                            if (review.service.id == reviewInfo.reviewId)
                            {
                                deletedFeefoReview = review;
                                break;
                            }
                        }
                        feefoReviewsList.RemoveAll(review => review.service.id == reviewInfo.reviewId);

                        // Remove the review and Update the Published Feefo Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(feefoReviewsList)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                        }

                        // Fetch the Unpublished Feefo Reviews
                        reviewsList = "";

                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsUnPublishedReviewsInputFolderKeyName)))
                        {
                            reviewsList = reader.ReadToEnd();
                        }
                        feefoReviews = JsonConvert.DeserializeObject<List<FeefoReviews>>(reviewsList);

                        deletedFeefoReview.isPublished = false;
                        feefoReviews.Add(deletedFeefoReview);

                        // Update the UnPublished Feefo Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsUnPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(feefoReviews)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                            else
                            {
                                apiResponse.errorMessage = "Success";
                                apiResponse.errorCode = 0;
                                return apiResponse;
                            }
                        }
                    }
                    // Publish the review
                    else
                    {
                        foreach (FeefoReviews review in feefoReviewsList)
                        {
                            if (review.service.id == reviewInfo.reviewId)
                            {
                                deletedFeefoReview = review;
                                break;
                            }
                        }
                        feefoReviewsList.RemoveAll(review => review.service.id == reviewInfo.reviewId);

                        // Remove the review and Update the UnPublished Feefo Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsUnPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(feefoReviewsList)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                        }

                        // Fetch the Published Feefo Reviews
                        reviewsList = "";

                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsPublishedReviewsInputFolderKeyName)))
                        {
                            reviewsList = reader.ReadToEnd();
                        }
                        feefoReviews = JsonConvert.DeserializeObject<List<FeefoReviews>>(reviewsList);

                        deletedFeefoReview.isPublished = true;

                        feefoReviews.Add(deletedFeefoReview);

                        // Update the Published Feefo Reviews
                        using (client = new AmazonS3Client(awsAccessKeyId, awsSceretAccessKeyId, Amazon.RegionEndpoint.EUWest1))
                        {
                            Amazon.S3.Model.PutObjectRequest request = new PutObjectRequest()
                            {
                                BucketName = awsBucketName,
                                Key = awsPublishedReviewsInputFolderKeyName
                            };
                            request.InputStream = new MemoryStream(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(feefoReviews)));
                            PutObjectResponse response = client.PutObject(request);
                            if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                            {
                                apiResponse.errorMessage = "Error updating the review status";
                                apiResponse.errorCode = -1;
                                return apiResponse;
                            }
                            else
                            {
                                apiResponse.errorMessage = "Success";
                                apiResponse.errorCode = 0;
                                return apiResponse;
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Object Not found:: " + ex.Message + ". " + ex.StackTrace, LogLevel.Error);
                apiResponse.errorMessage = "Error updating the review status";
                apiResponse.errorCode = -1;
                return apiResponse;
            }
            return apiResponse;
        }
        
        public static ResponseSummary UnpublishReview(ReviewInformation newReview)
        {
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            ReviewInformation reviewInformation;
            CustomerInfo customer;
            ResponseSummary apiResponse = new ResponseSummary();
            bool reviewFound = false;

            try
            {
                string reviewInfo = string.Empty;
                string publishedReviews = string.Empty;
                if (newReview.reviewProviderName.ToLower() == "feefo")
                {
                    awsBucketName = ConfigurationManager.AppSettings["FeefoReviewsBucket"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["FeefoPublishedReviewsJSON"];

                    // Unpublish the Feefo Review
                    List<FeefoReviews> publishedReviewsList = new List<FeefoReviews>();


                    // Fetch the reviews from the published Feefo Bucket and perform MD5 Check Sum
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsInputFolderKeyName)))
                    {
                        publishedReviews = reader.ReadToEnd();
                    }
                    publishedReviewsList = JsonConvert.DeserializeObject<List<FeefoReviews>>(publishedReviews);

                    foreach (FeefoReviews oldReview in publishedReviewsList)
                    {
                        if (oldReview.service.id == newReview.reviewId)
                        {
                            reviewFound = true;
                            reviewInformation = new ReviewInformation();

                            reviewInformation.reviewId = oldReview.service.id;
                            reviewInformation.reviewTitle = oldReview.service.title;
                            reviewInformation.reviewRating = oldReview.service.rating.rating;
                            reviewInformation.reviewProviderName = "Feefo";
                            reviewInformation.description = oldReview.service.review;
                            reviewInformation.postedOn = oldReview.last_modified_at;
                            reviewInformation.isPublished = true;

                            if (oldReview.customer != null)
                            {
                                customer = new CustomerInfo();
                                customer.postedBy = oldReview.customer.display_name;
                                reviewInformation.customer = customer;
                            }
                            string oldCheckSum = Utilities.BuildHashCodeForObject(reviewInformation);

                            if (oldCheckSum == Utilities.BuildHashCodeForObject(newReview))
                            {
                                // delete from published bucket and send a put Object request to put object in unpublished bucket
                                return UpdateFeefoReviewObject(newReview, publishedReviewsList); //return UpdateReviewObject(newReview);

                            }
                            else
                            {
                                apiResponse.errorMessage = "Trying to update an old copy of the review. Please get the updated review and perform the operation";
                                apiResponse.errorCode = 1;
                                return apiResponse;
                            }
                        }
                    }
                }
                else if (newReview.reviewProviderName.ToLower() == "trustpilot")
                {
                    awsBucketName = ConfigurationManager.AppSettings["TrustPilotReviewsBucket"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["TrustPilotPublishedReviewsJSON"];

                    // Unpublish the Trust Pilot Review
                    List<TrustPilotReviews> publishedTPReviewsList = new List<TrustPilotReviews>();

                    // Fetch the reviews from the published Trust Pilot Bucket and perform MD5 Check Sum
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsInputFolderKeyName)))
                    {
                        publishedReviews = reader.ReadToEnd();
                    }
                    publishedTPReviewsList = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(publishedReviews);

                    foreach (TrustPilotReviews oldReview in publishedTPReviewsList)
                    {
                        if (oldReview.id == newReview.reviewId)
                        {
                            reviewFound = true;
                            reviewInformation = new ReviewInformation();

                            reviewInformation.reviewId = oldReview.id;
                            reviewInformation.reviewTitle = oldReview.title;
                            reviewInformation.reviewRating = oldReview.stars;
                            reviewInformation.reviewProviderName = "TrustPilot";
                            reviewInformation.description = oldReview.text;
                            reviewInformation.postedOn = oldReview.last_modified_at;
                            reviewInformation.isPublished = true;

                            if (oldReview.consumer != null)
                            {
                                customer = new CustomerInfo();
                                customer.postedBy = oldReview.consumer.displayName;
                                reviewInformation.customer = customer;
                            }
                            string oldCheckSum = Utilities.BuildHashCodeForObject(reviewInformation);
                            if (oldCheckSum == Utilities.BuildHashCodeForObject(newReview))
                            {
                                // delete from published bucket and send a put Object request to put object in unpublished bucket
                                return UpdateTrustPilotReviewObject(newReview, publishedTPReviewsList); //return UpdateReviewObject(newReview);
                            }
                            else
                            {
                                apiResponse.errorMessage = "Trying to update an old copy of the review. Please get the updated review and perform the operation";
                                apiResponse.errorCode = 1;
                                return apiResponse;
                            }
                        }
                    }
                }
                if (!reviewFound)
                {
                    apiResponse.errorMessage = "Review is not present in the published bucket.";
                    apiResponse.errorCode = 1;
                    return apiResponse;
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
            return null;
        }

        public static ResponseSummary PublishReview(ReviewInformation newReview)
        {
            string awsBucketName = string.Empty;
            string awsInputFolderKeyName = string.Empty;
            ReviewInformation reviewInformation;
            CustomerInfo customer;
            ResponseSummary apiResponse = new ResponseSummary();
            bool reviewFound = false;

            try
            {
                string reviewInfo = string.Empty;
                string unPublishedReviewsInS3 = string.Empty;
                if (newReview.reviewProviderName.ToLower() == "feefo")
                {
                    awsBucketName = ConfigurationManager.AppSettings["FeefoReviewsBucket"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["FeefoUnPublishedReviewsJSON"];

                    // Publish the Feefo Review
                    List<FeefoReviews> unpublishedReviewsList = new List<FeefoReviews>();

                    // Fetch the reviews from the unpublished Feefo Bucket and perform MD5 Check Sum
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsInputFolderKeyName)))
                    {
                        unPublishedReviewsInS3 = reader.ReadToEnd();
                    }
                    unpublishedReviewsList = JsonConvert.DeserializeObject<List<FeefoReviews>>(unPublishedReviewsInS3);

                    FeefoReviews oldReview = (from feefoReview in unpublishedReviewsList
                                              where feefoReview.service.id.Equals(newReview.reviewId)
                                              select feefoReview).FirstOrDefault();

                    if (oldReview != null)
                    {
                        reviewInformation = new ReviewInformation();

                        reviewInformation.reviewId = oldReview.service.id;
                        reviewInformation.reviewTitle = oldReview.service.title;
                        reviewInformation.reviewRating = oldReview.service.rating.rating;
                        reviewInformation.reviewProviderName = "Feefo";
                        reviewInformation.description = oldReview.service.review;
                        reviewInformation.postedOn = oldReview.last_modified_at;
                        reviewInformation.isPublished = false;

                        if (oldReview.customer != null)
                        {
                            customer = new CustomerInfo();
                            customer.postedBy = oldReview.customer.display_name;
                            reviewInformation.customer = customer;
                        }
                        string oldCheckSum = Utilities.BuildHashCodeForObject(reviewInformation);

                        if (oldCheckSum == Utilities.BuildHashCodeForObject(newReview))
                        {
                            // delete from unpublished bucket and send a put Object request to put object in published bucket
                            return UpdateFeefoReviewObject(newReview, unpublishedReviewsList);

                        }
                        else
                        {
                            apiResponse.errorMessage = "Trying to update an old copy of the review. Please get the updated review and perform the operation";
                            apiResponse.errorCode = 1;
                            return apiResponse;
                        }
                    }
                }
                else if (newReview.reviewProviderName.ToLower() == "trustpilot")
                {
                    awsBucketName = ConfigurationManager.AppSettings["TrustPilotReviewsBucket"];
                    awsInputFolderKeyName = ConfigurationManager.AppSettings["TrustPilotUnPublishedReviewsJSON"];

                    // publish the Trust Pilot Review
                    List<TrustPilotReviews> unpublishedTPReviewsList = new List<TrustPilotReviews>();

                    // Fetch the reviews from the unpublished Trust Pilot Bucket and perform MD5 Check Sum
                    using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(awsBucketName, awsInputFolderKeyName)))
                    {
                        unPublishedReviewsInS3 = reader.ReadToEnd();
                    }
                    unpublishedTPReviewsList = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(unPublishedReviewsInS3);

                    foreach (TrustPilotReviews oldReview in unpublishedTPReviewsList)
                    {
                        if (oldReview.id == newReview.reviewId)
                        {
                            reviewInformation = new ReviewInformation();

                            reviewInformation.reviewId = oldReview.id;
                            reviewInformation.reviewTitle = oldReview.title;
                            reviewInformation.reviewRating = oldReview.stars;
                            reviewInformation.reviewProviderName = "TrustPilot";
                            reviewInformation.description = oldReview.text;
                            reviewInformation.postedOn = oldReview.last_modified_at;
                            reviewInformation.isPublished = false;

                            if (oldReview.consumer != null)
                            {
                                customer = new CustomerInfo();
                                customer.postedBy = oldReview.consumer.displayName;
                                reviewInformation.customer = customer;
                            }
                            string oldCheckSum = Utilities.BuildHashCodeForObject(reviewInformation);

                            if (oldCheckSum == Utilities.BuildHashCodeForObject(newReview))
                            {
                                // delete from unpublished bucket and send a put Object request to put object in published bucket
                                return UpdateTrustPilotReviewObject(newReview, unpublishedTPReviewsList);
                            }
                            else
                            {
                                apiResponse.errorMessage = "Trying to update an old copy of the review. Please get the updated review and perform the operation";
                                apiResponse.errorCode = 1;
                                return apiResponse;
                            }
                        }
                    }

                }
                if (!reviewFound)
                {
                    apiResponse.errorMessage = "Review is not present in the unpublished bucket.";
                    apiResponse.errorCode = 1;
                    return apiResponse;
                }

            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
            return null;
        }

        public static GenericReviews GetAllPublishedReviews(int limit, int startIndex, string reviewType)
        {
            try
            {
                GenericReviews genericReviews = new GenericReviews();
                List<Review> publishedReviewsList = new List<Review>();
                List<ReviewProvider> reviewProviders = new List<ReviewProvider>();
                List<FeefoReviews> feefoReviewsList = new List<FeefoReviews>();
                List<TrustPilotReviews> trustPilotReviewsList = new List<TrustPilotReviews>();
                Review review = null;
                Customer customer;

                string publishedReviews = string.Empty;

                // Feefo Published Reviews
                if (reviewType.ToLower() != "trustpilot")
                {
                    try
                    {
                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["FeefoReviewsBucket"], ConfigurationManager.AppSettings["FeefoPublishedReviewsJSON"])))
                        {
                            publishedReviews = reader.ReadToEnd();
                        }
                        feefoReviewsList = JsonConvert.DeserializeObject<List<FeefoReviews>>(publishedReviews);
                        foreach (FeefoReviews feeforeview in feefoReviewsList)
                        {
                            review = new Review();

                            if (feeforeview.customer != null)
                            {
                                customer = new Customer();
                                customer.postedBy = feeforeview.customer.display_name;
                                review.customer = customer;
                            }
                            review.id = feeforeview.service.id;
                            review.reviewTitle = feeforeview.service.title;
                            review.reviewRating = feeforeview.service.rating.rating;
                            review.description = feeforeview.service.review;
                            review.postedOn = Convert.ToDateTime(feeforeview.last_modified_at);
                            review.reviewProviderName = "Feefo";

                            publishedReviewsList.Add(review);
                        }
                        //feefoPublishedReviewsCount = publishedReviewsList.Count();
                    }
                    catch(Exception ex)
                    {
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                    }
                }
                if (reviewType.ToLower() != "feefo")
                {
                    try
                    {
                        publishedReviews = string.Empty;
                        // Trust Pilot Published Reviews
                        using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["TrustPilotReviewsBucket"], ConfigurationManager.AppSettings["TrustPilotPublishedReviewsJSON"])))
                        {
                            publishedReviews = reader.ReadToEnd();
                        }
                        trustPilotReviewsList = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(publishedReviews);
                        foreach (TrustPilotReviews trustPilotReview in trustPilotReviewsList)
                        {
                            review = new Review();

                            if (trustPilotReview.consumer != null)
                            {
                                customer = new Customer();

                                customer.postedBy = trustPilotReview.consumer.displayName;
                                review.customer = customer;
                            }
                            review.id = trustPilotReview.id;
                            review.reviewTitle = trustPilotReview.title;
                            review.reviewRating = trustPilotReview.stars;
                            review.description = trustPilotReview.text;
                            review.postedOn = Convert.ToDateTime(trustPilotReview.createdAt);
                            review.reviewProviderName = "TrustPilot";

                            publishedReviewsList.Add(review);
                        }
                        //trustPilotPublishedReviewsCount = publishedReviewsList.Count() - feefoPublishedReviewsCount;
                    }
                    catch(Exception ex)
                    {
                        ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                        throw ex;
                    }
                    
                }
                publishedReviewsList.Sort((review1, review2) => (-1 * DateTime.Compare(review1.postedOn, review2.postedOn)));
                publishedReviewsList = publishedReviewsList.Select(r => r).Skip(startIndex).Take(limit).ToList();
                genericReviews.reviews = publishedReviewsList;

                // Get Review Provider Details for Feefo
                string feefoResponse = string.Empty;
                FeefoReviewsSummary feefoReviewsSummary = new FeefoReviewsSummary();

                feefoReviewsSummary = GetFeefoReviewsSummary();

                ReviewProvider reviewProvider = new ReviewProvider();
                reviewProvider.name = "Feefo";
                reviewProvider.serviceRating = feefoReviewsSummary.rating.rating.ToString() + "/" + feefoReviewsSummary.rating.max.ToString();
                reviewProvider.totalReviewsCount = feefoReviewsSummary.meta.count;

                reviewProviders.Add(reviewProvider);

                // Get Review Provider Details for Trust Pilot
                string trustPilotResponse = string.Empty;
                TrustPilotReviewsSummary trustPilotReviewsSummary = new TrustPilotReviewsSummary();

                trustPilotReviewsSummary = GetTrustPilotReviewsSummary();
                reviewProvider = new ReviewProvider();
                reviewProvider.name = "TrustPilot";
                reviewProvider.serviceRating = trustPilotReviewsSummary.trustScore.ToString() + "/10";
                reviewProvider.totalReviewsCount = trustPilotReviewsSummary.numberOfReviews.total;

                reviewProviders.Add(reviewProvider);

                genericReviews.reviewProvider = reviewProviders;
                return genericReviews;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        public static ReviewResponse GetReviews(string limit, string startIndex, string reviewProvider, string showPublished, string showRatings)
        {
            try
            {
                string publishedReviews = string.Empty;
                string unpublishedReviews = string.Empty;
                int filteredReviewsCount = 0;

                ReviewResponse finalReviewResponse = new ReviewResponse();
                ResponseSummary errorInformation = new ResponseSummary();
                List<ReviewInformation> reviews = new List<ReviewInformation>();
                List<FeefoReviews> feefoPublishedReviews = new List<FeefoReviews>();

                FeefoReviewsSummary feefoReviewsSummary = new FeefoReviewsSummary();
                TrustPilotReviewsSummary trustPilotReviewsSummary = new TrustPilotReviewsSummary();

                ReviewInformation review = null;
                CustomerInfo customer;

                #region Feefo
                if (reviewProvider.ToLower() != "trustpilot")
                {
                    // Get the currently published Feefo Reviews
                    if (showPublished != "false")
                    {
                        try
                        {
                            using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["FeefoReviewsBucket"], ConfigurationManager.AppSettings["FeefoPublishedReviewsJSON"])))
                            {
                                publishedReviews = reader.ReadToEnd();
                            }
                            feefoPublishedReviews = JsonConvert.DeserializeObject<List<FeefoReviews>>(publishedReviews);
                            foreach (FeefoReviews feefoReview in feefoPublishedReviews)
                            {
                                review = new ReviewInformation();

                                review.isPublished = true;
                                review.reviewId = feefoReview.service.id;
                                review.reviewTitle = feefoReview.service.title;
                                review.description = feefoReview.service.review;
                                review.reviewRating = feefoReview.service.rating.rating;
                                review.reviewProviderName = "Feefo";
                                review.postedOn = feefoReview.last_modified_at;

                                if (feefoReview.customer != null)
                                {
                                    customer = new CustomerInfo();
                                    customer.postedBy = feefoReview.customer.display_name;
                                    review.customer = customer;
                                }

                                reviews.Add(review);
                            }
                            publishedReviews = string.Empty;
                        }
                        catch(Exception ex)
                        {
                            ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                            errorInformation.errorMessage = "FeefoPublished";
                        }
                    }
                    if (showPublished != "true")
                    {
                        try
                        {
                            using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["FeefoReviewsBucket"], ConfigurationManager.AppSettings["FeefoUnPublishedReviewsJSON"])))
                            {
                                unpublishedReviews = reader.ReadToEnd();
                            }

                            var feefoUnpublishedReviews = JsonConvert.DeserializeObject<List<FeefoReviews>>(unpublishedReviews);
                            foreach (FeefoReviews feefoReview in feefoUnpublishedReviews)
                            {
                                review = new ReviewInformation();

                                review.isPublished = false;
                                review.reviewId = feefoReview.service.id;
                                review.reviewTitle = feefoReview.service.title;
                                review.description = feefoReview.service.review;
                                review.reviewRating = feefoReview.service.rating.rating;
                                review.reviewProviderName = "Feefo";
                                review.postedOn = feefoReview.last_modified_at;

                                if (feefoReview.customer != null)
                                {
                                    customer = new CustomerInfo();
                                    customer.postedBy = feefoReview.customer.display_name;
                                    review.customer = customer;
                                }

                                reviews.Add(review);
                            }
                            unpublishedReviews = string.Empty;
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                            if (!string.IsNullOrWhiteSpace(errorInformation.errorMessage))
                                errorInformation.errorMessage += ".";
                            errorInformation.errorMessage += "FeefoUnpublished";
                        }
                    }
                }
                #endregion

                #region TrustPilot
                if (reviewProvider.ToLower() != "feefo")
                {
                    // Get the currently published Feefo Reviews
                    if (showPublished != "false")
                    {
                        try
                        {
                            using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["TrustPilotReviewsBucket"], ConfigurationManager.AppSettings["TrustPilotPublishedReviewsJSON"])))
                            {
                                publishedReviews = reader.ReadToEnd();
                            }
                            var trustPilotPublishedReviews = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(publishedReviews);
                            foreach (TrustPilotReviews trustPilotReview in trustPilotPublishedReviews)
                            {
                                review = new ReviewInformation();

                                review.isPublished = true;
                                review.reviewId = trustPilotReview.id;
                                review.reviewTitle = trustPilotReview.title;
                                review.description = trustPilotReview.text;
                                review.reviewRating = trustPilotReview.stars;
                                review.reviewProviderName = "TrustPilot";
                                review.postedOn = trustPilotReview.createdAt;

                                if (trustPilotReview.consumer != null)
                                {
                                    customer = new CustomerInfo();
                                    customer.postedBy = trustPilotReview.consumer.displayName;
                                    review.customer = customer;
                                }

                                reviews.Add(review);
                            }
                            publishedReviews = string.Empty;
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                            if (!string.IsNullOrWhiteSpace(errorInformation.errorMessage))
                                errorInformation.errorMessage += ".";
                            errorInformation.errorMessage += "TrustPilotPublished";
                        }                        
                    }
                    if (showPublished != "true")
                    {
                        try
                        {
                            using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["TrustPilotReviewsBucket"], ConfigurationManager.AppSettings["TrustPilotUnPublishedReviewsJSON"])))
                            {
                                unpublishedReviews = reader.ReadToEnd();
                            }

                            var trustPilotUnpublishedReviews = JsonConvert.DeserializeObject<List<TrustPilotReviews>>(unpublishedReviews);
                            foreach (TrustPilotReviews trustPilotReview in trustPilotUnpublishedReviews)
                            {
                                review = new ReviewInformation();

                                review.isPublished = false;
                                review.reviewId = trustPilotReview.id;
                                review.reviewTitle = trustPilotReview.title;
                                review.description = trustPilotReview.text;
                                review.reviewRating = trustPilotReview.stars;
                                review.reviewProviderName = "TrustPilot";
                                review.postedOn = trustPilotReview.createdAt;

                                if (trustPilotReview.consumer != null)
                                {
                                    customer = new CustomerInfo();
                                    customer.postedBy = trustPilotReview.consumer.displayName;
                                    review.customer = customer;
                                }

                                reviews.Add(review);
                            }
                            unpublishedReviews = string.Empty;
                        }
                        catch (Exception ex)
                        {
                            ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                            if (!string.IsNullOrWhiteSpace(errorInformation.errorMessage))
                                errorInformation.errorMessage += ".";
                            errorInformation.errorMessage += "TrustPilotUnpublished";
                        }                        
                    }
                }

                #endregion
                if(reviews.Count > 0)
                {
                    string[] ratings = showRatings.Split(',');
                    List<ReviewInformation> filteredReviews = new List<ReviewInformation>();
                    reviews = reviews.GroupBy(p => p.reviewId).Select(p => p.First()).ToList();

                    if (ratings.Length < 5)
                    {
                        foreach (string rating in ratings)
                        {
                            int ratingInNumber;
                            if(!string.IsNullOrWhiteSpace(rating) && int.TryParse(rating.Trim(), out ratingInNumber))
                            {
                                var reviewsList = reviews.Select(r => r).Where(r => r.reviewRating == ratingInNumber);
                                filteredReviews.AddRange(reviewsList);
                            }
                        }
                        reviews = filteredReviews;
                    }
                    if (filteredReviews.Count() > 0)
                        filteredReviewsCount = filteredReviews.Count();
                    else
                        filteredReviewsCount = reviews.Count();
                    reviews.Sort((review1, review2) => (-1 * DateTime.Compare(DateTime.Parse(review1.postedOn), DateTime.Parse(review2.postedOn))));
                    reviews = reviews.Skip(Convert.ToInt32(startIndex)).Take(Convert.ToInt32(limit)).ToList();
                }

                feefoReviewsSummary = GetFeefoReviewsSummary();
                trustPilotReviewsSummary = GetTrustPilotReviewsSummary();


                // If No Error has occured till this point
                if (string.IsNullOrWhiteSpace(errorInformation.errorMessage))
                {
                    errorInformation.errorCode = 0;
                    errorInformation.errorMessage = "SUCCESS";
                }
                else
                {
                    errorInformation.errorCode = 1;
                    errorInformation.errorMessage += ".FAILED";
                }
                finalReviewResponse.reviews = reviews;
                finalReviewResponse.error = errorInformation;
                finalReviewResponse.totalReviewsCount = feefoReviewsSummary.meta.count + trustPilotReviewsSummary.numberOfReviews.total;
                finalReviewResponse.filteredReviewsCount = filteredReviewsCount;

                return finalReviewResponse;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        public static FeefoReviewsSummary GetFeefoReviewsSummary()
        {
            try
            {
                string feefoResponse = string.Empty;
                FeefoReviewsSummary feefoReviewsSummary = new FeefoReviewsSummary();

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["FeefoReviewsBucket"], ConfigurationManager.AppSettings["FeefoReviewsSummaryJSON"])))
                {
                    feefoResponse = reader.ReadToEnd();
                }
                feefoReviewsSummary = JsonConvert.DeserializeObject<FeefoReviewsSummary>(feefoResponse);
                return feefoReviewsSummary;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }

        public static TrustPilotReviewsSummary GetTrustPilotReviewsSummary()
        {
            try
            {
                string trustPilotResponse = string.Empty;
                TrustPilotReviewsSummary trustPilotReviewsSummary = new TrustPilotReviewsSummary();

                using (StreamReader reader = new StreamReader(Utilities.DownloadS3ObjectForReviewsData(ConfigurationManager.AppSettings["TrustPilotReviewsBucket"], ConfigurationManager.AppSettings["TrustPilotReviewsSummaryJSON"])))
                {
                    trustPilotResponse = reader.ReadToEnd();
                }
                trustPilotReviewsSummary = JsonConvert.DeserializeObject<TrustPilotReviewsSummary>(trustPilotResponse);
                return trustPilotReviewsSummary;
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.Error);
                throw ex;
            }
        }
    }
}