﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
namespace Presentation.Web.WCFRestServices.TopDog
{
    public class MobileBasket
    {
        public TopDogBasketService.Basket TopDogBasket { get; set; }
        public Dictionary<string, string> Airports { get; set; }
        public Dictionary<string, string> Airlines { get; set; }

        public void UpdateAirportAndAirlineDetails()
        {
            if (TopDogBasket.Products!= null && TopDogBasket.Products.Length > 0)
            {
                List<TopDogBasketService.BasketProduct> prod = TopDogBasket.Products.Where(p => p.ProductType ==  TopDogBasketService.ProductType.FlightProduct).ToList();
                foreach (TopDogBasketService.BasketProduct bProd in prod)
                {
                    for (int x = 0; x < bProd.ProductDetails.FlightProduct.Itinerary.Length; x++)
                    {
                        for (int y = 0; y < bProd.ProductDetails.FlightProduct.Itinerary[0].Length; y++)
                        {
                                TopDogBasketService.SectorFlight flight = bProd.ProductDetails.FlightProduct.Itinerary[x][y];

                                if (!string.IsNullOrEmpty(flight.ArrivalAirportCode))
                                { AddAirportDetails(flight.ArrivalAirportCode); }

                                if (!string.IsNullOrEmpty(flight.DepartureAirportCode))
                                { AddAirportDetails(flight.DepartureAirportCode); }

                                if (!string.IsNullOrEmpty(flight.MarketingAirlineCode))
                                { AddAirlineDetails(flight.MarketingAirlineCode); }

                                if (!string.IsNullOrEmpty(flight.OperatingAirlineCode))
                                { AddAirlineDetails(flight.OperatingAirlineCode); }
                            }
                        }
                }
            }
        }        

        private void AddAirportDetails(string airportCode)
        {
            if (!Airports.ContainsKey(airportCode))
            {
                string airportName = GetAirportName(airportCode);
                Airports.Add(airportCode, airportName);
            }
        }

        private void AddAirlineDetails(string airlineCode)
        {
            if (!Airlines.ContainsKey(airlineCode))
            {
                string airlineName = GetAirlineName(airlineCode);
                Airlines.Add(airlineCode, airlineName);
            }
        }


        private string GetAirportName(string airportCode)
        {
            string airportName = "";            
            string fileName = HttpRuntime.AppDomainAppPath + @"data\AirportMaster.xml";
            DataSet dsAirport = new DataSet();

            if (System.IO.File.Exists(fileName))
            {
                dsAirport.ReadXml(fileName);

                foreach (DataRow row in dsAirport.Tables[0].Rows)
                {
                    if (row["code"].ToString().ToLower().Equals(airportCode.ToLower()))
                    {
                        airportName = row["name"].ToString();
                        break;
                    }
                }
            }
            return airportName;
        }

        private string GetAirlineName(string airlineCode)
        {
            string airlineName = "";
            string fileName = HttpRuntime.AppDomainAppPath + @"data\AirlinesIATACode.xml";
            DataSet dsAirline = new DataSet();

            if (System.IO.File.Exists(fileName))
            {
                dsAirline.ReadXml(fileName);

                foreach (DataRow row in dsAirline.Tables[0].Rows)
                {
                    if (row["IATACode"].ToString().ToLower().Equals(airlineCode.ToLower()))
                    {
                        airlineName = row["AirlineName"].ToString();
                        break;
                    }
                }
            }
            return airlineName;
        }
    }
}
