﻿using Presentation.WCFRestService.Model.Enum;
using System;
using System.IO;
using System.Web;

namespace Presentation.Web.WCFRestServices
{
    public static class ErrorLogger
    {
        public static void Log(string message, LogLevel logLevel)
        {
            string LogPath = HttpRuntime.AppDomainAppPath + @"Log\";
            string filename = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";

            if (logLevel == LogLevel.Information)
            {
                filename = "InformationLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearch)
            {
                filename = "Elasticsearch_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchDiversity)
            {
                filename = "ElasticSearchDiversity_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.MissingElasticSearchMappings)
            {
                filename = "MissingElasticSearchMappings" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.RoutingApi)
            {
                filename = "RoutingApi_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.RoutingApiError)
            {
                filename = "RoutingApiError_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.GetRoutingApiError)
            {
                filename = "GetRoutingApiError_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.CustomerReviews)
            {
                filename = "CustomerReviews_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.ElasticSearchAvailability)
            {
                filename = "ElasticSearchAvailability_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if(logLevel == LogLevel.PaySafeStandaloneCredit)
            {
                filename = "PaySafeStandaloneCredit_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.TTSSSearchResult)
            {
                filename = "TTSSSearchResults_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.TTSSSearchAPICallException)
            {
                filename = "TTSSSearchAPICallExceptions_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            if (logLevel == LogLevel.WidenedSearch)
            {
                filename = "WidenedSearch_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            }
            string filepath = LogPath + filename;
            try
            {
                if (File.Exists(filepath))
                {
                    using (StreamWriter writer = new StreamWriter(filepath, true))
                    {
                        if (logLevel == LogLevel.WidenedSearch || logLevel == LogLevel.TTSSSearchResult)
                        {
                            writer.WriteLine(message);
                        }
                        else
                        {
                            writer.WriteLine(DateTime.Now + "=> " + message);
                        }
                    }
                }
                else
                {
                    StreamWriter writer = File.CreateText(filepath);
                    if (logLevel == LogLevel.WidenedSearch)
                    {
                        writer.WriteLine("RequestType\tSearchRequestId\tTimeStamp\tDestinationId\tdepartureId\tdepartureDate\tdateMin\tdateMax\tduration\tadults\tchildren\tboardType\tratings\tsortType\ttradingNameId\tpriceMin\tpriceMax\tDestinationId\tdepartureId\tdepartureDate\tdateMin\tdateMax\tduration\tadults\tchildren\tboardType\tratings\tsortType\ttradingNameId\tpriceMin\tpriceMax\tdestinationType\tchannelId\tplatform\tOS\tresetStage");
                    }
                    else
                    {
                        writer.WriteLine(DateTime.Now + "=> " + message);
                    }                    
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.Message, LogLevel.Error);
            }
        }

    }
}