﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.DynamicSEO
{
    public class Holidays
    {
        public string templateId { get; set; }

        public string description { get; set; }

        public string metaDescription { get; set; }

        public string categoryId { get; set; }

        public string boardBasis { get; set; }

        public int duration { get; set; }

        public string starRating { get; set; }

        public string airport { get; set; }

        public int adults { get; set; }

        public string channel { get; set; }

        public int children { get; set; }

        public string title { get; set; }

        public string destination { get; set; }

        public int flexibilityDuration { get; set; }

        public int dateRolling { get; set; }

        public string destLabel { get; set; }

        public string regionId { get; set;  }

        public bool isHotelPage { get; set; }
        public string metaTitle { get; set; }
        public string parentRegionName { get; set; }
        public string grandRegionName { get; set; }
        public string bgImage { get; set; }

        public List<FooterLink> footerLinks { get; set; }

        public List<PlacesToGo> placesToGo { get; set; }
    }

    public class Link
    {
        public string title { get; set; }
        public string url { get; set; }
    }

    public class FooterLink
    {
        public string blockTitle { get; set; }
        public List<Link> links { get; set; }
    }

    public class PlacesToGo
    {        
        public string subheader { get; set; }        
        public string thumbnailImageurl { get; set; }
    }
}