﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Web.WCFRestServices.DynamicSEO
{

    public class Template
    {
        public string templateId { get; set; }

        public string description { get; set; }

        public string metaDescription { get; set; }

        public bool isDirty { get; set; }
    }
}