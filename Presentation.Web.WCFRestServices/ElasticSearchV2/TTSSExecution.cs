﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using System;
using System.Collections.Generic;
using System.Linq;
using Presentation.WCFRestService.Model.Enum;
using System.Web;
using Newtonsoft.Json;
using ServiceStack.Redis;
using System.Configuration;
using System.Threading.Tasks;
using System.Globalization;
using System.Text;
using System.Net;
using System.IO;
using static Presentation.WCFRestService.Model.ElasticSearchV2.FacetsResponseParams;
using System.Threading;
using Presentation.WCFRestService.Model.Misc;
using System.Xml.Serialization;
using System.Xml;
using Presentation.WCFRestService.Model;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class TTSSExecution
    {
        public static TTSSResultsParams HitTTSSToGetOffers(APISearchRequest apiSearchRequest, StringBuilder errorLogger)
        {
            //IE CODE
            string esOffersIndiceName = IndiceNameHelper.GetOffersIndex(apiSearchRequest);

            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            ttssResultsParams.isBulkInsertionOnRetry = true;
            TTSSResponseStream ttssRespStream = new TTSSResponseStream();
            OffersDocument offers;
            OffersDocumentJourney journey;
            OffersDocumentAccommodation accommodation;
            OffersDocumentOfferHotel hotel;
            OffersDocumentRating hotelTARating;
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;

            //IE CODE
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                parentAirportCodes = Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
            }

            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            int offersInserted = 0;
            int ttssCount = 0;
            int bulkInsertionRetryCount = 1;
            string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");            
            double offerDecorationTimeElapsed = 0;
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            List<string> distinctHotelKeys = new List<string>();
            int distinctQuoteCount = 0;
            Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            string ttssURL = QueryBuilder.GetTTSSSearchURL(apiSearchRequest);
            ttssRespStream = ExecuteQuery.ExecuteTTSSGetWebRequest(ttssURL, errorLogger);
            #region Json
            if (ConfigurationManager.AppSettings["TSSOutputFormat"] == "json")
            {
                var ttssResults = JsonConvert.DeserializeObject<TTSSResonseParms>(ttssRespStream.ttssResponse);
                ttssCount = ttssResults.Count;

                /* Reset Logic implementation for Null result Query - Starts Here */
                if (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && ttssCount == 0)
                {
                    ttssResultsParams.resetStage = GetResetStage(apiSearchRequest);
                    return ttssResultsParams;
                }
                else
                {
                    ttssResultsParams.resetStage = apiSearchRequest.resetStage;
                }
                /* Reset Logic implementation for Null result Query - Ends Here */

                distinctHotelKeys = (from offerResults in ttssResults.Results select offerResults.hotelKey).Distinct().ToList();
                System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
                offersDecorationTimer.Start();
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }
                //Parallel.ForEach(ttssResults.Results, parrOptions, results =>
                foreach (var results in ttssResults.Results)
                {
                    if (staticInfo.ContainsKey(int.Parse(results.hotelKey)))
                    {
                        try
                        {
                            staticResponseParms = staticInfo[int.Parse(results.hotelKey)];
                            offers = new OffersDocument();
                            offers.id = results.id;
                            offers.rank = int.Parse(results.rank);
                            offers.accommodationUpdateTime = results.accommodationUpdateTime;
                            offers.quoteRef = results.quoteRef;
                            //offers.price = results.price;
                            offers.isPreferential = false;
                            journey = new OffersDocumentJourney();

                            /****************** Implementation for flight triangulation *******************/
                            /*  Check if the outboundArrivalAirportCode is in the destinationArrivalAirportCode and
                            /*  Check if the alternative flight is available or not
                             *  Check if the Alternative flight details is available, So check if that is cheaper than the triangulation flight
                            */

                            if (ConfigurationManager.AppSettings["EnableTriangularFlightsForAllDestinations"] == "true" || airportCodeIsAvailableInFocusedDestinationAirportCode(results.outboundArrivalAirportCode))
                            {
                                if (results.alternativeTotalPrice != 0 && results.alternativeTotalPrice <= results.price)
                                {
                                    // Direct flight is cheaper than the triangulation flight
                                    offers.price = results.alternativeTotalPrice;
                                    journey.outboundDepartureDate = results.alternativeOutboundDepartureDate;
                                    journey.outboundArrivalDate = results.alternativeOutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.alternativeOutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.alternativeOutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.alternativeOutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.alternativeOutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.alternativeOutboundLegCount;
                                    journey.outboundFlightDuration = results.alternativeOutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.alternativeOutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.alternativeOutboundFlightNumber;
                                    journey.inboundDepartureDate = results.alternativeInboundDepartureDate;
                                    journey.inboundArrivalDate = results.alternativeInboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.alternativeInboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.alternativeInboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.alternativeInboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.alternativeInboundArrivalAirportName;
                                    journey.inboundLegCount = results.alternativeInboundLegCount;
                                    journey.inboundFlightDuration = results.alternativeInboundFlightDuration;
                                    journey.inboundAirlineICAO = results.alternativeInboundAirlineICAO;
                                    journey.inboundFlightNumber = results.alternativeInboundFlightNumber;
                                    journey.adultBagPrice = results.alternativeAdultBagPrice;
                                    journey.childBagPrice = results.alternativechildBagPrice;
                                    journey.duration = results.duration;
                                    journey.destination = results.destination;
                                    journey.glat = results.glat;
                                    journey.glong = results.glong;
                                }
                                else
                                {
                                    // Normal Flow (triangulation flight is cheaper)
                                    offers.price = results.price;
                                    journey.outboundDepartureDate = results.outboundDepartureDate;
                                    journey.outboundArrivalDate = results.outboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.outboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.outboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.outboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.outboundArrivalAirportCode;
                                    journey.outboundLegCount = results.outboundLegCount;
                                    journey.outboundFlightDuration = results.outboundFlightDuration;
                                    journey.outboundAirlineICAO = results.outboundAirlineICAO;
                                    journey.outboundFlightNumber = results.outboundFlightNumber;
                                    journey.inboundDepartureDate = results.inboundDepartureDate;
                                    journey.inboundArrivalDate = results.inboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.inboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.inboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.inboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.inboundArrivalAirportName;
                                    journey.inboundLegCount = results.inboundLegCount;
                                    journey.inboundFlightDuration = results.inboundFlightDuration;
                                    journey.inboundAirlineICAO = results.inboundAirlineICAO;
                                    journey.inboundFlightNumber = results.inboundFlightNumber;
                                    journey.adultBagPrice = results.adultBagPrice;
                                    journey.childBagPrice = results.childBagPrice;
                                    journey.duration = results.duration;
                                    journey.destination = results.destination;
                                    journey.glat = results.glat;
                                    journey.glong = results.glong;
                                }

                            }
                            else
                            {
                                if (results.alternativeTotalPrice != 0)
                                {
                                    // triangulation flight is there so, this will be direct flight
                                    offers.price = results.alternativeTotalPrice;
                                    journey.outboundDepartureDate = results.alternativeOutboundDepartureDate;
                                    journey.outboundArrivalDate = results.alternativeOutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.alternativeOutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.alternativeOutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.alternativeOutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.alternativeOutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.alternativeOutboundLegCount;
                                    journey.outboundFlightDuration = results.alternativeOutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.alternativeOutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.alternativeOutboundFlightNumber;
                                    journey.inboundDepartureDate = results.alternativeInboundDepartureDate;
                                    journey.inboundArrivalDate = results.alternativeInboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.alternativeInboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.alternativeInboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.alternativeInboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.alternativeInboundArrivalAirportName;
                                    journey.inboundLegCount = results.alternativeInboundLegCount;
                                    journey.inboundFlightDuration = results.alternativeInboundFlightDuration;
                                    journey.inboundAirlineICAO = results.alternativeInboundAirlineICAO;
                                    journey.inboundFlightNumber = results.alternativeInboundFlightNumber;
                                    journey.adultBagPrice = results.alternativeAdultBagPrice;
                                    journey.childBagPrice = results.alternativechildBagPrice;
                                    journey.duration = results.duration;
                                    journey.destination = results.destination;
                                    journey.glat = results.glat;
                                    journey.glong = results.glong;
                                }
                                else
                                {
                                    // no triangulation flight, so this is direct flight
                                    offers.price = results.price;
                                    journey.outboundDepartureDate = results.outboundDepartureDate;
                                    journey.outboundArrivalDate = results.outboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.outboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.outboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.outboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.outboundArrivalAirportCode;
                                    journey.outboundLegCount = results.outboundLegCount;
                                    journey.outboundFlightDuration = results.outboundFlightDuration;
                                    journey.outboundAirlineICAO = results.outboundAirlineICAO;
                                    journey.outboundFlightNumber = results.outboundFlightNumber;
                                    journey.inboundDepartureDate = results.inboundDepartureDate;
                                    journey.inboundArrivalDate = results.inboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.inboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.inboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.inboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.inboundArrivalAirportName;
                                    journey.inboundLegCount = results.inboundLegCount;
                                    journey.inboundFlightDuration = results.inboundFlightDuration;
                                    journey.inboundAirlineICAO = results.inboundAirlineICAO;
                                    journey.inboundFlightNumber = results.inboundFlightNumber;
                                    journey.adultBagPrice = results.adultBagPrice;
                                    journey.childBagPrice = results.childBagPrice;
                                    journey.duration = results.duration;
                                    journey.destination = results.destination;
                                    journey.glat = results.glat;
                                    journey.glong = results.glong;
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                            {
                                journey.glat = staticResponseParms.lat;
                                journey.glong = staticResponseParms.lng;
                            }
                            journey.parentRegion = staticResponseParms.parentRegion;
                            journey.parentRegionId = staticResponseParms.parentRegionId;
                            offers.journey = journey;
                            accommodation = new OffersDocumentAccommodation();
                            accommodation.boardTypeCode = results.boardTypeCode;
                            accommodation.boardTypeId = results.boardTypeId;
                            accommodation.children = results.children;
                            accommodation.adults = results.adults;
                            accommodation.rating = (int)Convert.ToDouble(results.rating);
                            accommodation.updated = results.updated;
                            offers.accommodation = accommodation;
                            hotel = new OffersDocumentOfferHotel();
                            hotel.iff = int.Parse(results.hotelKey);
                            hotel.features = staticResponseParms.features;
                            hotel.name = staticResponseParms.name;
                            hotelTARating = new OffersDocumentRating();
                            hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                            hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                            hotelTARating.reviewCount = staticResponseParms.reviewCount;
                            hotel.rating = hotelTARating;
                            offers.hotel = hotel;
                            offers.tradingNameId = new List<int>();
                            offers.tradingNameId.Add(int.Parse(results.tradingNameId));
                            offers.departureIds = int.Parse(results.departureId);
                            if (apiSearchRequest.departureIds < 0)
                            {
                                if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
                                {
                                    offers.parentDepartureIds = Utilities.GetParentDepartureId(apiSearchRequest.countrySite, int.Parse(results.departureId), apiSearchRequest.departureIds);
                                }
                                else
                                {
                                    offers.parentDepartureIds = parentAirportCodes[int.Parse(results.departureId)];
                                }
                            }
                            else
                            {
                                offers.parentDepartureIds = int.Parse(results.departureId);
                            }
                            offers.regionId = results.destinationId;
                            offers.labelId = apiSearchRequest.destinationIds;
                            //offers.timeStamp = timeStamp;

                            ////searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.quoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));
                            //searchParams.AppendLine("{\"update\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ParentType"] + "\",\"_id\":\"" + results.quoteRef + "\"}}\n{\"doc\":" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + ",\"doc_as_upsert\" : true}\n" + "{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"_id\":\"" + results.quoteRef + "\",\"_parent\":\"" + results.quoteRef + "\"}}\n{\"timeStamp\":\"" + timeStamp + "\"}");

                            //IE CODE
                            searchParams.AppendLine("{\"update\":{\"_index\":\"" + esOffersIndiceName + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ParentType"] + "\",\"_id\":\"" + results.quoteRef + "\"}}\n{\"doc\":" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + ",\"doc_as_upsert\" : true}\n" + "{\"index\":{\"_index\":\"" + esOffersIndiceName + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"_id\":\"" + results.quoteRef + "\",\"_parent\":\"" + results.quoteRef + "\"}}\n{\"timeStamp\":\"" + timeStamp + "\"}");

                            offersInserted++;
                        }
                        catch (Exception ex)
                        {
                            errorLogger.AppendLine((string.Format("Web exception occured while preparing offers. Url - {0}, Message - {1}, Stack Trace - {2}, ErrorMessage-2", ttssURL, ex.Message, ex.StackTrace)));
                        }
                        distinctQuoteCount--;
                    }
                }
                //);

                offersDecorationTimer.Stop();
                offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;
            }
            #endregion
            #region XML
            else
            {
                XmlDocument docResponse = new XmlDocument();
                docResponse.LoadXml(ttssRespStream.ttssResponse);
                XmlSerializer serializer = new XmlSerializer(typeof(Container));
                Container mytest = (Container)serializer.Deserialize(new StringReader(ttssRespStream.ttssResponse));

                if (ConfigurationManager.AppSettings["EnableTTSSSearchLogs"] == "true")
                {
                    ErrorLogger.Log(timeStamp + "\t" + ttssURL + "\t" + mytest.Results.Result.Count + "\n", LogLevel.TTSSSearchResult);
                }

                /* Reset Logic implementation for Null result Query - Starts Here */
                if (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && mytest.Results.Result.Count == 0 && apiSearchRequest.hotelKeys.Count == 0 && apiSearchRequest.resetStage != 4)
                {
                    ttssResultsParams.resetStage = GetResetStage(apiSearchRequest);
                    return ttssResultsParams;
                }
                else
                {
                    ttssResultsParams.resetStage = apiSearchRequest.resetStage;
                }
                /* Reset Logic implementation for Null result Query - Ends Here */

                MHIDStaticInfo mhidStaticInfo = new MHIDStaticInfo();
                distinctHotelKeys = (from ttssResult in mytest.Results.Result select ttssResult.HotelKey).Distinct().ToList();
                ttssCount = int.Parse(mytest.Results.Count);
                System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
                offersDecorationTimer.Start();
                
                #region offer Decoration
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        offersDecorated = redisStaticInfo.Count;
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }
                
                //Parallel.ForEach(mytest.Results.Result, parrOptions, results =>
                foreach (var results in mytest.Results.Result)
                {
                    if (staticInfo.ContainsKey(int.Parse(results.HotelKey)))
                    {
                        try
                        {
                            staticResponseParms = staticInfo[int.Parse(results.HotelKey)];
                            offers = new OffersDocument();
                            offers.id = results.Id;
                            offers.quoteRef = results.QuoteRef;
                            offers.isPreferential = false;
                            offers.rank = int.Parse(results.rank);
                            offers.accommodationUpdateTime = results.accommodationUpdateTime;
                            journey = new OffersDocumentJourney();
                            /****************** Implementation for flight triangulation *******************/
                            /*  Check if the outboundArrivalAirportCode is in the destinationArrivalAirportCode and
                            /*  Check if the alternative flight is available or not
                             *  Check if the Alternative flight details is available, So check if that is cheaper than the triangulation flight
                            */
                            if (ConfigurationManager.AppSettings["EnableTriangularFlightsForAllDestinations"] == "true" || airportCodeIsAvailableInFocusedDestinationAirportCode(results.OutboundArrivalAirportCode))
                            {
                                if (!string.IsNullOrWhiteSpace(results.AlternativeTotalPrice) && int.Parse(results.AlternativeTotalPrice) <= int.Parse(results.Price))
                                {
                                    // Direct flight is cheaper than the triangulation flight
                                    offers.price = (int)Convert.ToDouble(results.AlternativeTotalPrice);
                                    journey.outboundDepartureDate = results.AlternativeOutboundDepartureDate;
                                    journey.outboundArrivalDate = results.AlternativeOutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.AlternativeOutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.AlternativeOutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.AlternativeOutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.AlternativeOutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.AlternativeOutboundLegCount;
                                    journey.outboundFlightDuration = results.AlternativeOutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.AlternativeOutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.AlternativeOutboundFlightNumber;
                                    journey.inboundDepartureDate = results.AlternativeInboundDepartureDate;
                                    journey.inboundArrivalDate = results.AlternativeInboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.AlternativeInboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.AlternativeInboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.AlternativeInboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.AlternativeInboundArrivalAirportName;
                                    journey.inboundLegCount = results.AlternativeInboundLegCount;
                                    journey.inboundFlightDuration = results.AlternativeInboundFlightDuration;
                                    journey.inboundAirlineICAO = results.AlternativeInboundAirlineICAO;
                                    journey.inboundFlightNumber = results.AlternativeInboundFlightNumber;
                                    journey.adultBagPrice = results.AlternativeAdultBagPrice;
                                    journey.childBagPrice = results.AlternativechildBagPrice;
                                    journey.duration = int.Parse(results.Duration);
                                    journey.destination = results.Destination;
                                    journey.glat = results.Glat;
                                    journey.glong = results.Glong;
                                }
                                else
                                {
                                    // Normal Flow (triangulation flight is cheaper)
                                    offers.price = (int)Convert.ToDouble(results.Price);
                                    journey.outboundDepartureDate = results.OutboundDepartureDate;
                                    journey.outboundArrivalDate = results.OutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.OutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.OutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.OutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.OutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.OutboundLegCount;
                                    journey.outboundFlightDuration = results.OutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.OutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.OutboundFlightNumber;
                                    journey.inboundDepartureDate = results.InboundDepartureDate;
                                    journey.inboundArrivalDate = results.InboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.InboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.InboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.InboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.InboundArrivalAirportName;
                                    journey.inboundLegCount = results.InboundLegCount;
                                    journey.inboundFlightDuration = results.InboundFlightDuration;
                                    journey.inboundAirlineICAO = results.InboundAirlineICAO;
                                    journey.inboundFlightNumber = results.InboundFlightNumber;
                                    journey.adultBagPrice = results.AdultBagPrice;
                                    journey.childBagPrice = results.ChildBagPrice;
                                    journey.duration = int.Parse(results.Duration);
                                    journey.destination = results.Destination;
                                    journey.glat = results.Glat;
                                    journey.glong = results.Glong;
                                }

                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(results.AlternativeTotalPrice))
                                {
                                    // Direct flight is available in alternative field
                                    offers.price = (int)Convert.ToDouble(results.AlternativeTotalPrice);
                                    journey.outboundDepartureDate = results.AlternativeOutboundDepartureDate;
                                    journey.outboundArrivalDate = results.AlternativeOutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.AlternativeOutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.AlternativeOutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.AlternativeOutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.AlternativeOutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.AlternativeOutboundLegCount;
                                    journey.outboundFlightDuration = results.AlternativeOutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.AlternativeOutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.AlternativeOutboundFlightNumber;
                                    journey.inboundDepartureDate = results.AlternativeInboundDepartureDate;
                                    journey.inboundArrivalDate = results.AlternativeInboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.AlternativeInboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.AlternativeInboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.AlternativeInboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.AlternativeInboundArrivalAirportName;
                                    journey.inboundLegCount = results.AlternativeInboundLegCount;
                                    journey.inboundFlightDuration = results.AlternativeInboundFlightDuration;
                                    journey.inboundAirlineICAO = results.AlternativeInboundAirlineICAO;
                                    journey.inboundFlightNumber = results.AlternativeInboundFlightNumber;
                                    journey.adultBagPrice = results.AlternativeAdultBagPrice;
                                    journey.childBagPrice = results.AlternativechildBagPrice;
                                    journey.duration = int.Parse(results.Duration);
                                    journey.destination = results.Destination;
                                    journey.glat = results.Glat;
                                    journey.glong = results.Glong;
                                }
                                else
                                {
                                    // there is no triangulation flight so, this has to be used
                                    offers.price = (int)Convert.ToDouble(results.Price);
                                    journey.outboundDepartureDate = results.OutboundDepartureDate;
                                    journey.outboundArrivalDate = results.OutboundArrivalDate;
                                    journey.outboundDepartureAirportName = results.OutboundDepartureAirportName;
                                    journey.outboundDepartureAirportCode = results.OutboundDepartureAirportCode;
                                    journey.outboundArrivalAirportName = results.OutboundArrivalAirportName;
                                    journey.outboundArrivalAirportCode = results.OutboundArrivalAirportCode;
                                    journey.outboundLegCount = results.OutboundLegCount;
                                    journey.outboundFlightDuration = results.OutboundFlightDuration;
                                    journey.outboundAirlineICAO = results.OutboundAirlineICAO;
                                    journey.outboundFlightNumber = results.OutboundFlightNumber;
                                    journey.inboundDepartureDate = results.InboundDepartureDate;
                                    journey.inboundArrivalDate = results.InboundArrivalDate;
                                    journey.inboundDepartureAirportName = results.InboundDepartureAirportName;
                                    journey.inboundDepartureAirportCode = results.InboundDepartureAirportCode;
                                    journey.inboundArrivalAirportCode = results.InboundArrivalAirportCode;
                                    journey.inboundArrivalAirportName = results.InboundArrivalAirportName;
                                    journey.inboundLegCount = results.InboundLegCount;
                                    journey.inboundFlightDuration = results.InboundFlightDuration;
                                    journey.inboundAirlineICAO = results.InboundAirlineICAO;
                                    journey.inboundFlightNumber = results.InboundFlightNumber;
                                    journey.adultBagPrice = results.AdultBagPrice;
                                    journey.childBagPrice = results.ChildBagPrice;
                                    journey.duration = int.Parse(results.Duration);
                                    journey.destination = results.Destination;
                                    journey.glat = results.Glat;
                                    journey.glong = results.Glong;
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                            {
                                journey.glat = staticResponseParms.lat;
                                journey.glong = staticResponseParms.lng;
                            }
                            journey.parentRegion = staticResponseParms.parentRegion;
                            journey.parentRegionId = staticResponseParms.parentRegionId;
                            offers.journey = journey;
                            accommodation = new OffersDocumentAccommodation();
                            accommodation.boardTypeCode = results.BoardTypeCode;
                            accommodation.boardTypeId = int.Parse(results.BoardTypeId);
                            accommodation.children = int.Parse(results.Children);
                            accommodation.adults = int.Parse(results.Adults);
                            accommodation.rating = (int)Convert.ToDouble(results.Rating);
                            accommodation.updated = results.Updated;
                            offers.accommodation = accommodation;
                            hotel = new OffersDocumentOfferHotel();
                            hotel.iff = int.Parse(results.HotelKey);
                            hotel.features = staticResponseParms.features;
                            hotel.name = staticResponseParms.name;
                            hotelTARating = new OffersDocumentRating();
                            hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                            hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                            hotelTARating.reviewCount = staticResponseParms.reviewCount;
                            hotel.rating = hotelTARating;
                            offers.hotel = hotel;
                            offers.tradingNameId = new List<int>();
                            offers.tradingNameId.Add(int.Parse(results.TradingNameId));
                            offers.departureIds = int.Parse(results.DepartureId);
                            if (apiSearchRequest.departureIds < 0)
                            {
                                if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
                                {
                                    offers.parentDepartureIds = Utilities.GetParentDepartureId(apiSearchRequest.countrySite, int.Parse(results.DepartureId), apiSearchRequest.departureIds);
                                }
                                else
                                {
                                    offers.parentDepartureIds = parentAirportCodes[int.Parse(results.DepartureId)];
                                }
                            }
                            else
                            {
                                offers.parentDepartureIds = int.Parse(results.DepartureId);
                            }
                            offers.regionId = int.Parse(results.DestinationId);
                            offers.labelId = apiSearchRequest.destinationIds;                            
                            
                            //offers.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");

                            //IE CODE
                            searchParams.AppendLine("{\"update\":{\"_index\":\"" + esOffersIndiceName + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ParentType"] + "\",\"_id\":\"" + results.QuoteRef + "\"}}\n{\"doc\":" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + ",\"doc_as_upsert\" : true}\n" + "{\"index\":{\"_index\":\"" + esOffersIndiceName + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"_id\":\"" + results.QuoteRef + "\",\"_parent\":\"" + results.QuoteRef + "\"}}\n{\"timeStamp\":\"" + timeStamp + "\"}");
                            //searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.QuoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));
                            offersInserted++;
                        }
                        catch (Exception ex)
                        {
                            errorLogger.AppendLine((string.Format("Web exception occured while preparing offers. Url - {0}, Message - {1}, Stack Trace - {2},ErrorMessage-3", ttssURL, ex.Message, ex.StackTrace)));
                        }
                    }
                }
                //);
                #endregion

                offersDecorationTimer.Stop();
                offerDecorationTimeElapsed = offersDecorationTimer.Elapsed.TotalMilliseconds;
            }
            #endregion

            #region Offer Indexing
            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            if (distinctHotelKeys.Count != 0)
            {
                while (ttssResultsParams.isBulkInsertionOnRetry && bulkInsertionRetryCount >= 0)
                {
                    try
                    {
                        string bulkOffers = searchParams.ToString();
                        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "_bulk");
                        httpWebRequest.ContentType = "application/json";
                        httpWebRequest.Method = "POST";
                        httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                        httpWebRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                        {
                            streamWriter.Write(bulkOffers);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                        if (httpResponse != null)
                        {
                            httpResponse.Close();
                            httpResponse.Dispose();
                        }
                        ttssResultsParams.isBulkInsertionOnRetry = false;
                    }
                    catch (WebException ex)
                    {
                        errorLogger.AppendLine((string.Format("Web exception occured while Bulk call. Url - {0}, Message - {1}, Stack Trace - {2}, ErrorMessage-4", ttssURL, ex.Message, ex.StackTrace)));
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                    catch (Exception ex)
                    {
                        errorLogger.AppendLine((string.Format("Exception occured while making HTTP call. Url - {0}, Message - {1}, Stack Trace - {2}, ErrorMessage-5", ttssURL, ex.Message, ex.StackTrace)));
                        Thread.Sleep(500);
                        bulkInsertionRetryCount--;
                    }
                }
                string facetsResponse = string.Empty;
                FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();
                string facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
                try
                {
                    //IE CODE
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + esOffersIndiceName + "/_search", facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                    while (facetsTrials > 0)
                    {
                        if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                        {
                            break;
                        }
                        else
                        {
                            Thread.Sleep(500);
                            //IE CODE
                            facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + esOffersIndiceName + "/_search", facetsQuery);
                            facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                            facetsTrials--;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("Exception occured while checking the facets. Url - {0}, Message - {1}, Stack Trace - {2}, ErrorMessage-6", ttssURL, ex.Message, ex.StackTrace)));
                }           
            }
            offersIndexingTimer.Stop();
            #endregion

            ttssResultsParams.ttssCount = ttssCount;
            ttssResultsParams.decoratingTime = offerDecorationTimeElapsed;
            ttssResultsParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.offersDecorated = offersDecorated;
            ttssResultsParams.offersFound = distinctHotelKeys.Count;
            ttssResultsParams.offersInserted = offersInserted;
            ttssResultsParams.ttssResponseTime = ttssRespStream.ttssResponseTime;
            ttssResultsParams.facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]) - facetsTrials;
            ttssResultsParams.ttssURL = ttssURL;
            return ttssResultsParams;
        }
        private static bool airportCodeIsAvailableInFocusedDestinationAirportCode(string outboundArrivalAirportCode)
        {
            List<string> destinationAirportAirportCodes = Global.destinationAirportCodesForFlightTriangulation;

            foreach (string airportCode in destinationAirportAirportCodes)
            {
                if (outboundArrivalAirportCode.ToLower().Trim().Equals(airportCode.ToLower().Trim()))
                {
                    return true;
                }
            }
            return false;
        }

        public static TTSSResultsParams HitTTSSToGetDiversityOffers(APIDiversityRequest apiDiversityRequest, StringBuilder errorLogger)
        {
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            ttssResultsParams.isBulkInsertionOnRetry = true;
            TTSSResponseStream ttssRespStream = new TTSSResponseStream();
            OffersDocument offers;
            OffersDocumentJourney journey;
            OffersDocumentAccommodation accommodation;
            OffersDocumentOfferHotel hotel;
            OffersDocumentRating hotelTARating;
            string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
            int facetsTrials = int.Parse(ConfigurationManager.AppSettings["FacetsTrails"]);
            StringBuilder searchParams = new StringBuilder();
            searchParams.Append("");
            ParallelOptions parrOptions = new ParallelOptions { MaxDegreeOfParallelism = Global.MaxDegreeOfParallelism };
            string ttssURL = QueryBuilder.GetTTSSDiversityURL(apiDiversityRequest);
            ttssRespStream = ExecuteQuery.ExecuteTTSSGetWebRequest(ttssURL, errorLogger);
            var ttssResults = JsonConvert.DeserializeObject<TTSSResonseParms>(ttssRespStream.ttssResponse);
            Dictionary<string, int> reverseAirportCodes = Global.reverseAirportCodes;
            Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;

            //IE Code
            //TODO: Move all tenant specfic json retrieval to a helper
            if (apiDiversityRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                parentAirportCodes = Global.parentAirportCodesMT[apiDiversityRequest.countrySite.ToLower()];
                reverseAirportCodes = Global.reverseAirportCodesMT[apiDiversityRequest.countrySite.ToLower()];
            }

            List<string> distinctHotelKeys = (from offerResults in ttssResults.Results select offerResults.hotelKey).Distinct().ToList();
            List<string> redisStaticInfo = null;
            int offersDecorated = 0;
            int bulkInsertionRetryCount = 1;
            int offersInserted = 0;
            Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
            StaticResponseParms staticResponseParms = new StaticResponseParms();
            System.Diagnostics.Stopwatch offersDecorationTimer = new System.Diagnostics.Stopwatch();
            offersDecorationTimer.Start();
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                    redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                    offersDecorated = redisStaticInfo.Count;
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                        if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                        {
                            staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                        }
                    }
                }
            }
            //Parallel.ForEach(ttssResults.Results, parrOptions, results =>
            foreach (var results in ttssResults.Results)
            {
                if (staticInfo.ContainsKey(int.Parse(results.hotelKey)))
                {
                    try
                    {
                        staticResponseParms = staticInfo[int.Parse(results.hotelKey)];
                        offers = new OffersDocument();
                        offers.id = results.id;
                        offers.rank = int.Parse(results.rank);
                        offers.accommodationUpdateTime = results.accommodationUpdateTime;
                        offers.quoteRef = results.quoteRef;
                        //offers.price = results.price;
                        offers.isPreferential = false;
                        journey = new OffersDocumentJourney();

                        /****************** Implementation for flight triangulation *******************/
                        /*  Check if the outboundArrivalAirportCode is in the destinationArrivalAirportCode and
                        /*  Check if the alternative flight is available or not
                         *  Check if the Alternative flight details is available, So check if that is cheaper than the triangulation flight
                        */

                        if (airportCodeIsAvailableInFocusedDestinationAirportCode(results.outboundArrivalAirportCode))
                        {
                            if (results.alternativeTotalPrice != 0 && results.alternativeTotalPrice <= results.price)
                            {
                                // Direct flight is cheaper than the triangulation flight
                                offers.price = results.alternativeTotalPrice;
                                journey.outboundDepartureDate = results.alternativeOutboundDepartureDate;
                                journey.outboundArrivalDate = results.alternativeOutboundArrivalDate;
                                journey.outboundDepartureAirportName = results.alternativeOutboundDepartureAirportName;
                                journey.outboundDepartureAirportCode = results.alternativeOutboundDepartureAirportCode;
                                journey.outboundArrivalAirportName = results.alternativeOutboundArrivalAirportName;
                                journey.outboundArrivalAirportCode = results.alternativeOutboundArrivalAirportCode;
                                journey.outboundLegCount = results.alternativeOutboundLegCount;
                                journey.outboundFlightDuration = results.alternativeOutboundFlightDuration;
                                journey.outboundAirlineICAO = results.alternativeOutboundAirlineICAO;
                                journey.outboundFlightNumber = results.alternativeOutboundFlightNumber;
                                journey.inboundDepartureDate = results.alternativeInboundDepartureDate;
                                journey.inboundArrivalDate = results.alternativeInboundArrivalDate;
                                journey.inboundDepartureAirportName = results.alternativeInboundDepartureAirportName;
                                journey.inboundDepartureAirportCode = results.alternativeInboundDepartureAirportCode;
                                journey.inboundArrivalAirportCode = results.alternativeInboundArrivalAirportCode;
                                journey.inboundArrivalAirportName = results.alternativeInboundArrivalAirportName;
                                journey.inboundLegCount = results.alternativeInboundLegCount;
                                journey.inboundFlightDuration = results.alternativeInboundFlightDuration;
                                journey.inboundAirlineICAO = results.alternativeInboundAirlineICAO;
                                journey.inboundFlightNumber = results.alternativeInboundFlightNumber;
                                journey.adultBagPrice = results.alternativeAdultBagPrice;
                                journey.childBagPrice = results.alternativechildBagPrice;
                                journey.duration = results.duration;
                                journey.destination = results.destination;
                                journey.glat = results.glat;
                                journey.glong = results.glong;
                            }
                            else
                            {
                                // Normal Flow (triangulation flight is cheaper)
                                offers.price = results.price;
                                journey.outboundDepartureDate = results.outboundDepartureDate;
                                journey.outboundArrivalDate = results.outboundArrivalDate;
                                journey.outboundDepartureAirportName = results.outboundDepartureAirportName;
                                journey.outboundDepartureAirportCode = results.outboundDepartureAirportCode;
                                journey.outboundArrivalAirportName = results.outboundArrivalAirportName;
                                journey.outboundArrivalAirportCode = results.outboundArrivalAirportCode;
                                journey.outboundLegCount = results.outboundLegCount;
                                journey.outboundFlightDuration = results.outboundFlightDuration;
                                journey.outboundAirlineICAO = results.outboundAirlineICAO;
                                journey.outboundFlightNumber = results.outboundFlightNumber;
                                journey.inboundDepartureDate = results.inboundDepartureDate;
                                journey.inboundArrivalDate = results.inboundArrivalDate;
                                journey.inboundDepartureAirportName = results.inboundDepartureAirportName;
                                journey.inboundDepartureAirportCode = results.inboundDepartureAirportCode;
                                journey.inboundArrivalAirportCode = results.inboundArrivalAirportCode;
                                journey.inboundArrivalAirportName = results.inboundArrivalAirportName;
                                journey.inboundLegCount = results.inboundLegCount;
                                journey.inboundFlightDuration = results.inboundFlightDuration;
                                journey.inboundAirlineICAO = results.inboundAirlineICAO;
                                journey.inboundFlightNumber = results.inboundFlightNumber;
                                journey.adultBagPrice = results.adultBagPrice;
                                journey.childBagPrice = results.childBagPrice;
                                journey.duration = results.duration;
                                journey.destination = results.destination;
                                journey.glat = results.glat;
                                journey.glong = results.glong;
                            }

                        }
                        else
                        {
                            if (results.alternativeTotalPrice != 0)
                            {
                                // triangulation flight is there so, this will be direct flight
                                offers.price = results.alternativeTotalPrice;
                                journey.outboundDepartureDate = results.alternativeOutboundDepartureDate;
                                journey.outboundArrivalDate = results.alternativeOutboundArrivalDate;
                                journey.outboundDepartureAirportName = results.alternativeOutboundDepartureAirportName;
                                journey.outboundDepartureAirportCode = results.alternativeOutboundDepartureAirportCode;
                                journey.outboundArrivalAirportName = results.alternativeOutboundArrivalAirportName;
                                journey.outboundArrivalAirportCode = results.alternativeOutboundArrivalAirportCode;
                                journey.outboundLegCount = results.alternativeOutboundLegCount;
                                journey.outboundFlightDuration = results.alternativeOutboundFlightDuration;
                                journey.outboundAirlineICAO = results.alternativeOutboundAirlineICAO;
                                journey.outboundFlightNumber = results.alternativeOutboundFlightNumber;
                                journey.inboundDepartureDate = results.alternativeInboundDepartureDate;
                                journey.inboundArrivalDate = results.alternativeInboundArrivalDate;
                                journey.inboundDepartureAirportName = results.alternativeInboundDepartureAirportName;
                                journey.inboundDepartureAirportCode = results.alternativeInboundDepartureAirportCode;
                                journey.inboundArrivalAirportCode = results.alternativeInboundArrivalAirportCode;
                                journey.inboundArrivalAirportName = results.alternativeInboundArrivalAirportName;
                                journey.inboundLegCount = results.alternativeInboundLegCount;
                                journey.inboundFlightDuration = results.alternativeInboundFlightDuration;
                                journey.inboundAirlineICAO = results.alternativeInboundAirlineICAO;
                                journey.inboundFlightNumber = results.alternativeInboundFlightNumber;
                                journey.adultBagPrice = results.alternativeAdultBagPrice;
                                journey.childBagPrice = results.alternativechildBagPrice;
                                journey.duration = results.duration;
                                journey.destination = results.destination;
                                journey.glat = results.glat;
                                journey.glong = results.glong;
                            }
                            else
                            {
                                // no triangulation flight, so this is direct flight
                                offers.price = results.price;
                                journey.outboundDepartureDate = results.outboundDepartureDate;
                                journey.outboundArrivalDate = results.outboundArrivalDate;
                                journey.outboundDepartureAirportName = results.outboundDepartureAirportName;
                                journey.outboundDepartureAirportCode = results.outboundDepartureAirportCode;
                                journey.outboundArrivalAirportName = results.outboundArrivalAirportName;
                                journey.outboundArrivalAirportCode = results.outboundArrivalAirportCode;
                                journey.outboundLegCount = results.outboundLegCount;
                                journey.outboundFlightDuration = results.outboundFlightDuration;
                                journey.outboundAirlineICAO = results.outboundAirlineICAO;
                                journey.outboundFlightNumber = results.outboundFlightNumber;
                                journey.inboundDepartureDate = results.inboundDepartureDate;
                                journey.inboundArrivalDate = results.inboundArrivalDate;
                                journey.inboundDepartureAirportName = results.inboundDepartureAirportName;
                                journey.inboundDepartureAirportCode = results.inboundDepartureAirportCode;
                                journey.inboundArrivalAirportCode = results.inboundArrivalAirportCode;
                                journey.inboundArrivalAirportName = results.inboundArrivalAirportName;
                                journey.inboundLegCount = results.inboundLegCount;
                                journey.inboundFlightDuration = results.inboundFlightDuration;
                                journey.inboundAirlineICAO = results.inboundAirlineICAO;
                                journey.inboundFlightNumber = results.inboundFlightNumber;
                                journey.adultBagPrice = results.adultBagPrice;
                                journey.childBagPrice = results.childBagPrice;
                                journey.duration = results.duration;
                                journey.destination = results.destination;
                                journey.glat = results.glat;
                                journey.glong = results.glong;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(staticResponseParms.lat) && !string.IsNullOrWhiteSpace(staticResponseParms.lng))
                        {
                            journey.glat = staticResponseParms.lat;
                            journey.glong = staticResponseParms.lng;
                        }
                        journey.parentRegion = staticResponseParms.parentRegion;
                        journey.parentRegionId = staticResponseParms.parentRegionId;
                        offers.journey = journey;
                        accommodation = new OffersDocumentAccommodation();
                        accommodation.boardTypeCode = results.boardTypeCode;
                        accommodation.boardTypeId = results.boardTypeId;
                        accommodation.children = results.children;
                        accommodation.adults = results.adults;
                        accommodation.rating = (int)Convert.ToDouble(results.rating);
                        accommodation.updated = results.updated;
                        offers.accommodation = accommodation;
                        hotel = new OffersDocumentOfferHotel();
                        hotel.iff = int.Parse(results.hotelKey);
                        hotel.features = staticResponseParms.features;
                        hotel.name = staticResponseParms.name;
                        hotelTARating = new OffersDocumentRating();
                        hotelTARating.tripAdvisorId = staticResponseParms.tripAdvisorId;
                        hotelTARating.averageRating = (int)((float)staticResponseParms.averageRating * 10);
                        hotelTARating.reviewCount = staticResponseParms.reviewCount;
                        hotel.rating = hotelTARating;
                        offers.hotel = hotel;
                        offers.tradingNameId = new List<int>();
                        offers.tradingNameId.Add(int.Parse(results.tradingNameId));
                        offers.departureIds = int.Parse(results.departureId);
                        if (apiDiversityRequest.departureIds < 0)
                        {
                            if (apiDiversityRequest.countrySite != Global.COUNTRY_SITE_UK)
                            {
                                offers.parentDepartureIds = Utilities.GetParentDepartureId(apiDiversityRequest.countrySite, int.Parse(results.departureId), apiDiversityRequest.departureIds);
                            }
                            else
                            {
                                offers.parentDepartureIds = parentAirportCodes[int.Parse(results.departureId)];
                            }
                        }
                        else
                        {
                            offers.parentDepartureIds = int.Parse(results.departureId);
                        }
                        offers.regionId = results.destinationId;
                        offers.labelId = apiDiversityRequest.destinationId;
                        ////searchParams.AppendLine("{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_id\":\"" + results.quoteRef + "\", \"_type\":\"" + ConfigurationManager.AppSettings["ResultsIndiceDataType"] + "\"}}\n" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers));

                        //IE CODE
                        //searchParams.AppendLine("{\"update\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ParentType"] + "\",\"_id\":\"" + results.quoteRef + "\"}}\n{\"doc\":" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + ",\"doc_as_upsert\" : true}\n" + "{\"index\":{\"_index\":\"" + ConfigurationManager.AppSettings["OffersIndice"] + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"_id\":\"" + results.quoteRef + "\",\"_parent\":\"" + results.quoteRef + "\"}}\n{\"timeStamp\":\"" + timeStamp + "\"}");
                        searchParams.AppendLine("{\"update\":{\"_index\":\"" + IndiceNameHelper.GetOffersIndex(apiDiversityRequest.countrySite) + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ParentType"] + "\",\"_id\":\"" + results.quoteRef + "\"}}\n{\"doc\":" + new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(offers) + ",\"doc_as_upsert\" : true}\n" + "{\"index\":{\"_index\":\"" + IndiceNameHelper.GetOffersIndex(apiDiversityRequest.countrySite) + "\",\"_type\":\"" + ConfigurationManager.AppSettings["ChildType"] + "\",\"_id\":\"" + results.quoteRef + "\",\"_parent\":\"" + results.quoteRef + "\"}}\n{\"timeStamp\":\"" + timeStamp + "\"}");
                        offersInserted++;
                    }
                    catch (Exception ex)
                    {
                        errorLogger.AppendLine((string.Format("Web exception occured while preparing offers. Url - {0}, Message - {1}, Stack Trace - {2} ERD-1", ttssURL, ex.Message, ex.StackTrace)));
                    }
                }
            }
            //);
            offersDecorationTimer.Stop();
            System.Diagnostics.Stopwatch offersIndexingTimer = new System.Diagnostics.Stopwatch();
            offersIndexingTimer.Start();
            while (ttssResultsParams.isBulkInsertionOnRetry && bulkInsertionRetryCount > 0)
            {
                try
                {
                    string bulkOffers = searchParams.ToString();
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + "/_bulk");
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";
                    httpWebRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(bulkOffers);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    if (httpResponse != null)
                    {
                        httpResponse.Close();
                        httpResponse.Dispose();
                    }

                    ttssResultsParams.isBulkInsertionOnRetry = false;
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("ERD-2 --> Exception - {0} & Bulk Request Failed TTSS URL - {1}", ex.Message, ttssURL)));
                    Thread.Sleep(500);
                    bulkInsertionRetryCount--;
                }
            }
            string facetsResponse = string.Empty;
            FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();
            string facetsQuery = QueryBuilder.GetFacetsDiversityQuery(apiDiversityRequest);
            try
            {
                //IE CODE
                //facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search", facetsQuery);
                facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetOffersIndex(apiDiversityRequest.countrySite) + "/_search", facetsQuery);
                facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                while (facetsTrials > 0)
                {
                    if (facetsRequestResponse.aggregations.price_max.value != 0 && facetsRequestResponse.aggregations.price_min.value != 0 && facetsRequestResponse.hits.total >= Math.Round(offersInserted * 0.8))
                    {
                        break;
                    }
                    else
                    {
                        Thread.Sleep(500);
                        //IE CODE
                        //facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search", facetsQuery);
                        facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetOffersIndex(apiDiversityRequest.countrySite) + "/_search", facetsQuery);
                        facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                        facetsTrials--;
                    }
                }
            }
            catch (Exception ex)
            {
                errorLogger.AppendLine((string.Format("ERD-3 --> Exception - {0} & Facets Request ", ex.Message)));
            }
            offersIndexingTimer.Stop();
            ttssResultsParams.ttssCount = ttssResults.Count;
            ttssResultsParams.decoratingTime = offersDecorationTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.indexingTime = offersIndexingTimer.Elapsed.TotalMilliseconds;
            ttssResultsParams.offersDecorated = offersDecorated;
            ttssResultsParams.offersFound = distinctHotelKeys.Count;
            ttssResultsParams.offersInserted = offersInserted;
            ttssResultsParams.ttssResponseTime = ttssRespStream.ttssResponseTime;
            ttssResultsParams.ttssURL = ttssURL;
            return ttssResultsParams;
        }

        private static int GetResetStage(APISearchRequest apiSearchRequest)
        {
            int resetStage = -1;
            // Check for Stage 1 -> +/- 3 Days
            if (apiSearchRequest.departureDate == apiSearchRequest.dateMin && apiSearchRequest.departureDate == apiSearchRequest.dateMax)
            {
                resetStage = 1;
                return resetStage;
            }
            // Check for Stage 2 -> If Airport Group is not selected.

            //IE Code
            //TODO: Refactor to make it MT
            if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_IE)
            {
                if (!ConfigurationManager.AppSettings["PrincipleAirportGroupListIE"].Contains(apiSearchRequest.departureIds.ToString()))
                {
                    resetStage = 2;
                    return resetStage;
                }
            }
            else if (!ConfigurationManager.AppSettings["PrincipleAirportGroupList"].Contains(apiSearchRequest.departureIds.ToString()))
            {
                resetStage = 2;
                return resetStage;
            }
            // Check for Stage 3 -> If destination is not a parent destination, change destination id to parent destination Id
            if (!checkIfParentDestination(apiSearchRequest.destinationIds))
            {
                resetStage = 3;
                return resetStage;
            }
            // Check for Stage 4 -> If still returns no result, change destination id to 'TText Fav'
            resetStage = 4;
            return resetStage;
        }

        private static bool checkIfParentDestination(int destinationIds)
        {
            if (Global.parentDestinations.ContainsKey(destinationIds))
            {
                return false;
            }
            return true;            
        }

        public static APISearchRequest ResetSearchRequestParams(APISearchRequest apiSearchRequest, int resetStage)
        {
            if (resetStage == 1)
            {
                // Flexibility date is not chosen, so add the flexibility of  +/- 3 days
                // Setting the DateMin
                double dayDiff = (apiSearchRequest.departureDate - DateTime.UtcNow).TotalDays - 1;
                if(dayDiff > 3)
                {
                    dayDiff = 3;
                }
                apiSearchRequest.dateMin = apiSearchRequest.departureDate.AddDays(-1 * dayDiff);

                // Setting the DateMax
                // dayDiff = (apiSearchRequest.departureDate - DateTime.UtcNow).TotalDays - 1;
                apiSearchRequest.dateMax = apiSearchRequest.departureDate.AddDays(3);
                apiSearchRequest.resetStage = 1;
            }
            else if (resetStage == 2)
            {
                //IE CODE
                if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
                {
                    if (Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()].ContainsKey(apiSearchRequest.departureIds))
                    {
                        apiSearchRequest.departureIds = Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()][apiSearchRequest.departureIds];
                        apiSearchRequest.resetStage = 2;
                    }
                }
                // Global.parentAirportCodes
                else if (Global.parentAirportCodes.ContainsKey(apiSearchRequest.departureIds))
                {
                    apiSearchRequest.departureIds = Global.parentAirportCodes[apiSearchRequest.departureIds];
                    apiSearchRequest.resetStage = 2;
                }
            }
            else if (resetStage == 3)
            {
                if(Global.parentDestinations.ContainsKey(apiSearchRequest.destinationIds))
                {
                    apiSearchRequest.destinationIds = Global.parentDestinations[apiSearchRequest.destinationIds];
                    apiSearchRequest.resetStage = 3;
                }
            }
            else if (resetStage == 4)
            {
                apiSearchRequest.destinationIds = int.Parse(ConfigurationManager.AppSettings["FallbackDestinationId"]);
                apiSearchRequest.destinationType = ConfigurationManager.AppSettings["DefaultDestinationType"];
                apiSearchRequest.labelId = 0;
                apiSearchRequest.resetStage = 4;
            }
            return apiSearchRequest;
        }

        public static APISearchRequest JumpToResetStage(APISearchRequest apiSearchRequest, int jumpResetStage)
        {
            int resetStage = 1;
            while(resetStage <= jumpResetStage)
            {
                if (resetStage == 1)
                {
                    // Flexibility date is not chosen, so add the flexibility of  +/- 3 days
                    // Setting the DateMin
                    double dayDiff = (apiSearchRequest.departureDate - DateTime.UtcNow).TotalDays - 1;
                    if (dayDiff > 3)
                    {
                        dayDiff = 3;
                    }
                    apiSearchRequest.dateMin = apiSearchRequest.departureDate.AddDays(-1 * dayDiff);

                    // Setting the DateMax
                    apiSearchRequest.dateMax = apiSearchRequest.departureDate.AddDays(3);
                    apiSearchRequest.resetStage = 1;
                }
                else if (resetStage == 2)
                {
                    //IE CODE
                    if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
                    {
                        if (Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()].ContainsKey(apiSearchRequest.departureIds))
                        {
                            apiSearchRequest.departureIds = Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()][apiSearchRequest.departureIds];
                            apiSearchRequest.resetStage = 2;
                        }
                    }
                    // Global.parentAirportCodes
                    else if (Global.parentAirportCodes.ContainsKey(apiSearchRequest.departureIds))
                    {
                        apiSearchRequest.departureIds = Global.parentAirportCodes[apiSearchRequest.departureIds];
                        apiSearchRequest.resetStage = 2;
                    }
                }
                else if (resetStage == 3)
                {
                    if (Global.parentDestinations.ContainsKey(apiSearchRequest.destinationIds))
                    {
                        apiSearchRequest.destinationIds = Global.parentDestinations[apiSearchRequest.destinationIds];
                        apiSearchRequest.resetStage = 3;
                    }
                }
                else if (resetStage == 4)
                {
                    apiSearchRequest.destinationIds = int.Parse(ConfigurationManager.AppSettings["FallbackDestinationId"]);
                    apiSearchRequest.destinationType = ConfigurationManager.AppSettings["DefaultDestinationType"];
                    apiSearchRequest.resetStage = 4;
                }
                resetStage++;
            }
            return apiSearchRequest;
        }
    }
}