﻿using Presentation.WCFRestService.Model.ElasticSearchV2;
using Presentation.WCFRestService.Model.Enum;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web;
using static Presentation.WCFRestService.Model.ElasticSearchV2.FacetsResponseParams;
using static Presentation.WCFRestService.Model.ElasticSearchV2.OffersResponseParams;
using System.Xml;
using static Presentation.WCFRestService.Model.ElasticSearchV2.SearchResponseParams;
using static Presentation.WCFRestService.Model.ElasticSearchV2.DiversityResponseParams;
using Presentation.WCFRestService.Model.Misc;
using System.Text;
using System.Text.RegularExpressions;
using ServiceStack.Redis;

namespace Presentation.Web.WCFRestServices.ElasticSearchV2
{
    public class OffersBuilder
    {
        public static HolidayOffers GenerateOffers(SearchRequestAvailability searchRequestAvailability, APISearchRequest apiSearchRequest, StringBuilder errorLogger)
        {
            HolidayOffers holidayOffers = new HolidayOffers();
            holidayOffers.resetStage = apiSearchRequest.resetStage;
            Dictionary<string, string> airportCodes = Global.airportCodes;

            //IE CODE
            if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
            {
                airportCodes = Global.airportCodesMT[apiSearchRequest.countrySite.ToLower()];
            }

            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            FacetsRequestResponse facetsRequestResponse = new FacetsRequestResponse();

            string imgSourceURLFormat = Global.imgSourceURLFormat;

            Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
            StaticResponseParms staticResponseParms = new StaticResponseParms();

            List<string> redisStaticInfo = null;

            //IE CODE
            string esResultsSearch = ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetOffersIndex(apiSearchRequest) + "/_search";

            //string esResultsSearch = ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search";
            bool userPrefferedHotelKeys = false;
            bool tradingNameId576Offers = false;
            bool isTradingNameId576OffersAvailable = false;
            bool isFacetsCallRequired = false;
            int thresholdOfferLimit = int.Parse(ConfigurationManager.AppSettings["ThresholdOfferLimit"]);
            if (apiSearchRequest.boardType.Count == 1 && apiSearchRequest.boardType.Contains("5"))
            {
                thresholdOfferLimit = int.Parse(ConfigurationManager.AppSettings["AIThresholdOfferLimit"]);
            }
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                thresholdOfferLimit = apiSearchRequest.hotelKeys.Count;
            }
            if (apiSearchRequest.sort != null && apiSearchRequest.sort.Contains("pagination"))
            {
                thresholdOfferLimit = int.Parse(ConfigurationManager.AppSettings["PaginationThresholdOfferLimit"]);
            }
            List<string> offersServingOut = new List<string>();
            Dictionary<string, string> masterHotelShortDescription = new Dictionary<string, string>();

            int SHORT_DESCRIPTION_MAX_LENGTH = int.Parse(ConfigurationManager.AppSettings["ShortDescriptionLength"]);
            int offerCountConfigValue = 0;
            Int32.TryParse(ConfigurationManager.AppSettings["OfferCount"], out offerCountConfigValue);

            int tradingNameId576OffersCount = offerCountConfigValue;

            string tradingNameIdOffers = string.Empty;

            List<int> hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude;

            int tradingNameId576Count = int.Parse(ConfigurationManager.AppSettings["576TradingNameIdCount"]);

            int offerCount = 0;
            if (apiSearchRequest.hotelKeys.Count != 0)
            {
                offerCount = apiSearchRequest.hotelKeys.Count;
            }
            else if (apiSearchRequest.sort != null && apiSearchRequest.sort.Contains("pagination"))
            {
                offerCount = int.Parse(ConfigurationManager.AppSettings["PaginationOfferCount"]);
            }
            else
            {
                offerCount = offerCountConfigValue;
            }
            #region TTSS_Criteria
            string facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
            string facetsResponse = string.Empty;

            if (searchRequestAvailability.ttssCount > 0)
            {
                try
                {
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, facetsQuery);
                    facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                        
                    if (searchRequestAvailability.ttssCount >= int.Parse(ConfigurationManager.AppSettings["TTSSMaxSearchResults"]) && facetsRequestResponse.aggregations != null && facetsRequestResponse.aggregations.hotel.buckets.Count < thresholdOfferLimit)
                    {
                        ttssResultsParams = TTSSExecution.HitTTSSToGetOffers(apiSearchRequest, errorLogger);
                        isFacetsCallRequired = true;
                        // Reset Logic
                        holidayOffers.resetStage = ttssResultsParams.resetStage;
                        if (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && apiSearchRequest.hotelKeys.Count == 0 && ttssResultsParams.ttssCount == 0 && ttssResultsParams.resetStage <= 4)
                        {
                            return holidayOffers;
                        }
                    }
                    else
                    {
                        if(searchRequestAvailability.resetStage > 0)
                        {
                            apiSearchRequest = TTSSExecution.JumpToResetStage(apiSearchRequest, searchRequestAvailability.resetStage);
                            //holidayOffers.resetStage = searchRequestAvailability.resetStage;
                            isFacetsCallRequired = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1},ErrorMessage-7", ex.Message, ex.StackTrace));
                    ttssResultsParams = TTSSExecution.HitTTSSToGetOffers(apiSearchRequest, errorLogger);
                    isFacetsCallRequired = true;
                    // Reset Logic
                    holidayOffers.resetStage = ttssResultsParams.resetStage;
                    if (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && apiSearchRequest.hotelKeys.Count == 0 && ttssResultsParams.ttssCount == 0 && ttssResultsParams.resetStage <= 4)
                    {
                        return holidayOffers;
                    }
                    //ErrorLogger.Log(string.Format("Error Deserializing the es request response. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }

            }
            else
            {
                ttssResultsParams = TTSSExecution.HitTTSSToGetOffers(apiSearchRequest, errorLogger);
                isFacetsCallRequired = true;
                // Reset Logic
                holidayOffers.resetStage = ttssResultsParams.resetStage;
                if (ConfigurationManager.AppSettings["EnableResetLogicForNullResults"] == "true" && apiSearchRequest.hotelKeys.Count == 0 && ttssResultsParams.ttssCount == 0 && ttssResultsParams.resetStage <= 4)
                {
                    return holidayOffers;
                }
            }
            #endregion

            #region Offers_Logic
            holidayOffers.isOfferFound = true;
            OffersResponse userPrefferedHotelKeyOffer = new OffersResponse();
            OffersResponse tradingNameId576Offer = new OffersResponse();
            OffersResponse genericOffers = new OffersResponse();
            if (apiSearchRequest.usersPreferredHotelKeys.Count != 0)
            {
                tradingNameIdOffers = string.Join(",", apiSearchRequest.tradingNameIds.Select(n => n.ToString()).ToArray());
                string userPrefferedHotelKeyQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, 1, tradingNameIdOffers, hotelKeysToExclude, true);
                try
                {
                    string userPrefferedHotelKeyQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, userPrefferedHotelKeyQuery);
                    userPrefferedHotelKeyOffer = JsonConvert.DeserializeObject<OffersResponse>(userPrefferedHotelKeyQueryResponse);
                    if (userPrefferedHotelKeyOffer.aggregations.hotel_iff.buckets.Count != 0)
                    {
                        offerCount = offerCount - 1;
                        tradingNameId576OffersCount = tradingNameId576OffersCount - 1;
                        userPrefferedHotelKeys = true;
                        hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude.Concat(apiSearchRequest.usersPreferredHotelKeys).ToList();
                        offersServingOut.AddRange((from offerResults in userPrefferedHotelKeyOffer.aggregations.hotel_iff.buckets select offerResults.key.ToString()).Distinct().ToList());
                    }
                }
                catch (Exception ex)
                {
                    userPrefferedHotelKeys = false;
                    errorLogger.AppendLine((string.Format("Web exception occured while preparing preffered ES offers. Message - {0}, Stack Trace - {1},ErrorMessage-8", ex.Message, ex.StackTrace)));
                }
            }
            if (apiSearchRequest.sort != null && apiSearchRequest.sort.Count == 0 && !apiSearchRequest.tradingNameIds.Contains<string>("808") && apiSearchRequest.hotelKeys.Count == 0)
            {
                string tradingNameId576OfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCountConfigValue, "576", hotelKeysToExclude, false);
                string tradingNameId576OfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, tradingNameId576OfferQuery);
                try
                {
                    tradingNameId576Offer = JsonConvert.DeserializeObject<OffersResponse>(tradingNameId576OfferQueryResponse);
                    if (tradingNameId576Offer.aggregations.hotel_iff.buckets.Count > 0)
                    {
                        offerCount = offerCount - Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                        tradingNameId576OffersCount = tradingNameId576OffersCount - Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                        offersServingOut.AddRange((from offerResults in tradingNameId576Offer.aggregations.hotel_iff.buckets select offerResults.key.ToString()).Distinct().ToList());
                        tradingNameId576Offers = true;
                        if (tradingNameId576Offer.aggregations.hotel_iff.buckets.Count > tradingNameId576Count)
                        {
                            isTradingNameId576OffersAvailable = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    isTradingNameId576OffersAvailable = false;
                    errorLogger.AppendLine((string.Format("Web exception occured while preparing 576TNId ES offers. Message - {0}, Stack Trace - {1},ErrorMessage-9", ex.Message, ex.StackTrace)));
                }

                string genericOfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCount, "192", hotelKeysToExclude, false);
                string genericOfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, genericOfferQuery);
                try
                {
                    genericOffers = JsonConvert.DeserializeObject<OffersResponse>(genericOfferQueryResponse);
                    offersServingOut.AddRange((from offerResults in genericOffers.aggregations.hotel_iff.buckets select offerResults.key.ToString()).Distinct().ToList());
                    if (genericOffers.aggregations.hotel_iff.buckets.Count < offerCount)
                    {
                        offerCount = offerCount - genericOffers.aggregations.hotel_iff.buckets.Count;
                        if (isTradingNameId576OffersAvailable)
                        {
                            tradingNameId576OffersCount = Math.Min(offerCount + tradingNameId576Count, tradingNameId576OffersCount + tradingNameId576Count);
                        }
                    }
                    else
                    {
                        tradingNameId576OffersCount = Math.Min(tradingNameId576Count, tradingNameId576Offer.aggregations.hotel_iff.buckets.Count);
                    }
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("Web exception occured while preparing 192TNIds ES offers. Message - {0}, Stack Trace - {1},ErrorMessage-9", ex.Message, ex.StackTrace)));
                }
                //make both 576 and 192 queries
            }
            else
            {
                tradingNameIdOffers = string.Join(",", apiSearchRequest.tradingNameIds.Select(n => n.ToString()).ToArray());
                string genericOfferQuery = QueryBuilder.GetOffersQuery(apiSearchRequest, offerCount, tradingNameIdOffers, hotelKeysToExclude, false);
                string genericOfferQueryResponse = Utilities.ExecuteESPostJsonWebRequest(esResultsSearch, genericOfferQuery);
                try
                {
                    genericOffers = JsonConvert.DeserializeObject<OffersResponse>(genericOfferQueryResponse);
                    offersServingOut.AddRange((from offerResults in genericOffers.aggregations.hotel_iff.buckets select offerResults.key.ToString()).Distinct().ToList());
                    //prepare a generic query
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("Web exception occured while preparing Generic ES offers. Message - {0}, Stack Trace - {1},ErrorMessage-10", ex.Message, ex.StackTrace)));
                }
            }
            
            if (apiSearchRequest.isShortDescriptionRequired)
            {
                //make a bulk redis call to get the short desciption for all the serving out offers
                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHDescription"]);
                        List<string> descriptionList = redisClient.GetValues(offersServingOut);
                        for (int descriptionIndex = 0; descriptionIndex < descriptionList.Count(); descriptionIndex++)
                        {
                            MHDescription mhDescription = JsonConvert.DeserializeObject<MHDescription>(descriptionList[descriptionIndex]);

                            if (!string.IsNullOrWhiteSpace(mhDescription.MHID))
                            {
                                if (!masterHotelShortDescription.ContainsKey(mhDescription.MHID))
                                {
                                    masterHotelShortDescription.Add(mhDescription.MHID, mhDescription.description);
                                }
                            }
                        }
                    }
                }
            }
            #endregion
            // Redis Call for fetching the Hotel Static Info
            using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
            {
                using (IRedisClient redisClient = pooledClientManager.GetClient())
                {
                    redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                    redisStaticInfo = redisClient.GetValues(offersServingOut);
                    for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                    {
                        staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                        if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                        {
                            staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                        }
                    }
                }
            }
            holidayOffers.offers = new List<HOffer>();
            #region UserPrefferedHotelKeyOffers
            int pax = apiSearchRequest.adults + apiSearchRequest.children;
            bool PlatinumCallRoutingEnabled = bool.Parse(ConfigurationManager.AppSettings["PlatinumCallRoutingEnabled"]);
            if (userPrefferedHotelKeys)
            {
                foreach (ORPHotelBucket bucket in userPrefferedHotelKeyOffer.aggregations.hotel_iff.buckets)
                {
                    try
                    {
                        HOffer offer = new HOffer();
                        offer.rank = bucket.top_price.hits.hits[0]._source.rank;
                        offer.accommodationUpdateTime = bucket.top_price.hits.hits[0]._source.accommodationUpdateTime;
                        offer.id = bucket.top_price.hits.hits[0]._source.id;
                        offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                        offer.price = bucket.top_price.hits.hits[0]._source.price;
                        string phoneNumber = string.Empty;

                        //IE CODE
                        if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_UK)
                        {
                            if (PlatinumCallRoutingEnabled)
                            {
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                }
                            }
                            else
                            {
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                }
                            }
                        }
                        else
                        {
                            phoneNumber = GetTelephoneNumberMT(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = GetTelephoneNumberByLabelIDMT(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            }

                        }

                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = "0123456789";
                        }
                        offer.phone = phoneNumber;
                        Journey journey = new Journey();
                        journey.outboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                        journey.outboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                        journey.outboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportCode;
                        journey.outboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                        journey.outboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                        journey.outboundLegCount = bucket.top_price.hits.hits[0]._source.journey.outboundLegCount;
                        journey.outboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDuration;
                        journey.outboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO;
                        try
                        {
                            if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO))
                            {
                                journey.outboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO];
                            }
                            else
                            {
                                journey.outboundAirlines = string.Empty;
                            }
                        }
                        catch
                        {
                            journey.outboundAirlines = string.Empty;
                        }
                        journey.outboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.outboundFlightNumber;
                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        journey.inboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalDate;
                        journey.inboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportName;
                        journey.inboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportCode;
                        journey.inboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportName;
                        journey.inboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportCode;
                        journey.inboundLegCount = bucket.top_price.hits.hits[0]._source.journey.inboundLegCount;
                        journey.inboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDuration;
                        journey.inboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO;
                        try
                        {
                            if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO))
                            {
                                journey.inboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO];
                            }
                            else
                            {
                                journey.inboundAirlines = string.Empty;
                            }
                        }
                        catch
                        {
                            journey.inboundAirlines = string.Empty;
                        }
                        journey.inboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.inboundFlightNumber;
                        journey.adultBagPrice = bucket.top_price.hits.hits[0]._source.journey.adultBagPrice;
                        journey.childBagPrice = bucket.top_price.hits.hits[0]._source.journey.childBagPrice;
                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;

                        // Changes for app --> start
                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        // Changes for app --> end

                        offer.journey = journey;
                        Accommodation accommodation = new Accommodation();
                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                        offer.accommodation = accommodation;
                        OfferHotel hotel = new OfferHotel();
                        hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                        hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                        hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                        WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                        rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                        rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                        rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                        hotel.rating = rating;
                        hotel.mobileimages = new List<string>();
                        hotel.thumbnailimages = new List<string>();
                        hotel.images = new List<string>();
                        int hoteliffCount = 0;
                        if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                        {
                            hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                            if (hoteliffCount == 0)
                            {
                                hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                            }
                            else
                            {
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                        }
                        else
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                        }

                        // Short Description call
                        if (apiSearchRequest.isShortDescriptionRequired)
                        {
                            if (Global.mhidToLandingPageURL.ContainsKey(hotel.iff))
                            {
                                hotel.landingPageURL = Global.mhidToLandingPageURL[hotel.iff];
                            }
                            else
                            {
                                hotel.landingPageURL = string.Empty;// "/hsm-canarios-park-hotel-calas-de-mallorca-majorca-balearic-islands";
                            }
                            if (!string.IsNullOrWhiteSpace(hotel.iff) && masterHotelShortDescription.ContainsKey(hotel.iff))
                            {
                                string tempDesc = masterHotelShortDescription[hotel.iff];
                                tempDesc = Regex.Replace(tempDesc, "<h3>.*?</h3>", String.Empty).Trim();
                                tempDesc = Regex.Replace(tempDesc, "<.*?>", String.Empty).Trim();
                                if (tempDesc.Length > SHORT_DESCRIPTION_MAX_LENGTH)
                                {
                                    hotel.shortDescription = tempDesc.Substring(0, SHORT_DESCRIPTION_MAX_LENGTH);
                                }
                                else
                                {
                                    hotel.shortDescription = tempDesc.Substring(0, tempDesc.Length);
                                }
                            }
                            else
                            {
                                hotel.shortDescription = string.Empty;
                            }
                        }
                        else
                        {
                            hotel.shortDescription = string.Empty;
                        }

                        offer.hotel = hotel;
                        offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                        offer.isPreferredByUser = true;
                        if (apiSearchRequest.isDirectionalSellingValue)
                        {
                            Dictionary<int, string> MhidsWithComments = new Dictionary<int, string>(apiSearchRequest.mhidsWithComments);
                            if (MhidsWithComments.ContainsKey(int.Parse(hotel.iff)))
                            {

                                offer.comment = MhidsWithComments[int.Parse(hotel.iff)];
                            }
                        }
                        if (offer.tradingNameId == "576")
                        {
                            offer.isPreferential = true;
                        }
                        else
                        {
                            offer.isPreferential = false;
                        }

                        if (staticInfo.ContainsKey(Convert.ToInt32(offer.hotel.iff)))
                        {
                            StaticResponseParms staticHotelInfoRecord = staticInfo[Convert.ToInt32(offer.hotel.iff)];

                            if (staticHotelInfoRecord.errataData != null)
                            {
                                offer.errataData = staticHotelInfoRecord.errataData;
                            }
                            else
                            {
                                offer.errataData = new List<ErrataInfo>();
                            }

                        }
                        else
                        {
                            offer.errataData = new List<ErrataInfo>();
                        }

                        holidayOffers.offers.Add(offer);
                    }
                    catch (Exception ex)
                    {
                        errorLogger.AppendLine((string.Format("Web exception occured while preparing Preffered offers. Message - {0}, Stack Trace - {1},ErrorMessage-11", ex.Message, ex.StackTrace)));
                    }
                }

            }
            #endregion

            #region TradingNameId576Offers            
            if (tradingNameId576Offers)
            {
                List<ORPHotelBucket> limitedList = tradingNameId576Offer.aggregations.hotel_iff.buckets.Take(tradingNameId576OffersCount).ToList();

                foreach (ORPHotelBucket bucket in limitedList)
                {
                    try
                    {
                        HOffer offer = new HOffer();
                        offer.rank = bucket.top_price.hits.hits[0]._source.rank;
                        offer.accommodationUpdateTime = bucket.top_price.hits.hits[0]._source.accommodationUpdateTime;
                        offer.id = bucket.top_price.hits.hits[0]._source.id;
                        offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                        offer.price = bucket.top_price.hits.hits[0]._source.price;
                        string phoneNumber = string.Empty;

                        //IE CODE 
                        if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_UK)
                        {
                            if (PlatinumCallRoutingEnabled)
                            {
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                }
                            }
                            else
                            {
                                phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                if (string.IsNullOrWhiteSpace(phoneNumber))
                                {
                                    phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                                }
                            }
                        }
                        else
                        {
                            phoneNumber = GetTelephoneNumberMT(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = GetTelephoneNumberByLabelIDMT(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            }

                        }

                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = "0123456789";
                        }
                        offer.phone = phoneNumber;
                        Journey journey = new Journey();
                        journey.outboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                        journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                        journey.outboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                        journey.outboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportCode;
                        journey.outboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                        journey.outboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                        journey.outboundLegCount = bucket.top_price.hits.hits[0]._source.journey.outboundLegCount;
                        journey.outboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDuration;
                        journey.outboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO;
                        try
                        {
                            if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO))
                            {
                                journey.outboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO];
                            }
                            else
                            {
                                journey.outboundAirlines = string.Empty;
                            }
                        }
                        catch
                        {
                            journey.outboundAirlines = string.Empty;
                        }

                        journey.outboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.outboundFlightNumber;
                        journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        journey.inboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalDate;
                        journey.inboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportName;
                        journey.inboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportCode;
                        journey.inboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportName;
                        journey.inboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportCode;
                        journey.inboundLegCount = bucket.top_price.hits.hits[0]._source.journey.inboundLegCount;
                        journey.inboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDuration;
                        journey.inboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO;
                        try
                        {
                            if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO))
                            {
                                journey.inboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO];
                            }
                            else
                            {
                                journey.inboundAirlines = string.Empty;
                            }
                        }
                        catch
                        {
                            journey.inboundAirlines = string.Empty;
                        }
                        journey.inboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.inboundFlightNumber;
                        journey.adultBagPrice = bucket.top_price.hits.hits[0]._source.journey.adultBagPrice;
                        journey.childBagPrice = bucket.top_price.hits.hits[0]._source.journey.childBagPrice;
                        journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                        journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                        journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                        journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                        journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                        journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;

                        // Changes for app --> start
                        journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                        journey.departure = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                        journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                        journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                        journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                        // Changes for app --> end

                        offer.journey = journey;
                        Accommodation accommodation = new Accommodation();
                        accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                        accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                        accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                        accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                        accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                        offer.accommodation = accommodation;
                        OfferHotel hotel = new OfferHotel();
                        hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                        hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                        hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                        WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                        rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                        rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                        rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                        hotel.rating = rating;
                        hotel.mobileimages = new List<string>();
                        hotel.thumbnailimages = new List<string>();
                        hotel.images = new List<string>();
                        int hoteliffCount = 0;
                        if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                        {
                            hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                            if (hoteliffCount == 0)
                            {
                                hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                                hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                            }
                            else
                            {
                                for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                                {
                                    hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                    hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                }
                            }
                        }
                        else
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }

                        // Short Description Call
                        if (apiSearchRequest.isShortDescriptionRequired)
                        {
                            if (Global.mhidToLandingPageURL.ContainsKey(hotel.iff))
                            {
                                hotel.landingPageURL = Global.mhidToLandingPageURL[hotel.iff];
                            }
                            else
                            {
                                hotel.landingPageURL = string.Empty;//"/hsm-canarios-park-hotel-calas-de-mallorca-majorca-balearic-islands";
                            }
                            if (!string.IsNullOrWhiteSpace(hotel.iff) && masterHotelShortDescription.ContainsKey(hotel.iff))
                            {
                                string tempDesc = masterHotelShortDescription[hotel.iff];
                                tempDesc = Regex.Replace(tempDesc, "<h3>.*?</h3>", String.Empty).Trim();
                                tempDesc = Regex.Replace(tempDesc, "<.*?>", String.Empty).Trim();
                                if (tempDesc.Length > SHORT_DESCRIPTION_MAX_LENGTH)
                                {
                                    hotel.shortDescription = tempDesc.Substring(0, SHORT_DESCRIPTION_MAX_LENGTH);
                                }
                                else
                                {
                                    hotel.shortDescription = tempDesc.Substring(0, tempDesc.Length);
                                }
                            }
                            else
                            {
                                hotel.shortDescription = string.Empty;
                            }
                        }
                        else
                        {
                            hotel.shortDescription = string.Empty;
                        }

                        offer.hotel = hotel;
                        offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                        offer.isPreferential = true;
                        offer.isPreferredByUser = false;
                        if (apiSearchRequest.isDirectionalSellingValue)
                        {
                            Dictionary<int, string> MhidsWithComments = new Dictionary<int, string>(apiSearchRequest.mhidsWithComments);
                            if (MhidsWithComments.ContainsKey(int.Parse(hotel.iff)))
                            {

                                offer.comment = MhidsWithComments[int.Parse(hotel.iff)];
                            }
                        }

                        if (staticInfo.ContainsKey(Convert.ToInt32(offer.hotel.iff)))
                        {
                            StaticResponseParms staticHotelInfoRecord = staticInfo[Convert.ToInt32(offer.hotel.iff)];

                            if (staticHotelInfoRecord.errataData != null)
                            {
                                offer.errataData = staticHotelInfoRecord.errataData;
                            }
                            else
                            {
                                offer.errataData = new List<ErrataInfo>();
                            }

                        }
                        else
                        {
                            offer.errataData = new List<ErrataInfo>();
                        }

                        holidayOffers.offers.Add(offer);
                    }
                    catch (Exception ex)
                    {
                        errorLogger.AppendLine((string.Format("Web exception occured while preparing 576TNIds offers. Message - {0}, Stack Trace - {1},ErrorMessage-12", ex.Message, ex.StackTrace)));
                    }
                }
            }

            #endregion

            #region GenericOffers            
            foreach (ORPHotelBucket bucket in genericOffers.aggregations.hotel_iff.buckets)
            {
                try
                {
                    HOffer offer = new HOffer();
                    offer.rank = bucket.top_price.hits.hits[0]._source.rank;
                    offer.accommodationUpdateTime = bucket.top_price.hits.hits[0]._source.accommodationUpdateTime;
                    offer.id = bucket.top_price.hits.hits[0]._source.id;
                    offer.quoteRef = bucket.top_price.hits.hits[0]._source.quoteRef;
                    offer.price = bucket.top_price.hits.hits[0]._source.price;
                    string phoneNumber = string.Empty;

                    //IE CODE 
                    if (apiSearchRequest.countrySite == Global.COUNTRY_SITE_UK)
                    {
                        if (PlatinumCallRoutingEnabled)
                        {
                            phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (((pax >= 4) && (offer.price * pax > int.Parse(ConfigurationManager.AppSettings["PricePerPaxPlatinum"]))) ? "192_platinum" : (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192"), bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            }
                        }
                        else
                        {
                            phoneNumber = GetTelephoneNumber(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            if (string.IsNullOrWhiteSpace(phoneNumber))
                            {
                                phoneNumber = GetTelephoneNumberByLabelID(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                            }
                        }
                    }
                    else
                    {
                        phoneNumber = GetTelephoneNumberMT(bucket.top_price.hits.hits[0]._source.regionId, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                        if (string.IsNullOrWhiteSpace(phoneNumber))
                        {
                            phoneNumber = GetTelephoneNumberByLabelIDMT(apiSearchRequest.destinationIds, (apiSearchRequest.tradingNameIds != null && apiSearchRequest.tradingNameIds.Count() > 0) ? apiSearchRequest.tradingNameIds.FirstOrDefault() : "192", bucket.top_price.hits.hits[0]._source.price, apiSearchRequest.platform);
                        }

                    }

                    if (string.IsNullOrWhiteSpace(phoneNumber))
                    {
                        phoneNumber = "0123456789";
                    }
                    offer.phone = phoneNumber;
                    Journey journey = new Journey();
                    journey.outboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                    journey.outboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalDate;
                    journey.outboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                    journey.outboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportCode;
                    journey.outboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                    journey.outboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                    journey.outboundLegCount = bucket.top_price.hits.hits[0]._source.journey.outboundLegCount;
                    journey.outboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.outboundFlightDuration;
                    journey.outboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO;
                    try
                    {
                        if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO))
                        {
                            journey.outboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.outboundAirlineICAO];
                        }
                        else
                        {
                            journey.outboundAirlines = string.Empty;
                        }
                    }
                    catch
                    {
                        journey.outboundAirlines = string.Empty;
                    }
                    journey.outboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.outboundFlightNumber;
                    journey.inboundDepartureDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                    journey.inboundArrivalDate = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalDate;
                    journey.inboundDepartureAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportName;
                    journey.inboundDepartureAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureAirportCode;
                    journey.inboundArrivalAirportName = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportName;
                    journey.inboundArrivalAirportCode = bucket.top_price.hits.hits[0]._source.journey.inboundArrivalAirportCode;
                    journey.inboundLegCount = bucket.top_price.hits.hits[0]._source.journey.inboundLegCount;
                    journey.inboundFlightDuration = bucket.top_price.hits.hits[0]._source.journey.inboundFlightDuration;
                    journey.inboundAirlineICAO = bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO;
                    try
                    {
                        if (Global.ICAOAirlinesMapping.ContainsKey(bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO))
                        {
                            journey.inboundAirlines = Global.ICAOAirlinesMapping[bucket.top_price.hits.hits[0]._source.journey.inboundAirlineICAO];
                        }
                        else
                        {
                            journey.inboundAirlines = string.Empty;
                        }
                    }
                    catch
                    {
                        journey.inboundAirlines = string.Empty;
                    }
                    journey.inboundFlightNumber = bucket.top_price.hits.hits[0]._source.journey.inboundFlightNumber;
                    journey.adultBagPrice = bucket.top_price.hits.hits[0]._source.journey.adultBagPrice;
                    journey.childBagPrice = bucket.top_price.hits.hits[0]._source.journey.childBagPrice;
                    journey.duration = bucket.top_price.hits.hits[0]._source.journey.duration;
                    journey.destination = bucket.top_price.hits.hits[0]._source.journey.destination;
                    journey.glat = bucket.top_price.hits.hits[0]._source.journey.glat;
                    journey.glong = bucket.top_price.hits.hits[0]._source.journey.glong;
                    journey.parentRegion = bucket.top_price.hits.hits[0]._source.journey.parentRegion;
                    journey.parentRegionId = bucket.top_price.hits.hits[0]._source.journey.parentRegionId;

                    // Changes for app --> start
                    journey.departureDate = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                    journey.departure = bucket.top_price.hits.hits[0]._source.journey.outboundDepartureAirportName;
                    journey.gatewayCode = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportCode;
                    journey.gatewayName = bucket.top_price.hits.hits[0]._source.journey.outboundArrivalAirportName;
                    journey.returnDate = bucket.top_price.hits.hits[0]._source.journey.inboundDepartureDate;
                    // Changes for app --> end

                    offer.journey = journey;
                    Accommodation accommodation = new Accommodation();
                    accommodation.boardTypeCode = bucket.top_price.hits.hits[0]._source.accommodation.boardTypeCode;
                    accommodation.adults = bucket.top_price.hits.hits[0]._source.accommodation.adults;
                    accommodation.children = bucket.top_price.hits.hits[0]._source.accommodation.children;
                    accommodation.rating = bucket.top_price.hits.hits[0]._source.accommodation.rating;
                    accommodation.updated = bucket.top_price.hits.hits[0]._source.accommodation.updated;
                    offer.accommodation = accommodation;
                    OfferHotel hotel = new OfferHotel();
                    hotel.iff = bucket.top_price.hits.hits[0]._source.hotel.iff.ToString();
                    hotel.features = bucket.top_price.hits.hits[0]._source.hotel.features;
                    hotel.name = bucket.top_price.hits.hits[0]._source.hotel.name;
                    WCFRestService.Model.ElasticSearchV2.Rating rating = new WCFRestService.Model.ElasticSearchV2.Rating();
                    rating.tripAdvisorId = bucket.top_price.hits.hits[0]._source.hotel.rating.tripAdvisorId;
                    rating.averageRating = bucket.top_price.hits.hits[0]._source.hotel.rating.averageRating / 10;
                    rating.reviewCount = bucket.top_price.hits.hits[0]._source.hotel.rating.reviewCount;
                    hotel.rating = rating;
                    hotel.mobileimages = new List<string>();
                    hotel.thumbnailimages = new List<string>();
                    hotel.images = new List<string>();
                    int hoteliffCount = 0;
                    if (Global.mhidtoImageCountList.ContainsKey(hotel.iff))
                    {
                        hoteliffCount = Global.mhidtoImageCountList[hotel.iff];
                        if (hoteliffCount == 0)
                        {
                            hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                            hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                        }
                        else
                        {
                            for (int hotelIndex = 1; hotelIndex <= hoteliffCount; hotelIndex++)
                            {
                                hotel.images.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                hotel.mobileimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopMobileImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                                hotel.thumbnailimages.Add(string.Format(imgSourceURLFormat, ConfigurationManager.AppSettings["CompressedDesktopThumbnailImageResolution"] + "/" + hotel.iff + "/" + hotelIndex + ".jpg"));
                            }
                        }
                    }
                    else
                    {
                        hotel.thumbnailimages.Add("https://resources.teletextholidays.co.uk/mob/noimage.jpg");
                        hotel.mobileimages.Add("https://resources.teletextholidays.co.uk/mob/noImage205286.jpg");
                    }
                    // Short Description call
                    if (apiSearchRequest.isShortDescriptionRequired)
                    {
                        if (Global.mhidToLandingPageURL.ContainsKey(hotel.iff))
                        {
                            hotel.landingPageURL = Global.mhidToLandingPageURL[hotel.iff];
                        }
                        else
                        {
                            hotel.landingPageURL = string.Empty;//"/hsm-canarios-park-hotel-calas-de-mallorca-majorca-balearic-islands";
                        }
                        if (!string.IsNullOrWhiteSpace(hotel.iff) && masterHotelShortDescription.ContainsKey(hotel.iff))
                        {
                            string tempDesc = masterHotelShortDescription[hotel.iff];
                            tempDesc = Regex.Replace(tempDesc, "<h3>.*?</h3>", String.Empty).Trim();
                            tempDesc = Regex.Replace(tempDesc, "<.*?>", String.Empty).Trim();
                            if (tempDesc.Length > SHORT_DESCRIPTION_MAX_LENGTH)
                            {
                                hotel.shortDescription = tempDesc.Substring(0, SHORT_DESCRIPTION_MAX_LENGTH);
                            }
                            else
                            {
                                hotel.shortDescription = tempDesc.Substring(0, tempDesc.Length);
                            }
                        }
                        else
                        {
                            hotel.shortDescription = string.Empty;
                        }
                    }
                    else
                    {
                        hotel.shortDescription = string.Empty;
                    }

                    offer.hotel = hotel;
                    offer.tradingNameId = bucket.top_price.hits.hits[0]._source.tradingNameId[0].ToString();
                    offer.isPreferredByUser = false;
                    if (apiSearchRequest.isDirectionalSellingValue)
                    {
                        Dictionary<int, string> MhidsWithComments = new Dictionary<int, string>(apiSearchRequest.mhidsWithComments);
                        if (MhidsWithComments.ContainsKey(int.Parse(hotel.iff)))
                        {

                            offer.comment = MhidsWithComments[int.Parse(hotel.iff)];
                        }
                    }

                    if (offer.tradingNameId == "576")
                    {
                        offer.isPreferential = true;
                    }
                    else
                    {
                        offer.isPreferential = false;
                    }

                    if (staticInfo.ContainsKey(Convert.ToInt32(offer.hotel.iff)))
                    {
                        StaticResponseParms staticHotelInfoRecord = staticInfo[Convert.ToInt32(offer.hotel.iff)];

                        if (staticHotelInfoRecord.errataData != null)
                        {
                            offer.errataData = staticHotelInfoRecord.errataData;
                        }
                        else
                        {
                            offer.errataData = new List<ErrataInfo>();
                        }

                    }
                    else
                    {
                        offer.errataData = new List<ErrataInfo>();
                    }

                    holidayOffers.offers.Add(offer);
                }
                catch (Exception ex)
                {
                    errorLogger.AppendLine((string.Format("Web exception occured while preparing Generic offers. Message - {0}, Stack Trace - {1},ErrorMessage-13", ex.Message, ex.StackTrace)));
                }
            }
            #endregion
            
            if(holidayOffers.offers.Count == 0 && holidayOffers.resetStage > -1)
            {
                holidayOffers.resetStage = -1;
            }
            #region Facets
            if (!apiSearchRequest.isDirectionalSellingValue)
            {
                if (isFacetsCallRequired)
                {
                    facetsQuery = QueryBuilder.GetFacetsQuery(apiSearchRequest);
                    //facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search", facetsQuery);
                    //IE CODE
                    facetsResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetOffersIndex(apiSearchRequest) + "/_search", facetsQuery);

                    facetsRequestResponse = JsonConvert.DeserializeObject<FacetsRequestResponse>(facetsResponse);
                }
                holidayOffers.totalHotels = facetsRequestResponse.aggregations.hotel.buckets.Count;
                holidayOffers.facets = new Facets();
                holidayOffers.facets.priceRange = new Pricerange();
                holidayOffers.facets.priceRange.min = Convert.ToInt32(facetsRequestResponse.aggregations.price_min.value);
                holidayOffers.facets.priceRange.max = Convert.ToInt32(facetsRequestResponse.aggregations.price_max.value);
                holidayOffers.facets.airports = new List<Airport>();
                foreach (FRRAirportBucket airportBucket in facetsRequestResponse.aggregations.airports.buckets)
                {
                    try
                    {
                        Airport airport = new Airport();
                        airport.count = airportBucket.NoOfAirports.value;
                        airport.departureId = airportBucket.key;
                        airport.name = airportCodes[airportBucket.key.ToString()];
                        holidayOffers.facets.airports.Add(airport);
                    }
                    catch (Exception ex)
                    {

                    }
                }
                holidayOffers.facets.hotels = new List<FacetHotel>();

                /* Get the Hotel Names for the Facets Call - Starts Here */
                ////Dictionary<int, StaticResponseParms> staticInfo = new Dictionary<int, StaticResponseParms>();
                ////StaticResponseParms staticResponseParms = new StaticResponseParms();

                ////List<string> redisStaticInfo = null;
                var distinctHotelKeys = (from facetsResult in facetsRequestResponse.aggregations.hotel.buckets select facetsResult.key.ToString()).Distinct().ToList();

                using (PooledRedisClientManager pooledClientManager = new PooledRedisClientManager(Global.RedisConnectionStringForRoutingApi))
                {
                    using (IRedisClient redisClient = pooledClientManager.GetClient())
                    {
                        redisClient.Db = Convert.ToInt32(ConfigurationManager.AppSettings["RedisDBForMHStaticInfo"]);
                        redisStaticInfo = redisClient.GetValues(distinctHotelKeys);
                        for (int mhidCount = 0; mhidCount < redisStaticInfo.Count; mhidCount++)
                        {
                            staticResponseParms = JsonConvert.DeserializeObject<StaticResponseParms>(redisStaticInfo[mhidCount]);
                            if (!staticInfo.ContainsKey(staticResponseParms.mhid))
                            {
                                staticInfo.Add(staticResponseParms.mhid, staticResponseParms);
                            }
                        }
                    }
                }

                /* Get the Hotel Names for the Facets Call - Ends Here */
                foreach (FRRHotelBucket hotelBucket in facetsRequestResponse.aggregations.hotel.buckets)
                {
                    FacetHotel facetHotel = new FacetHotel();
                    facetHotel.count = hotelBucket.NoOfHotels.value;
                    facetHotel.hotelKey = hotelBucket.key.ToString();

                    if(staticInfo.ContainsKey(hotelBucket.key))
                    {
                        facetHotel.name = staticInfo[hotelBucket.key].name;
                        if (Global.MHIDtoRegIDMap.ContainsKey(hotelBucket.key.ToString()))
                        {
                            facetHotel.destinationId = Global.MHIDtoRegIDMap[hotelBucket.key.ToString()];
                        }
                        holidayOffers.facets.hotels.Add(facetHotel);
                    }
                }
                holidayOffers.facets.resorts = new List<Resort>();
                foreach (FRRResortsBucket resortBucket in facetsRequestResponse.aggregations.resorts.buckets)
                {
                    Resort resort = new Resort();
                    resort.count = resortBucket.NoOfResorts.value;
                    resort.destinationId = Convert.ToInt32(resortBucket.key);
                    if (Global.RegIDtoRegNameMap.ContainsKey(resort.destinationId))
                    {
                        resort.name = Global.RegIDtoRegNameMap[resort.destinationId];
                        holidayOffers.facets.resorts.Add(resort);
                    }
                }
            }
            string searchUrl = QueryBuilder.GetSearchURL(apiSearchRequest);

            #endregion

            #region  Async_call_to_insert_in_request_index
            Task.Factory.StartNew(() =>
            {
                SearchRequestParams searchRequestParams = new SearchRequestParams();
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                Dictionary<int, string> childAirportCodes = Global.childAirportCodes;
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;

                //IE CODE
                //TODO: Move all tenant specfic json retrieval to a helper
                if (apiSearchRequest.countrySite != Global.COUNTRY_SITE_UK)
                {
                    parentAirportCodes = Global.parentAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
                    childAirportCodes = Global.childAirportCodesMT[apiSearchRequest.countrySite.ToLower()];
                }

                searchRequestParams.os = apiSearchRequest.os;
                searchRequestParams.platform = apiSearchRequest.platform;
                searchRequestParams.offersReturned = holidayOffers.offers.Count;
                searchRequestParams.searchURL = searchUrl;
                searchRequestParams.adults = apiSearchRequest.adults;
                searchRequestParams.boardType = apiSearchRequest.boardType;
                searchRequestParams.children = apiSearchRequest.children;
                searchRequestParams.departureDate = apiSearchRequest.departureDate.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.dateMin = apiSearchRequest.dateMin.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.dateMax = apiSearchRequest.dateMax.ToString("yyyy-MM-ddT23:59:59");
                searchRequestParams.durationMin = apiSearchRequest.durationMin;
                searchRequestParams.durationMax = apiSearchRequest.durationMax;
                searchRequestParams.departureIds = apiSearchRequest.departureIds;
                searchRequestParams.airportIds = new List<string>();
                if (apiSearchRequest.departureIds < 0)
                {
                    searchRequestParams.airportIds = childAirportCodes[apiSearchRequest.departureIds].Split(',').ToList();
                    searchRequestParams.parentDepartureIds = apiSearchRequest.departureIds;
                }
                else
                {
                    searchRequestParams.parentDepartureIds = parentAirportCodes[apiSearchRequest.departureIds];
                    searchRequestParams.airportIds.Add(apiSearchRequest.departureIds.ToString());
                }
                searchRequestParams.priceMax = apiSearchRequest.priceMax;
                searchRequestParams.priceMin = apiSearchRequest.priceMin;
                searchRequestParams.ttssResponseTime = ttssResultsParams.ttssResponseTime;
                searchRequestParams.regionIds = new List<string>();
                if (apiSearchRequest.destinationType.ToLower() == "region")
                {
                    searchRequestParams.destinationIds = apiSearchRequest.labelId;
                    searchRequestParams.labelId = apiSearchRequest.labelId;
                    searchRequestParams.destinationType = "Region";
                    searchRequestParams.isRegion = true;
                    searchRequestParams.regionIds.Add(apiSearchRequest.destinationIds.ToString());
                }
                else
                {
                    searchRequestParams.destinationIds = apiSearchRequest.destinationIds;
                    searchRequestParams.labelId = apiSearchRequest.destinationIds;
                    searchRequestParams.destinationType = "Label";
                    searchRequestParams.isRegion = false;
                    searchRequestParams.regionIds = parentRegionIds[apiSearchRequest.destinationIds.ToString()].Split(',').ToList();
                }
                searchRequestParams.taRating = apiSearchRequest.tripAdvisorRating;
                searchRequestParams.channelId = apiSearchRequest.channelId;
                if (!string.IsNullOrWhiteSpace(ttssResultsParams.ttssURL))
                {
                    searchRequestParams.isCacheHit = false;
                }
                else
                {
                    searchRequestParams.isCacheHit = true;
                }
                searchRequestParams.ratings = apiSearchRequest.ratings;
                searchRequestParams.tradingNameIds = apiSearchRequest.tradingNameIds;
                searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.ttssURL = ttssResultsParams.ttssURL;
                searchRequestParams.indexingTime = ttssResultsParams.indexingTime;
                searchRequestParams.decoratingTime = ttssResultsParams.decoratingTime;
                searchRequestParams.offersInserted = ttssResultsParams.offersInserted;
                searchRequestParams.offersFound = ttssResultsParams.offersFound;
                searchRequestParams.ttssCount = ttssResultsParams.ttssCount;
                searchRequestParams.facetsTrails = ttssResultsParams.facetsTrials;
                searchRequestParams.offersDecorated = ttssResultsParams.offersDecorated;
                searchRequestParams.hotelKeys = apiSearchRequest.hotelKeys;
                if (apiSearchRequest.hotelKeys.Count > 0)
                {
                    searchRequestParams.isHotelKeys = true;
                }
                else
                {
                    searchRequestParams.isHotelKeys = false;
                }
                searchRequestParams.usersPreferredHotelKeys = apiSearchRequest.usersPreferredHotelKeys;
                searchRequestParams.hotelKeysToExclude = apiSearchRequest.hotelKeysToExclude;
                searchRequestParams.skipFacets = apiSearchRequest.skipFacets;
                searchRequestParams.endPoint = "search";
                if (apiSearchRequest.sort != null && apiSearchRequest.sort.Count != 0)
                {
                    searchRequestParams.sort = apiSearchRequest.sort;
                }
                else
                {
                    searchRequestParams.sort = new List<string>();
                    searchRequestParams.sort.Add("price");
                }
                searchRequestParams.longDateRangeQuery = apiSearchRequest.longDateRangeQuery;
                searchRequestParams.cacheHotTime = apiSearchRequest.cacheHotTime;
                searchRequestParams.navigatedPage = apiSearchRequest.navigatedPage;
                searchRequestParams.pageSource = apiSearchRequest.pageSource;
                searchRequestParams.isBulkInsertionOnRetry = ttssResultsParams.isBulkInsertionOnRetry;
                searchRequestParams.resetStage = apiSearchRequest.resetStage;
                try
                {
                    //HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);

                    //IE CODE
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetRequestIndex(apiSearchRequest) + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);

                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLogger.Log(string.Format("Web exception occured while inserting Search call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting Search call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }

            });
            #endregion
            return holidayOffers;
        }

        public static WCFRestService.Model.Misc.Diversity GenerateDiversityOffers(APIDiversityRequest apiDiversityRequest, StringBuilder errorLogger)
        {
            bool hitTTSS = false;
            Diversity diversity = new Diversity();
            string diversityUrl = string.Empty;
            TTSSResultsParams ttssResultsParams = new TTSSResultsParams();
            string diversityRequestQuery = string.Empty;
            diversityRequestQuery = QueryBuilder.GetDiversityRequestQuery(apiDiversityRequest);
            //esDiversityQuery = Utilities.DiversitySearchQuery(destinationId, departureIds, dateMin, dateMax, durationMin, durationMax, adults, children, ratings, hotelKeys, tradingNameIds);

            //IE CODE
            var ESDSearchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetRequestIndex(apiDiversityRequest.countrySite) + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"] + "/_search", diversityRequestQuery);

            //var ESDSearchResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["DiversityIndiceDataType"] + "/_search", diversityRequestQuery);
            SearchRequestResponse esDSR = new SearchRequestResponse();
            try
            {
                esDSR = JsonConvert.DeserializeObject<SearchRequestResponse>(ESDSearchResponse);
            }
            catch (Exception ex)
            {
                hitTTSS = true;
                errorLogger.AppendLine((string.Format("Diversity Exception. Message - {0}, Stack Trace - {1},ErrorMessage-14", ex.Message, ex.StackTrace)));
            }
            if (esDSR.hits.total == 0 || hitTTSS)
            {
                ttssResultsParams = TTSSExecution.HitTTSSToGetDiversityOffers(apiDiversityRequest, errorLogger);
            }
            string diversityQuery = string.Empty;
            diversityQuery = QueryBuilder.GetDiversityQuery(apiDiversityRequest);
            //var diversityOffersResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["OffersIndice"] + "/_search", diversityQuery);

            //IE CODE
            var diversityOffersResponse = Utilities.ExecuteESPostJsonWebRequest(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetOffersIndex(apiDiversityRequest.countrySite) + "/_search", diversityQuery);

            DiversityResponseParams.DiversityResponse diversityResponse = new DiversityResponseParams.DiversityResponse();
            try
            {
                diversityResponse = JsonConvert.DeserializeObject<DiversityResponseParams.DiversityResponse>(diversityOffersResponse);
                diversity.destinationId = apiDiversityRequest.destinationId;
                diversity.hotels = new List<DiversityHotel>();
                foreach (DiversityResponseParams.IFFBucket divHotels in diversityResponse.aggregations.group_by_iff.buckets)
                {
                    DiversityHotel diversityHotel = new DiversityHotel();
                    diversityHotel.hotelKey = divHotels.key.ToString();
                    DiversityOffer divOffer = new DiversityOffer();
                    diversityHotel.offers = new List<DiversityOffer>();
                    foreach (DiversityResponseParams.BoardTypeBucket boardType in divHotels.group_by_category.buckets)
                    {
                        DiversityOffer diversityOffer = new DiversityOffer();
                        diversityOffer.boardTypeId = boardType.key;
                        diversityOffer.price = boardType.min_price.value;
                        diversityOffer.quoteRef = boardType.top_price.hits.hits[0]._source.quoteRef;
                        diversityOffer.date = boardType.top_price.hits.hits[0]._source.journey.outboundDepartureDate;
                        //diversityOffer.date = DateTime.ParseExact(boardType.top_price.hits.hits[0]._source.journey.departureDate, "yyyy-MM-dd'T'HH:mm:sszzz", CultureInfo.InvariantCulture);
                        diversityHotel.offers.Add(diversityOffer);
                    }
                    diversity.hotels.Add(diversityHotel);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(string.Format("Diversity Message: {0} & StackTrace; {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
            }
            diversityUrl = QueryBuilder.GetDiversityURL(apiDiversityRequest);
            Task.Factory.StartNew(() =>
            {
                SearchRequestParams searchRequestParams = new SearchRequestParams();
                Dictionary<int, int> parentAirportCodes = Global.parentAirportCodes;
                Dictionary<int, string> childAirportCodes = Global.childAirportCodes;
                Dictionary<string, string> parentRegionIds = Global.labelRegionMappingList;

                //IE CODE
                //TODO: Move all tenant specfic json retrieval to a helper
                if (apiDiversityRequest.countrySite != Global.COUNTRY_SITE_UK)
                {
                    parentAirportCodes = Global.parentAirportCodesMT[apiDiversityRequest.countrySite.ToLower()];
                    childAirportCodes = Global.childAirportCodesMT[apiDiversityRequest.countrySite.ToLower()];
                }

                searchRequestParams.os = apiDiversityRequest.os;
                searchRequestParams.platform = apiDiversityRequest.platform;
                searchRequestParams.offersReturned = diversity.hotels.Count;
                searchRequestParams.searchURL = diversityUrl;
                searchRequestParams.adults = apiDiversityRequest.adults;
                searchRequestParams.children = apiDiversityRequest.children;
                searchRequestParams.dateMin = apiDiversityRequest.dateMin;
                searchRequestParams.dateMax = apiDiversityRequest.dateMax;
                searchRequestParams.durationMin = apiDiversityRequest.durationMin;
                searchRequestParams.durationMax = apiDiversityRequest.durationMax;
                searchRequestParams.departureIds = apiDiversityRequest.departureIds;
                searchRequestParams.airportIds = new List<string>();
                if (apiDiversityRequest.departureIds < 0)
                {
                    searchRequestParams.airportIds = childAirportCodes[apiDiversityRequest.departureIds].Split(',').ToList();
                    searchRequestParams.parentDepartureIds = apiDiversityRequest.departureIds;
                }
                else
                {
                    searchRequestParams.parentDepartureIds = parentAirportCodes[apiDiversityRequest.departureIds];
                    searchRequestParams.airportIds.Add(apiDiversityRequest.departureIds.ToString());
                }
                searchRequestParams.ttssResponseTime = ttssResultsParams.ttssResponseTime;
                searchRequestParams.destinationIds = apiDiversityRequest.destinationId;
                searchRequestParams.labelId = apiDiversityRequest.destinationId;
                searchRequestParams.destinationType = "Label";
                searchRequestParams.isRegion = false;

                if (!string.IsNullOrWhiteSpace(ttssResultsParams.ttssURL))
                {
                    searchRequestParams.isCacheHit = false;
                }
                else
                {
                    searchRequestParams.isCacheHit = true;
                }
                searchRequestParams.ratings = apiDiversityRequest.ratings;
                searchRequestParams.tradingNameIds = apiDiversityRequest.tradingNameIds;
                searchRequestParams.timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");
                searchRequestParams.ttssURL = ttssResultsParams.ttssURL;
                searchRequestParams.indexingTime = ttssResultsParams.indexingTime;
                searchRequestParams.decoratingTime = ttssResultsParams.decoratingTime;
                searchRequestParams.offersInserted = ttssResultsParams.offersInserted;
                searchRequestParams.offersFound = ttssResultsParams.offersFound;
                searchRequestParams.ttssCount = ttssResultsParams.ttssCount;
                searchRequestParams.offersDecorated = ttssResultsParams.offersDecorated;
                searchRequestParams.hotelKeys = apiDiversityRequest.hotelKeys;
                if (apiDiversityRequest.hotelKeys.Count > 0)
                {
                    searchRequestParams.isHotelKeys = true;
                }
                else
                {
                    searchRequestParams.isHotelKeys = false;
                }
                searchRequestParams.endPoint = "diversity";
                try
                {
                    //HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + ConfigurationManager.AppSettings["RequestIndice"] + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);

                    //IE CODE
                    HttpWebRequest httpWebSearchRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["ES-ClusterUrl"] + IndiceNameHelper.GetRequestIndex(apiDiversityRequest.countrySite) + "-" + DateTime.UtcNow.ToString("yyyy-MM-dd") + "/" + ConfigurationManager.AppSettings["SearchIndiceDataType"]);

                    httpWebSearchRequest.ContentType = "application/json";
                    httpWebSearchRequest.Method = "POST";
                    httpWebSearchRequest.Timeout = int.Parse(ConfigurationManager.AppSettings["esTimeOut"]);
                    httpWebSearchRequest.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["esUserName"], ConfigurationManager.AppSettings["esPassword"]);
                    using (var streamWriter = new StreamWriter(httpWebSearchRequest.GetRequestStream()))
                    {
                        string searchRequestParamsResponse = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchRequestParams);
                        streamWriter.Write(searchRequestParamsResponse);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }
                    var httpSearchResponse = (HttpWebResponse)httpWebSearchRequest.GetResponse();
                    if (httpSearchResponse != null)
                    {
                        httpSearchResponse.Close();
                        httpSearchResponse.Dispose();
                    }
                }
                catch (WebException ex)
                {
                    ErrorLogger.Log(string.Format("Web exception occured while inserting Diversity call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log(string.Format("Exception occured while inserting Diversity call. Message - {0}, Stack Trace - {1}", ex.Message, ex.StackTrace), LogLevel.ElasticSearch);
                }
            });
            return diversity;
        }

        public static string GetTelephoneNumber(int regionId, string tradingNameId, int price, string platForm)
        {
            try
            {
                tradingNameId = tradingNameId.ToLower();
                string destinationTelephoneNumberKey = tradingNameId + '|' + regionId.ToString();

                if (!Global.DestinationTelePhoneNumbers.ContainsKey(destinationTelephoneNumberKey))
                {
                    return String.Empty;
                }

                List<Presentation.WCFRestService.Model.DestinationTelePhoneNumber> phoneNumbers = platForm.ToLower() != "app" ? Global.DestinationTelePhoneNumbers[destinationTelephoneNumberKey] : Global.DestinationTelePhoneNumbersForApp[destinationTelephoneNumberKey];
                var telePhoneSets = platForm.ToLower() != "app" ? Global.TelePhoneSets : Global.TelePhoneSetsForApp;
                var random = new Random();
                string telephoneNumber = string.Empty;

                if (phoneNumbers != null && phoneNumbers.Count > 0)
                {
                    if (price <= phoneNumbers[0].Price)
                    {
                        int index = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers[0].TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers[0].TelephoneSet].Number[index];
                    }

                    for (int index = 0; index < phoneNumbers.Count-1; index++)
                    {
                        if (phoneNumbers[index].Price <= price && price < phoneNumbers[index + 1].Price)
                        {
                            int numberCount = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers[index].TelephoneSet].Number.Count);
                            return telePhoneSets[phoneNumbers[index].TelephoneSet].Number[numberCount];
                        }
                    }

                    if (price >= phoneNumbers.Last().Price)
                    {
                        int numberCount = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers.Last().TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers.Last().TelephoneSet].Number[numberCount];
                    }
                }

                return telephoneNumber;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }

        private static string GetTelephoneNumberByLabelID(int labelId, string tradingNameId, int price, string platform)
        {
            XmlDocument xml = new XmlDocument();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            xml.Load(fileName);
            XmlNodeList regionList = xml.SelectNodes(string.Format("/Container/HolidaysData/Label[@Id='{0}']/Region", labelId));
            string regionId = string.Empty;
            string telephoneNumber = string.Empty;

            if (regionList != null && regionList.Count > 0)
            {
                foreach (XmlNode xn in regionList)
                {
                    regionId = xn.Attributes["Id"].Value;
                    telephoneNumber = GetTelephoneNumber(int.Parse(regionId), tradingNameId, price,platform);

                    if (!string.IsNullOrEmpty(telephoneNumber))
                    {
                        break;
                    }
                }
            }

            return telephoneNumber;
        }

        //IE CODE - TODO: Make code MT
        private static string GetTelephoneNumberMT(int regionId, string tradingNameId, int price, string platForm)
        {
            try
            {
                tradingNameId = tradingNameId.ToLower();
                string destinationTelephoneNumberKey = tradingNameId + '|' + regionId.ToString();

                if (!Global.DestinationTelePhoneNumbersMT.ContainsKey(destinationTelephoneNumberKey))
                {
                    return String.Empty;
                }

                List<Presentation.WCFRestService.Model.DestinationTelePhoneNumber> phoneNumbers = platForm.ToLower() != "app" ? Global.DestinationTelePhoneNumbersMT[destinationTelephoneNumberKey] : Global.DestinationTelePhoneNumbersForAppMT[destinationTelephoneNumberKey];
                var telePhoneSets = platForm.ToLower() != "app" ? Global.TelePhoneSetsMT : Global.TelePhoneSetsForAppMT;
                var random = new Random();
                string telephoneNumber = string.Empty;

                if (phoneNumbers != null && phoneNumbers.Count > 0)
                {
                    if (price <= phoneNumbers[0].Price)
                    {
                        int index = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers[0].TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers[0].TelephoneSet].Number[index];
                    }

                    for (int index = 0; index < phoneNumbers.Count - 1; index++)
                    {
                        if (phoneNumbers[index].Price <= price && price < phoneNumbers[index + 1].Price)
                        {
                            int numberCount = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers[index].TelephoneSet].Number.Count);
                            return telePhoneSets[phoneNumbers[index].TelephoneSet].Number[numberCount];
                        }
                    }

                    if (price >= phoneNumbers.Last().Price)
                    {
                        int numberCount = Utilities.GenerateRandomNumber(telePhoneSets[phoneNumbers.Last().TelephoneSet].Number.Count);
                        return telePhoneSets[phoneNumbers.Last().TelephoneSet].Number[numberCount];
                    }
                }

                return telephoneNumber;
            }
            catch (Exception ex)
            {

            }
            return string.Empty;
        }


        //IE CODE - TODO: Make code MT
        private static string GetTelephoneNumberByLabelIDMT(int labelId, string tradingNameId, int price, string platform)
        {
            XmlDocument xml = new XmlDocument();
            string fileName = HttpRuntime.AppDomainAppPath + @"data\destinationLabelRegions.xml";
            xml.Load(fileName);
            XmlNodeList regionList = xml.SelectNodes(string.Format("/Container/HolidaysData/Label[@Id='{0}']/Region", labelId));
            string regionId = string.Empty;
            string telephoneNumber = string.Empty;

            if (regionList != null && regionList.Count > 0)
            {
                foreach (XmlNode xn in regionList)
                {
                    regionId = xn.Attributes["Id"].Value;
                    telephoneNumber = GetTelephoneNumberMT(int.Parse(regionId), tradingNameId, price, platform);

                    if (!string.IsNullOrEmpty(telephoneNumber))
                    {
                        break;
                    }
                }
            }

            return telephoneNumber;
        }

    }
}