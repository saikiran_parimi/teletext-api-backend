﻿using System;
using Presentation.WCFRestService.Model;
using Newtonsoft.Json;
using System.Configuration;
using Presentation.WCFRestService.Model.Enum;

namespace Presentation.Web.WCFRestServices
{
    /* this class will responsible to get data from flight stats */
    public class FlightStats
    {
        /* implemented on Flight status (flights departing on date) */
        public FlightStatusResponse GetFlightByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightStatusResponse item = null;
            try
            {
                string airlineCode;
                string flightCode;
                GetAirlineCodeAndFlightNumber(flightNumber, out airlineCode, out flightCode);
                string flightStatUrl = ConfigurationManager.AppSettings["FS_FlightStatusURL"];
                string fsParams = ConfigurationManager.AppSettings["FS_FlightStatusParams"];
                fsParams = string.Format(fsParams, airlineCode, flightCode, year, month, day, ConfigurationManager.AppSettings["FS_appId"], ConfigurationManager.AppSettings["FS_appKey"]);
                string url = flightStatUrl + fsParams;              
                var json = String.Empty;   
                json = Utilities.GetJson<FlightStatusResponse>(url);
                item = JsonConvert.DeserializeObject<FlightStatusResponse>(json);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }

            return item;
        }

        /* Scheduled Flight(s) by carrier and flight number, departing on the given date. */
        public FlightScheduleResponse GetFlightScheduleByFlightNumberAndFlightDate(string flightNumber, int day, int month, int year)
        {
            FlightScheduleResponse item = null;
            try
            {
                string airlineCode;
                string flightCode;
                GetAirlineCodeAndFlightNumber(flightNumber, out airlineCode, out flightCode);
                string flightStatUrl = ConfigurationManager.AppSettings["FS_FlightScheduleURL"];
                string fsParams = ConfigurationManager.AppSettings["FS_FlightScheduleParams"];
                fsParams = string.Format(fsParams, airlineCode, flightCode, year, month, day, ConfigurationManager.AppSettings["FS_appId"], ConfigurationManager.AppSettings["FS_appKey"]);
                string url = flightStatUrl + fsParams;                
                var json = String.Empty;                
                json = Utilities.GetJson<FlightScheduleResponse>(url);
                if (string.IsNullOrEmpty(json))
                {
                    item = new FlightScheduleResponse();
                }
                else
                {
                    item = JsonConvert.DeserializeObject<FlightScheduleResponse>(json);
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log(ex.ToString(), LogLevel.Error);
            }

            return item;
        }

        private static void GetAirlineCodeAndFlightNumber(string flightNumber, out string airlineCode, out string flightCode)
        {
            if (Char.IsLetter(flightNumber, 2))
            {
                airlineCode = flightNumber.Substring(0, 3);
                flightCode = flightNumber.Substring(3, flightNumber.Length - 3);
            }
            else
            {
                airlineCode = flightNumber.Substring(0, 2);
                flightCode = flightNumber.Substring(2, flightNumber.Length - 2);
            }
        }

        
    }
}